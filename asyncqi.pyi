from .modules import *

import qi

class AsyncQi:
{services}
    def __init__(self, ip: str = "nao.local", port: int = 9559): ...

    def connect_signal(self, signal: str) -> AsyncSignal: ...

    def get_session(self) -> qi.Session: ...
