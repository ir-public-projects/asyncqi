from pathlib import Path
from typing import Any
import json
from collections.abc import Callable
from textwrap import indent
import argparse

DEFAULT_DESCRIPTION_PATTERNS: list[tuple[Callable[[str], bool], str]] = [
    (
        lambda desc: any(
            x in desc.lower() for x in ["position", "coordinate", "transform"]
        )
        and any(x in desc.lower() for x in ["array", "arrays", "vector"]),
        "list[list[float]]",
    ),
    (
        lambda desc: ("angle" in desc.lower() or "position" in desc.lower())
        and "list" in desc.lower(),
        "list[float] | list[list[float]]",
    ),
]


DEFAULT_COMBINED_PATTERNS: list[tuple[Callable[[str, str], bool], str]] = [
    (
        lambda desc, name: ("name" in name.lower() or "names" in name.lower())
        and "list" in desc.lower(),
        "str | list[str]",
    ),
    (
        lambda desc, name: "config" in name.lower() or "configuration" in desc.lower(),
        "dict[str, Any]",
    ),
    (
        lambda desc, name: "time" in name.lower() and "list" in desc.lower(),
        "list[float]",
    ),
]


def resolve_arg_type_from_description(description: str | None, name: str) -> str:
    """Resolve type hints from description and name."""
    if description is None:
        return "Any"
    for pattern, type_hint in DEFAULT_COMBINED_PATTERNS:
        if pattern(description, name):
            return type_hint
    for pattern, type_hint in DEFAULT_DESCRIPTION_PATTERNS:
        if pattern(description):
            return type_hint
    return "Any"


def resolve_return_type_from_description(description: str) -> str:
    """Resolve type hints from description."""
    if description is None:
        return "Any"
    for pattern, type_hint in DEFAULT_DESCRIPTION_PATTERNS:
        if pattern(description):
            return type_hint
    return "Any"


def wrap_with_quotes(value: str) -> str:
    if "\n" in value:
        return f'"""{value}\n"""'
    return f'"""{value}"""'


def preprocess_description(description: str) -> str:
    description = description.replace("\n", " ")
    description = description.replace("[", "\\\\[")
    description = description.replace("]", "\\\\]")
    return description


def apply_indent(value: str, level: int, indent_size: int = 4) -> str:
    return indent(value, " " * (level * indent_size))


def create_parameter_line(name: str, type_hint: str, description: str) -> str:
    return f"{name} ({type_hint}): {preprocess_description(description)}"


def create_parameter_section(parameters: list[dict[str, Any]]) -> str:
    return "Args:\n" + apply_indent(
        "\n".join(
            [
                create_parameter_line(p["name"], p["parameter_type"], p["description"])
                for p in parameters
            ]
        ),
        1,
    )


def create_return_section(return_type: str, return_description: str) -> str:
    if return_description is None:
        return "Returns:\n" + apply_indent(f"{return_type}: No description provided", 1)
    return "Returns:\n" + apply_indent(
        f"{return_type}: {preprocess_description(return_description)}", 1
    )


def create_docstring(
    description: str,
    parameters: list[dict[str, Any]],
    return_type: str,
    return_description: str,
) -> str:
    lines = []
    lines.append(f"{description}")
    if parameters:
        lines.append(create_parameter_section(parameters))
    if return_type is not None:
        lines.append(create_return_section(return_type, return_description))
    return wrap_with_quotes("\n\n".join(lines))


def create_deprecation_decorator(description: str) -> str:
    reason = [
        line.split("DEPRECATED. ")[-1]
        for line in description.split("\n")
        if "deprecated" in line.lower()
    ]

    if len(reason) > 0 and "use" in reason[0].lower():
        return f"@deprecated('{reason[0]}')"

    return "@deprecated('No reason provided')"


METHOD_HEADER = "async def {name}({params}) -> {return_type}:"
METHOD_RETURN = "..."


def create_method_stub(method: dict[str, Any]) -> str:
    """Generate a method stub with docstring and type hints."""
    for param in method["parameters"]:
        param["name"] = param["name"].replace(" ", "_")
    parts = []
    if method["needs_overload"]:
        parts.append("@overload")
    if "deprecated" in method["description"].lower():
        parts.append(create_deprecation_decorator(method["description"]))
    parts.append(
        METHOD_HEADER.format(
            name=method["name"],
            params=", ".join(
                ["self"]
                + [f"{p['name']}: {p['parameter_type']}" for p in method["parameters"]]
            ),
            return_type=method["returns"]["return_type"],
        )
    )
    parts.append(
        apply_indent(
            create_docstring(
                method["description"],
                method["parameters"],
                method["returns"]["return_type"],
                method["returns"]["description"],
            ),
            1,
        )
    )
    parts.append(apply_indent(METHOD_RETURN, 1))
    return "\n".join(parts)


def preprocess_types(
    service: dict[str, Any], overrides: dict[str, dict[str, str]] | None
) -> None:
    for method in service["methods"]:
        if overrides and method["name"] in overrides:
            for param_override_name, param_override in overrides[
                method["name"]
            ].items():
                for param in method["parameters"]:
                    if param["name"] == param_override_name:
                        param["parameter_type"] = param_override
            if "returns" in overrides[method["name"]]:
                method["returns"]["return_type"] = overrides[method["name"]]["return"]
        for param in method["parameters"]:
            if param["parameter_type"] == "Unknown":
                param["parameter_type"] = resolve_arg_type_from_description(
                    param["description"], param["name"]
                )
        if method["returns"]["return_type"] == "Unknown":
            method["returns"]["return_type"] = resolve_return_type_from_description(
                method["returns"]["description"]
            )


def add_overload(service: dict[str, Any]):
    nrof_definitions: dict[str, int] = dict()
    for method in service["methods"]:
        if method["name"] in nrof_definitions:
            nrof_definitions[method["name"]] += 1
        else:
            nrof_definitions[method["name"]] = 1
    for method in service["methods"]:
        if nrof_definitions[method["name"]] > 1:
            method["needs_overload"] = True
        else:
            method["needs_overload"] = False


def create_service_stub(
    service: dict[str, Any], overrides: dict[str, dict[str, str]] | None = None
) -> str:
    """Generate a complete stub file content for a service."""
    preprocess_types(service, overrides)
    add_overload(service)
    parts = []
    parts.append("from typing import Any, overload")
    parts.append("from typing_extensions import deprecated")
    parts.append(f"class {service['name']}:")
    parts.append(apply_indent(wrap_with_quotes(service["description"]), 1))
    parts.append(apply_indent("def __init__(self) -> None:", 1))
    parts.append(apply_indent("...", 2))
    for method in service["methods"]:
        parts.append(apply_indent(create_method_stub(method), 1))
    return "\n".join(parts)


class StubGenerator:
    def __init__(self, json_data: list[dict[str, Any]]):
        self.services = json_data

    def generate_stubs(self) -> dict[str, str]:
        """Generate stub files for all services."""
        return {
            f"{service['name']}.pyi": create_service_stub(service)
            for service in self.services
        }


def to_snake_case(text: str) -> str:
    result = []
    for i, char in enumerate(text):
        # Add underscore if it's a capital letter and:
        # - not the first letter
        # - not preceded by a capital OR
        # - followed by a lowercase letter (for cases like 'API')
        if char.isupper() and i > 0:
            if not text[i - 1].isupper() or (
                i + 1 < len(text) and text[i + 1].islower()
            ):
                result.append("_")
        result.append(char.lower())
    return "".join(result)


def make_root_stub_file(root_template: str, service_names: list[str]) -> str:
    services_stub_string = "\n".join(
        [
            f"{to_snake_case(service_name)}: {service_name}"
            for service_name in service_names
        ]
    )
    services_stub_string = apply_indent(services_stub_string, 1)
    return root_template.format(services=services_stub_string)


def make_services_init_file(service_names: list[str]) -> str:
    imports = "\n".join(
        [
            f"from .{service_name} import {service_name}"
            for service_name in service_names
        ]
    )
    all_prop = (
        "__all__ = [\n"
        + apply_indent(
            ",\n".join([f"'{service_name}'" for service_name in service_names]), 1
        )
        + "\n]"
    )
    return f"{imports}\n\n{all_prop}"


def generate_stub_files(
    service_data_path: str,
    output_directory_path: Path,
    overrides_path: str,
    asyncqi_template_path: str,
) -> None:
    """Generate .pyi stub files from JSON data."""

    with open(service_data_path, "r") as f:
        json_data = json.load(f)

    with open(overrides_path, "r") as f:
        overrides = json.load(f)

    for service in json_data:
        service_override = overrides.get(service["name"], None)
        content = create_service_stub(service, service_override)
        with open(
            Path(output_directory_path) / "modules" / f"{service["name"]}.pyi", "w"
        ) as f:
            f.write(content)

    with open(asyncqi_template_path, "r") as f:
        root_template = f.read()

    with open(Path(output_directory_path) / "modules" / "__init__.pyi", "w") as f:
        f.write(make_services_init_file([service["name"] for service in json_data]))

    with open(Path(output_directory_path) / "asyncqi.pyi", "w") as f:
        f.write(
            make_root_stub_file(
                root_template, [service["name"] for service in json_data]
            )
        )


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--service-data",
        type=str,
        help="Path to the JSON file with service data",
        required=True,
    )
    parser.add_argument(
        "--overrides", type=str, help="Path to the JSON file with overrides"
    )
    parser.add_argument(
        "--output", type=str, help="Path to the output directory", required=True
    )
    parser.add_argument(
        "--asyncqi-template",
        type=str,
        help="Path to the asyncqi template file",
        required=True,
    )
    args = parser.parse_args()

    output_directory = Path(args.output)
    output_directory.mkdir(parents=True, exist_ok=True)
    output_directory.joinpath("modules").mkdir(exist_ok=True)

    generate_stub_files(
        args.service_data, output_directory, args.overrides, args.asyncqi_template
    )


# Example usage:
if __name__ == "__main__":
    main()
