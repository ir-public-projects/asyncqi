from hypothesis import given, strategies as st
from hypothesis.strategies import composite, DrawFn

# Import your module here - assuming the parser code is in parser.py
from codegen.type_signature_parser import parse_signature, strip_annotations
from typing import Any


# Custom strategies for generating valid type signatures
@composite
def basic_type_strategy(draw: DrawFn) -> str:
    """Strategy for generating basic type signatures"""
    return draw(st.sampled_from("sifIbmoX"))


@composite
def type_signature_strategy(draw: DrawFn, max_depth: int = 3):
    """Strategy for generating valid type signatures with nested structures"""
    if max_depth == 0:
        return draw(basic_type_strategy())

    # Choose between basic type, list, tuple, or dict
    structure_type = draw(st.sampled_from(["basic", "list", "tuple", "dict"]))

    if structure_type == "basic":
        return draw(basic_type_strategy())

    if structure_type == "list":
        inner_strategy = type_signature_strategy(max_depth=max_depth - 1)
        inner = draw(inner_strategy)
        return f"[{inner}]"

    if structure_type == "tuple":
        num_elements = draw(st.integers(min_value=1, max_value=3))
        elements = [
            draw(type_signature_strategy(max_depth=max_depth - 1))
            for _ in range(num_elements)
        ]
        return f'({"".join(elements)})'

    # Dict case
    key_type = draw(type_signature_strategy(max_depth=max_depth - 1))
    value_type = draw(type_signature_strategy(max_depth=max_depth - 1))
    return f"{{{key_type}{value_type}}}"


@composite
def annotated_signature_strategy(draw):
    """Strategy for generating signatures with annotations"""
    base_sig = draw(type_signature_strategy())
    annotation = draw(
        st.text(
            alphabet=st.characters(blacklist_characters="<>"), min_size=1, max_size=10
        )
    )
    return f"{base_sig}<{annotation}>"


# Core property-based tests
@given(basic_type_strategy())
def test_basic_types(type_char):
    """Test that any basic type can be parsed successfully"""
    result = parse_signature(type_char)
    assert not isinstance(result, str), f"Failed to parse basic type: {type_char}"
    assert len(result) == 1, "Should return exactly one type"


@given(st.lists(basic_type_strategy(), min_size=1, max_size=5))
def test_multiple_basic_types(types):
    """Test that any sequence of basic types can be parsed"""
    signature = "".join(types)
    result = parse_signature(signature)
    assert not isinstance(result, str), f"Failed to parse: {signature}"
    assert len(result) == len(types), "Number of parsed types should match input"


@given(type_signature_strategy())
def test_nested_structures(signature):
    """Test that any valid nested structure can be parsed"""
    result = parse_signature(signature)
    assert not isinstance(result, str), f"Failed to parse nested structure: {signature}"
    assert len(result) == 1, "Should return exactly one type for nested structure"


@given(annotated_signature_strategy())
def test_annotated_signatures(signature):
    """Test that signatures with annotations are properly handled"""
    result = parse_signature(signature)
    assert not isinstance(
        result, str
    ), f"Failed to parse annotated signature: {signature}"

    # The same signature without annotations should parse to the same result
    clean_sig = strip_annotations(signature)
    clean_result = parse_signature(clean_sig)
    assert result == clean_result, "Annotations should not affect parsing result"


@given(st.lists(type_signature_strategy(), min_size=1, max_size=3))
def test_multiple_complex_types(signatures):
    """Test that sequences of complex types can be parsed"""
    combined_signature = "".join(signatures)
    result = parse_signature(combined_signature)
    assert not isinstance(result, str), f"Failed to parse: {combined_signature}"
    assert len(result) == len(signatures), "Number of parsed types should match input"


@given(type_signature_strategy(max_depth=2))
def test_structure_preservation(signature):
    """Test that the structure of the type signature is preserved in the result"""
    result = parse_signature(signature)
    assert not isinstance(result, str), f"Failed to parse: {signature}"

    # Check basic structural properties
    if signature.startswith("["):
        assert str(result[0]).startswith("list"), "List structure not preserved"
    elif signature.startswith("("):
        assert str(result[0]).startswith("tuple"), "Tuple structure not preserved"
    elif signature.startswith("{"):
        assert str(result[0]).startswith("dict"), "Dict structure not preserved"


# A few minimal traditional tests for obvious error cases
def test_obvious_errors():
    """Test that obviously invalid inputs result in errors"""
    invalid_cases = [
        "]",  # Closing bracket without opening
        "[",  # Unclosed bracket
        "xyz",  # Invalid characters
        "{s",  # Incomplete dict
        "(s",  # Incomplete tuple
    ]

    for invalid_sig in invalid_cases:
        result = parse_signature(invalid_sig)
        assert isinstance(result, str) and result.startswith(
            "Error:"
        ), f"Expected error for invalid signature: {invalid_sig}"

