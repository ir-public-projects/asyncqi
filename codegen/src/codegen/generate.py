from dataclasses import dataclass, asdict
import json
from typing import get_origin, get_args, Literal
from typing import Any, TypedDict
import qi
import logging
from codegen.type_signature_parser import parse_signature, ParamType

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.DEBUG)


@dataclass
class Parameter:
    name: str
    description: str
    parameter_type: type[ParamType]


@dataclass
class Return:
    return_type: type[ParamType | None]
    description: str | None = None


@dataclass
class Signal:
    name: str
    description: str
    callback_signature: type


@dataclass
class Method:
    name: str
    description: str
    parameters: list[Parameter]
    returns: Return


@dataclass
class Service:
    name: str
    description: str
    methods: list[Method]
    signals: list[Signal]


class ServiceEncoder(json.JSONEncoder):
    """Custom JSON encoder to handle Service-related dataclasses and types."""

    def _encode_type(self, obj: type) -> str:
        """Helper method to encode type objects into string representations."""
        # Handle basic types
        if obj in (str, int, float, bool):
            return obj.__name__

        # Handle Literal types
        if get_origin(obj) is Literal:
            args = get_args(obj)
            if len(args) == 1 and args[0] == "Unknown":
                return "Unknown"
            return f"Literal[{', '.join(repr(arg) for arg in args)}]"

        # Handle list types
        if get_origin(obj) is list:
            args = get_args(obj)
            if args:
                return f"list[{self._encode_type(args[0])}]"
            return "list"

        # Handle tuple types
        if get_origin(obj) is tuple:
            args = get_args(obj)
            if args:
                return f"tuple[{', '.join(self._encode_type(arg) for arg in args)}]"
            return "tuple"

        # Handle dict types
        if get_origin(obj) is dict:
            args = get_args(obj)
            if len(args) == 2:
                return (
                    f"dict[{self._encode_type(args[0])}, {self._encode_type(args[1])}]"
                )
            return "dict"

        # Handle Unknown literal type
        if obj == "Unknown":
            return "Unknown"

        # Default case
        return str(obj)

    def default(self, obj: Any) -> Any:
        if hasattr(obj, "__dataclass_fields__"):
            # Handle dataclasses by converting them to dictionaries
            return asdict(obj)
        if isinstance(obj, type) or hasattr(obj, "__origin__"):
            # Handle type objects and typing constructs
            return self._encode_type(obj)
        return super().default(obj)


def serialize(services: list[Service]):
    return json.dumps(services, cls=ServiceEncoder, indent=4)


def parse_return_type(return_string: str) -> type[ParamType | None]:
    if return_string == "v":
        return None  # pyright: ignore[reportReturnType]
    result = parse_signature(return_string)
    if isinstance(result, str):
        raise ValueError(f"Error parsing return type: {result}")
    return result[0]


class ParameterMeta(TypedDict):
    name: str
    description: str


def parse_parameters(
    parameters_meta: list[ParameterMeta],
    parameters_signature: str,
):
    logger.debug("Parsing parameters %s, %s", parameters_meta, parameters_signature)
    parameters: list[Parameter] = []
    parameter_types: str | list[ParamType] = parse_signature(
        parameters_signature.strip("()")
    )
    if isinstance(parameter_types, str):
        if "Text empty" in parameter_types:
            return parameters
        raise ValueError(f"Error parsing parameters: {parameter_types}")
    if not parameter_types:
        return parameters
    if len(parameters_meta) == len(parameter_types):
        logger.warning("Parameter length of type and meta mismatch")
    for parameter_type_unparsed, parameter_meta in zip(
        parameter_types, parameters_meta
    ):
        name: str = parameter_meta["name"]
        description: str = parameter_meta["description"]
        parameter_type: type[ParamType] = parameter_type_unparsed
        parameters.append(Parameter(name, description, parameter_type))
    return parameters


def parse_return(return_signature: str, return_description: str):
    logger.debug("Parsing return signature %s", return_signature)
    return Return(
        return_type=parse_return_type(return_signature),
        description=return_description if return_description else None,
    )


# Index where internal methods from aldebaran we really dont care about stop
INTERNAL_METHODS_STOP_INDEX = 100

INTERNAL_METHOD_NAMES = [
    "pCall",
    "call",
    "version",
    "exit",
    "ping",
    "getMethodList",
    "getMetaObject",
    "getUsage",
    "getMethodHelp",
    "getModuleHelp",
    "wait",
    "isRunning",
    "getBrokerName",
    "stop",
]


class MethodMeta(TypedDict):
    name: str
    description: str
    parameters: list[ParameterMeta]
    parametersSignature: str
    returnSignature: str
    returnDescription: str


class ServiceMeta(TypedDict):
    methods: dict[int, MethodMeta]
    description: str


def get_service_methods(service_meta: ServiceMeta):
    methods_meta = service_meta["methods"]
    methods: list[Method] = []
    for method_id in methods_meta.keys():
        method_meta = methods_meta[method_id]
        if (
            method_id < INTERNAL_METHODS_STOP_INDEX
            or method_meta["name"] in INTERNAL_METHOD_NAMES
            or method_meta["name"].startswith("_")
        ):
            logger.debug("Skipping method %s", method_meta["name"])
            continue
        logger.debug("Parsing method %s", method_meta)
        name = method_meta["name"]
        description = method_meta["description"]
        parameters = parse_parameters(
            parameters_meta=method_meta["parameters"],
            parameters_signature=method_meta["parametersSignature"],
        )
        returns = parse_return(
            method_meta["returnSignature"], method_meta["returnDescription"]
        )

        methods.append(
            Method(
                name,
                description,
                parameters,
                returns,
            )
        )
    return methods


def generate_service(service_name: str, service_meta: ServiceMeta):
    logger.debug("Generating service %s", service_name)
    return Service(
        service_name,
        service_meta["description"],
        get_service_methods(service_meta),
        [],
    )


SERVICE_BLACKLIST = ["ALServiceManager", "ALModularity"]


def generate_services(session: qi.Session):
    logger.debug("Generating services")
    return [
        generate_service(service["name"], session.service(service["name"]).metaObject())
        for service in session.services()
        if service["name"].startswith("AL") and service["name"] not in SERVICE_BLACKLIST
    ]


def main():
    # Get the service ALTextToSpeech.
    session = qi.Session()
    session.connect()
    services = generate_services(session)
    with open("services.json", "w") as f:
        f.write(serialize(services))


if __name__ == "__main__":
    main()
