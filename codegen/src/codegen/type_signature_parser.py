from collections.abc import Iterator
from typing import Literal, override
from dataclasses import dataclass
import itertools


@dataclass(frozen=True)
class Token:
    type: Literal["basic", "bracket"]
    value: str

    @override
    def __repr__(self) -> str:
        return f"Token(type={self.type}, value={self.value})"


type Unknown = Literal["Unknown"]
type BaseParamType = str | int | float | bool | Unknown
type ParamType = (
    BaseParamType
    | list["ParamType"]
    | tuple["ParamType", ...]
    | dict["ParamType", "ParamType"]
)
type PythonType = type[str] | type[int] | type[bool] | type[float]
type MappedType = PythonType | Unknown
type TypeMapping = dict[str, MappedType]

TYPES: TypeMapping = {
    "s": str,
    "i": int,
    "I": int,
    "f": float,
    "b": bool,
    "m": "Unknown",
    "o": "Unknown",
    "X": "Unknown",
}

BRACKETS = {
    "[": "]",  # List
    "(": ")",  # Tuple
    "{": "}",  # Dict
}


class ParseError(Exception):
    """Raised when parsing fails with detailed context."""

    def __init__(self, message: str, position: int):
        self.position = position
        super().__init__(f"{message} at position {position}")


def strip_annotations(signature: str) -> str:
    """Remove angle bracket annotations from the signature."""
    result: list[str] = []
    depth: int = 0

    for char in signature:
        if char == "<":
            depth += 1
        elif char == ">":
            depth -= 1
        elif depth == 0:
            result.append(char)

    return "".join(result)


def tokenize(text: str) -> Iterator[Token]:
    """Tokenize the input text."""
    for pos, char in enumerate(text):
        if char in TYPES:
            yield Token("basic", char)
        elif char in BRACKETS.keys() or char in BRACKETS.values():
            yield Token("bracket", char)
        else:
            raise ParseError(f"Invalid character: '{char}'", pos)


def parse_type(tokens: Iterator[Token], depth: int = 0) -> ParamType:
    token = next(tokens)

    if token.type == "basic":
        return TYPES[token.value]

    if token.type != "bracket" or token.value not in BRACKETS:
        raise ParseError(f"Expected opening bracket, got '{token.value}'", depth)

    opening = token.value
    closing = BRACKETS[opening]

    match opening:
        case "[":
            try:
                element_type = parse_type(tokens, depth + 1)
            except StopIteration:
                raise ParseError("Unexpected end of input", depth)
            try:
                token = next(tokens)
                if token.value != closing:
                    raise ParseError(f"Expected closing bracket '{closing}'", depth)
            except StopIteration:
                raise ParseError("Unexpected end of input", depth)
            return list[element_type]

        case "(":
            types: list[ParamType] = []
            try:
                token = None
                while True:
                    tokens = itertools.chain([token], tokens) if token else tokens
                    types.append(parse_type(tokens, depth + 1))
                    token = next(tokens)
                    if token.value == closing:
                        break
            except StopIteration:
                raise ParseError("Unexpected end of input", depth)

            if not types:
                raise ParseError("Empty tuple", depth)

            return tuple[ParamType, ...]

        case "{":
            try:
                key_type = parse_type(tokens, depth + 1)
                value_type = parse_type(tokens, depth + 1)
            except StopIteration:
                raise ParseError("Unexpected end of input", depth)
            if (token := next(tokens, None)) is None or token.value != closing:
                raise ParseError(f"Expected closing bracket '{closing}'", depth)
            return dict[key_type, value_type]
    raise ParseError(f"Invalid bracket type: '{opening}'", depth)


def parse_signature(signature: str) -> list[type[ParamType]] | str:
    try:
        cleaned_signature = strip_annotations(signature)
        tokens = tokenize(cleaned_signature)
        types: list[type[ParamType]] = []
        while True:
            try:
                types.append(parse_type(tokens))
            except StopIteration:
                break

        return types
    except ParseError as e:
        return f"Error: {str(e)}"
