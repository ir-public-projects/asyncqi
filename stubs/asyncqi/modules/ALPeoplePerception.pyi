from typing import Any, overload
from typing_extensions import deprecated
class ALPeoplePerception:
    """This module controls the People Perception flow and pushes information about detected people in ALMemory."""
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def isPaused(self) -> bool:
        """Gets extractor pause status

        Returns:
            bool: True if the extractor is paused, False if not
        """
        ...
    async def pause(self, status: bool) -> None:
        """Changes the pause status of the extractor

        Args:
            status (bool): New pause satus
        """
        ...
    async def isProcessing(self) -> bool:
        """Gets extractor running status

        Returns:
            bool: True if the extractor is currently processing images, False if not
        """
        ...
    async def getMaximumDetectionRange(self) -> float:
        """Gets the current maximum detection and tracking range.

        Returns:
            float: Maximum range in meters.
        """
        ...
    async def getMinimumBodyHeight(self) -> float:
        """Gets the current minimum body height used for human detection (3D mode only).

        Returns:
            float: Minimum height in meters.
        """
        ...
    async def getMaximumBodyHeight(self) -> float:
        """Gets the current maximum body height used for human detection (3D mode only).

        Returns:
            float: Maximum height in meters.
        """
        ...
    async def getTimeBeforePersonDisappears(self) -> float:
        """Gets the time after which a person, supposed not to be in the field of view of the camera disappears if it has not been detected.

        Returns:
            float: Time in seconds.
        """
        ...
    async def getTimeBeforeVisiblePersonDisappears(self) -> float:
        """Gets the time after which a person, supposed to be in the field of view of the camera disappears if it has not been detected.

        Returns:
            float: Time in seconds.
        """
        ...
    async def isFaceDetectionEnabled(self) -> bool:
        """Gets the current status of face detection.

        Returns:
            bool: True if face detection is enabled, False otherwise.
        """
        ...
    async def isFastModeEnabled(self) -> bool:
        """Gets the current status of fast mode.

        Returns:
            bool: True if fast mode is enabled, False otherwise.
        """
        ...
    async def isGraphicalDisplayEnabled(self) -> bool:
        """Gets the current status of graphical display in Choregraphe.

        Returns:
            bool: True if graphical display is enabled, False otherwise.
        """
        ...
    async def isMovementDetectionEnabled(self) -> bool:
        """Gets the current status of movement detection in Choregraphe.

        Returns:
            bool: True if movement detection is enabled, False otherwise.
        """
        ...
    async def resetPopulation(self) -> None:
        """Empties the tracked population."""
        ...
    async def setFastModeEnabled(self, enable: bool) -> None:
        """Turns fast mode on or off.

        Args:
            enable (bool): True to turn it on, False to turn it off.
        """
        ...
    async def setGraphicalDisplayEnabled(self, enable: bool) -> None:
        """Turns graphical display in Choregraphe on or off.

        Args:
            enable (bool): True to turn it on, False to turn it off.
        """
        ...
    async def setMaximumDetectionRange(self, range: float) -> None:
        """Sets the maximum range for human detection and tracking.

        Args:
            range (float): Maximum range in meters.
        """
        ...
    async def setMinimumBodyHeight(self, height: float) -> None:
        """Sets the minimum human body height (3D mode only).

        Args:
            height (float): Minimum height in meters.
        """
        ...
    async def setMaximumBodyHeight(self, height: float) -> None:
        """Sets the maximum human body height (3D mode only).

        Args:
            height (float): Maximum height in meters.
        """
        ...
    async def setMovementDetectionEnabled(self, enable: bool) -> None:
        """Turns movement detection on or off.

        Args:
            enable (bool): True to turn it on, False to turn it off.
        """
        ...
    async def setTimeBeforePersonDisappears(self, seconds: float) -> None:
        """Sets the time after which a person, supposed not to be in the field of view of the camera disappears if it has not been detected.

        Args:
            seconds (float): Time in seconds.
        """
        ...
    async def setTimeBeforeVisiblePersonDisappears(self, seconds: float) -> None:
        """Sets the time after which a person, supposed to be in the field of view of the camera disappears if it has not been detected.

        Args:
            seconds (float): Time in seconds.
        """
        ...
    @deprecated('No reason provided')
    async def setFaceDetectionEnabled(self) -> None:
        """DEPRECATED"""
        ...