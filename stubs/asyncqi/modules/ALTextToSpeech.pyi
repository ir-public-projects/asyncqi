from typing import Any, overload
from typing_extensions import deprecated
class ALTextToSpeech:
    """This module embeds a speech synthetizer whose role is to convert text commands into sound waves that are then either sent to Nao's loudspeakers or written into a file. This service supports several languages and some parameters of the synthetizer can be tuned to change each language's synthetic voice."""
    def __init__(self) -> None:
        ...
    @overload
    async def say(self, stringToSay: str) -> None:
        """Performs the text-to-speech operations : it takes a std::string as input and outputs a sound in both speakers. String encoding must be UTF8.

        Args:
            stringToSay (str): Text to say, encoded in UTF-8.
        """
        ...
    @overload
    async def say(self, stringToSay: str, language: str) -> None:
        """Performs the text-to-speech operations in a specific language: it takes a std::string as input and outputs a sound in both speakers. String encoding must be UTF8. Once the text is said, the language is set back to its initial value.

        Args:
            stringToSay (str): Text to say, encoded in UTF-8.
            language (str): Language used to say the text.
        """
        ...
    async def sayToFile(self, pStringToSay: str, pFileName: str) -> None:
        """Performs the text-to-speech operations: it takes a std::string as input and outputs the corresponding audio signal in the specified file.

        Args:
            pStringToSay (str): Text to say, encoded in UTF-8.
            pFileName (str): RAW file where to store the generated signal. The signal is encoded with a sample rate of 22050Hz, format S16_LE, 2 channels.
        """
        ...
    async def stopAll(self) -> None:
        """This method stops the current and all the pending tasks immediately."""
        ...
    async def setLanguage(self, pLanguage: str) -> None:
        """Changes the language used by the Text-to-Speech engine. It automatically changes the voice used since each of them is related to a unique language. If you want that change to take effect automatically after reboot of your robot, refer to the robot web page (setting page).

        Args:
            pLanguage (str): Language name. Must belong to the languages available in TTS (can be obtained with the getAvailableLanguages method).  It should be an identifier std::string.
        """
        ...
    async def getLanguage(self) -> str:
        """Returns the language currently used by the text-to-speech engine.

        Returns:
            str: Language of the current voice.
        """
        ...
    async def getAvailableLanguages(self) -> list[str]:
        """Outputs the languages installed on the system.

        Returns:
            list[str]: Array of std::string that contains the languages installed on the system.
        """
        ...
    async def getSupportedLanguages(self) -> list[str]:
        """Outputs all the languages supported (may be installed or not).

        Returns:
            list[str]: Array of std::string that contains all the supported languages (may be installed or not).
        """
        ...
    async def resetSpeed(self) -> None:
        """Changes the parameters of the voice. For now, it is only possible to reset the voice speed."""
        ...
    async def setParameter(self, pEffectName: str, pEffectValue: float) -> None:
        """Changes the parameters of the voice. The available parameters are: 
         	 pitchShift: applies a pitch shifting to the voice. The value indicates the ratio between the new fundamental frequencies and the old ones (examples: 2.0: an octave above, 1.5: a quint above). Correct range is (1.0 -- 4), or 0 to disable effect.
         	 doubleVoice: adds a second voice to the first one. The value indicates the ratio between the second voice fundamental frequency and the first one. Correct range is (1.0 -- 4), or 0 to disable effect 
         	 doubleVoiceLevel: the corresponding value is the level of the double voice (1.0: equal to the main voice one). Correct range is (0 -- 4). 
         	 doubleVoiceTimeShift: the corresponding value is the delay between the double voice and the main one. Correct range is (0 -- 0.5) 
         If the effect value is not available, the effect parameter remains unchanged.

        Args:
            pEffectName (str): Name of the parameter.
            pEffectValue (float): Value of the parameter.
        """
        ...
    async def getParameter(self, pParameterName: str) -> float:
        """Returns the value of one of the voice parameters. The available parameters are: "pitchShift", "doubleVoice","doubleVoiceLevel" and "doubleVoiceTimeShift"

        Args:
            pParameterName (str): Name of the parameter.

        Returns:
            float: Value of the specified parameter
        """
        ...
    async def setVoice(self, pVoiceID: str) -> None:
        """Changes the voice used by the text-to-speech engine. The voice identifier must belong to the installed voices, that can be listed using the 'getAvailableVoices' method. If the voice is not available, it remains unchanged. No exception is thrown in this case. For the time being, only two voices are available by default : Kenny22Enhanced (English voice) and Julie22Enhanced (French voice)

        Args:
            pVoiceID (str): The voice (as a std::string).
        """
        ...
    async def getVoice(self) -> str:
        """Returns the voice currently used by the text-to-speech engine.

        Returns:
            str: Name of the current voice
        """
        ...
    async def getAvailableVoices(self) -> list[str]:
        """Outputs the available voices. The returned list contains the voice IDs.

        Returns:
            list[str]:  Array of std::string containing the voices installed on the system.
        """
        ...
    async def setVolume(self, volume: float) -> None:
        """Sets the volume of text-to-speech output.

        Args:
            volume (float): Volume (between 0.0 and 1.0).
        """
        ...
    async def getVolume(self) -> float:
        """Fetches the current volume the text to speech.

        Returns:
            float: Volume (integer between 0 and 100).
        """
        ...
    async def locale(self) -> str:
        """Get the locale associate to the current language.

        Returns:
            str: A string with xx_XX format (region_country)
        """
        ...
    async def loadVoicePreference(self, pPreferenceName: str) -> None:
        """Loads a set of voice parameters defined in a xml file contained in the preferences folder.The name of the xml file must begin with ALTextToSpeech_Voice_ 

        Args:
            pPreferenceName (str): Name of the voice preference.
        """
        ...
    async def setLanguageDefaultVoice(self, Language: str, Voice: str) -> None:
        """Sets a voice as the default voice for the corresponding language

        Args:
            Language (str): The language among those available on your robot as a String
            Voice (str): The voice among those available on your robot as a String
        """
        ...
    async def showDictionary(self) -> None:
        """Shows the Dictionary."""
        ...
    async def reset(self) -> None:
        """Reset ALTextToSpeech to his default state."""
        ...
    @overload
    async def deleteFromDictionary(self, word: str) -> bool:
        """Unload the dictionary.

        Args:
            word (str): the word you wish to delete, does not have to be in japanese.

        Returns:
            bool: bool: true if succeeded, false if failed
        """
        ...
    @overload
    async def deleteFromDictionary(self, word: str) -> bool:
        """Unload the dictionary.

        Args:
            word (str): the word you wish to delete, does not have to be in japanese.

        Returns:
            bool: bool: true if succeeded, false if failed
        """
        ...
    @overload
    async def addToDictionary(self, type: str, word: str, priority: str, phonetic: str, accent: str) -> bool:
        """Add a word to the library

        Args:
            type (str): the type of word you wish to insert, does not have to be in japanese.
            word (str): the word you wish to insert, does not have to be in japanese.
            priority (str): the priority of the word.
            phonetic (str): the phonetic pronouciation in KATAKANA.
            accent (str): syllabus and accentuation

        Returns:
            bool: bool: true if succeeded, false if failed
        """
        ...
    @overload
    async def addToDictionary(self, text: str, toReplace: str) -> bool:
        """Add a word to the library

        Args:
            text (str): the text you wish to insert.
            toReplace (str): text to replace.

        Returns:
            bool: bool: true if succeeded, false if failed
        """
        ...