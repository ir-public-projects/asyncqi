from typing import Any, overload
from typing_extensions import deprecated
class ALExpressionWatcher:
    """"""
    def __init__(self) -> None:
        ...
    async def add(self, expression: str, report_mode: int) -> Any:
        """Adds a condition expression to ALExpressionWatcher engine

        Args:
            expression (str): Condition expression in ConditionChecker language
            report_mode (int): ALExpressionWatcher report mode, available modes: { REPORT_CHANGE = 0, REPORT_EDGE = 1, REPORT_EDGE_TRUE = 2 }

        Returns:
            Any: Corresponding ExpressionObject
        """
        ...