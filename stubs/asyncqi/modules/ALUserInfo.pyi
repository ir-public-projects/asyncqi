from typing import Any, overload
from typing_extensions import deprecated
class ALUserInfo:
    """"""
    def __init__(self) -> None:
        ...
    @overload
    async def get(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def get(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def get(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def get(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def set(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def set(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def has(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def has(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def has(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def has(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def remove(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def remove(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def removeUser(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def removeUser(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getType(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...