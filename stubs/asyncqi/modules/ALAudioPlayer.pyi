from typing import Any, overload
from typing_extensions import deprecated
class ALAudioPlayer:
    """This module allows to play wav files on NAO"""
    def __init__(self) -> None:
        ...
    @overload
    async def playFile(self, fileName: str) -> None:
        """Plays a wav or mp3 file

        Args:
            fileName (str): Path of the sound file
        """
        ...
    @overload
    async def playFile(self, fileName: str, volume: float, pan: float) -> None:
        """Plays a wav or mp3 file, with specific volume and audio balance

        Args:
            fileName (str): Path of the sound file
            volume (float): volume of the sound file (must be between 0.0 and 1.0)
            pan (float): audio balance of the sound file (-1.0 : left / 1.0 : right / 0.0 : centered)
        """
        ...
    @overload
    async def playFileInLoop(self, fileName: str) -> None:
        """Plays a wav or mp3 file in loop

        Args:
            fileName (str): Path of the sound file
        """
        ...
    @overload
    async def playFileInLoop(self, fileName: str, volume: float, pan: float) -> None:
        """Plays a wav or mp3 file in loop, with specific volume and audio balance

        Args:
            fileName (str): Path of the sound file
            volume (float): volume of the sound file (must be between 0.0 and 1.0)
            pan (float): audio balance of the sound file (-1.0 : left / 1.0 : right)
        """
        ...
    @overload
    async def playFileFromPosition(self, fileName: str, position: float) -> None:
        """Plays a wav or mp3 file from a given position in the file.

        Args:
            fileName (str): Name of the sound file
            position (float): Position in second where the playing has to begin
        """
        ...
    @overload
    async def playFileFromPosition(self, fileName: str, position: float, volume: float, pan: float) -> None:
        """Plays a wav or mp3 file from a given position in the file, with specific volume and audio balance

        Args:
            fileName (str): Name of the sound file
            position (float): Position in second where the playing has to begin
            volume (float): volume of the sound file (must be between 0.0 and 1.0)
            pan (float): audio balance of the sound file (-1.0 : left / 1.0 : right)
        """
        ...
    async def pause(self, id: int) -> None:
        """Pause a play back

        Args:
            id (int): Id of the process that is playing the file you want to put in pause
        """
        ...