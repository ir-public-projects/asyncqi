from typing import Any, overload
from typing_extensions import deprecated
class ALPythonBridge:
    """This module evaluates python commands on the fly."""
    def __init__(self) -> None:
        ...
    async def eval(self, stringToEvaluate: str) -> str:
        """eval script

        Args:
            stringToEvaluate (str): string to eval

        Returns:
            str: if the evaluation has gone wrong
        """
        ...
    async def evalReturn(self, stringToEvaluate: str) -> Any:
        """eval script and return result. evalReturn(2+2) will return 4

        Args:
            stringToEvaluate (str): string to eval

        Returns:
            Any: the result of the evaluation
        """
        ...
    async def evalFull(self, stringToEvaluate: str) -> Any:
        """evaluates script and returns an informative array.

        Args:
            stringToEvaluate (str): string to eval

        Returns:
            Any: an array containing \\[return value, exceptions, stdout, stderr\\]
        """
        ...