from typing import Any, overload
from typing_extensions import deprecated
class ALAnimationPlayer:
    """"""
    def __init__(self) -> None:
        ...
    async def addTagForAnimations(self) -> None:
        """"""
        ...
    async def declarePathForTags(self) -> None:
        """"""
        ...
    async def reset(self) -> None:
        """"""
        ...
    async def run(self) -> None:
        """"""
        ...
    @overload
    async def runTag(self) -> None:
        """"""
        ...
    @overload
    async def runTag(self) -> None:
        """"""
        ...