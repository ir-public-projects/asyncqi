from typing import Any, overload
from typing_extensions import deprecated
class ALFileManager:
    """ALFileManager manages the user files stored in a shared folder.
    Note that FileManager starts to look in the shared folder, and if it does not find anything,
    then it looks in the data folder.
    Shared folder can be changed on the fly, and module will then be able to say which
    files are available in this folder, as well as return their complete path.
    """
    def __init__(self) -> None:
        ...
    async def setUserSharedFolderPath(self, fileName: str) -> None:
        """Set a new value of the user shared folder path.

        Args:
            fileName (str): Name of the module associate to the preference.
        """
        ...
    async def getUserSharedFolderPath(self) -> str:
        """Get the current user shared folder path.

        Returns:
            str: User shared folder path string.
        """
        ...
    async def getSystemSharedFolderPath(self) -> str:
        """Get the current system shared folder path.

        Returns:
            str: System shared folder path string.
        """
        ...
    async def fileExists(self, fileName: str) -> bool:
        """Try to find if this file does exist on robot or not.

        Args:
            fileName (str): Name of the module associate to the preference.

        Returns:
            bool: True upon success
        """
        ...
    async def dataFileExists(self, fileName: str) -> bool:
        """Try to find if this file does exist on robot or not.

        Args:
            fileName (str): Name of the module associate to the preference.

        Returns:
            bool: True upon success
        """
        ...
    async def getFileCompletePath(self, prefs: str) -> str:
        """Returns the complete path of the file if it does exist. Starts by looking in user's shared folder, then in system folder.

        Args:
            prefs (str): array reprenting the whole file.

        Returns:
            str: True upon success
        """
        ...
    async def getFileContents(self, prefs: str) -> Any:
        """Returns the complete path of the file if it does exist. Starts by looking in user's shared folder, then in system folder.

        Args:
            prefs (str): array reprenting the whole file.

        Returns:
            Any: True upon success
        """
        ...