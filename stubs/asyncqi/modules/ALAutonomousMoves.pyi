from typing import Any, overload
from typing_extensions import deprecated
class ALAutonomousMoves:
    """Module that manage the background moves automatically started by the robot."""
    def __init__(self) -> None:
        ...
    @deprecated('No reason provided')
    async def startSmallDisplacements(self) -> None:
        """DEPRECATED since 2.0: do ALBasicAwareness.setTrackingMode("MoveContextually") instead.Start small base moves."""
        ...
    @deprecated('No reason provided')
    async def stopSmallDisplacements(self) -> None:
        """DEPRECATED since 2.0: do ALBasicAwareness.setTrackingMode instead.Stop small base moves."""
        ...
    @deprecated('No reason provided')
    async def setExpressiveListeningEnabled(self, enable: bool) -> None:
        """DEPRECATED since 2.4: Call ALListeningMovement.setEnabled(bool) instead.Enable or disable the listening movements.

        Args:
            enable (bool): The boolean value: true to enable, false to disable.
        """
        ...
    @deprecated('No reason provided')
    async def getExpressiveListeningEnabled(self) -> bool:
        """DEPRECATED since 2.4: Call ALListeningMovement.isEnabled() instead.If listening movements are enabled.

        Returns:
            bool: The boolean value: true means it is enabled, false means it is disabled.
        """
        ...
    async def setBackgroundStrategy(self, strategy: str) -> None:
        """The background strategy.

        Args:
            strategy (str): The autonomous background posture strategy. ("none" or "backToNeutral")
        """
        ...
    async def getBackgroundStrategy(self) -> str:
        """Gets the background strategy.

        Returns:
            str: The autonomous background posture strategy. ("none" or "backToNeutral")
        """
        ...