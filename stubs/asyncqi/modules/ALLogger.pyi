from typing import Any, overload
from typing_extensions import deprecated
class ALLogger:
    """This module allows you log errors, warnings, and info stdout or a file. The verbosity level allow you to  customize the output."""
    def __init__(self) -> None:
        ...
    @deprecated('Use qiLogFatal instead. ')
    async def fatal(self, moduleName: str, message: str) -> None:
        """DEPRECATED. Use qiLogFatal instead. 
         Log a fatal error.

        Args:
            moduleName (str): Name of the module.
            message (str): Log Message.
        """
        ...
    @deprecated('Use qiLogError instead. ')
    async def error(self, moduleName: str, message: str) -> None:
        """DEPRECATED. Use qiLogError instead. 
         Log an error.

        Args:
            moduleName (str): Name of the module.
            message (str): Log Message.
        """
        ...
    @deprecated('DEPRECATED: use qiLogWarning instead. Log a warning.')
    async def warn(self, moduleName: str, message: str) -> None:
        """DEPRECATED: use qiLogWarning instead. Log a warning.

        Args:
            moduleName (str): Name of the module.
            message (str): Log Message.
        """
        ...
    @deprecated('Use qiLogInfo instead. ')
    async def info(self, moduleName: str, message: str) -> None:
        """DEPRECATED. Use qiLogInfo instead. 
         Log a info message.

        Args:
            moduleName (str): Name of the module.
            message (str): Log Message.
        """
        ...
    @deprecated('Use qiLogDebug instead. ')
    async def debug(self, moduleName: str, message: str) -> None:
        """DEPRECATED. Use qiLogDebug instead. 
         Log a debug message.

        Args:
            moduleName (str): Name of the module.
            message (str): Log Message.
        """
        ...
    async def setVerbosity(self) -> None:
        """Removed: not implemented anymore."""
        ...
    async def verbosity(self) -> int:
        """Removed: not implemented anymore.

        Returns:
            int: No description provided
        """
        ...
    async def logInFile(self) -> None:
        """Removed: not implemented anymore."""
        ...
    async def logInForwarder(self) -> None:
        """Removed: not implemented anymore."""
        ...
    async def removeHandler(self) -> None:
        """Removed: not implemented anymore."""
        ...