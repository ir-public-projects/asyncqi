from typing import Any, overload
from typing_extensions import deprecated
class ALRobotPosture:
    """Use ALRobotPosture module to make the robot go tothe asked posture."""
    def __init__(self) -> None:
        ...
    async def getPostureFamily(self) -> str:
        """Returns the posture family for example Standing, LyingBelly,...

        Returns:
            str: Returns the posture family, e.g. Standing.
        """
        ...
    async def goToPosture(self, postureName: str, maxSpeedFraction: float) -> bool:
        """Make the robot go to the choosenposture.

        Args:
            postureName (str): Name of the desired posture. Use getPostureList to get the list of posture name available.
            maxSpeedFraction (float): A fraction.

        Returns:
            bool: Returns if the posture was reached or not.
        """
        ...
    async def applyPosture(self, postureName: str, maxSpeedFraction: float) -> bool:
        """Set the angle of the joints of the  robot to the choosen posture.

        Args:
            postureName (str): Name of the desired posture. Use getPostureList to get the list of posture name available.
            maxSpeedFraction (float): A fraction.

        Returns:
            bool: Returns if the posture was reached or not.
        """
        ...
    async def stopMove(self) -> None:
        """Stop the posture move."""
        ...
    async def getPostureList(self) -> list[str]:
        """Get the list of posture names available.

        Returns:
            list[str]: No description provided
        """
        ...
    async def getPostureFamilyList(self) -> list[str]:
        """Get the list of posture family names available.

        Returns:
            list[str]: No description provided
        """
        ...
    async def setMaxTryNumber(self, pMaxTryNumber: int) -> None:
        """Set maximum of tries ongoToPosture fail.

        Args:
            pMaxTryNumber (int): Number of retry if goToPosture fail.
        """
        ...
    async def getPosture(self) -> str:
        """Determine posture.

        Returns:
            str: No description provided
        """
        ...