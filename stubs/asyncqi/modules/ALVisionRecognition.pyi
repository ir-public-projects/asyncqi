from typing import Any, overload
from typing_extensions import deprecated
class ALVisionRecognition:
    """ALVisionRecognition is a module which detects and recognizes learned pictures, like pages of a comic books, faces of objects or even locations.
    The learning stage is done using the Choregraphe interface. Follow the steps in the green doc that will explain how to create your own database.
    The output value is written in ALMemory in the PictureDetected variable.
    It contains an array of tags, with the following format: 
 
    [ [ TimeStampField ] [ Picture_info_0 , Picture _info_1, . . . , Picture_info_N-1 ] ] 
 
    with as many Picture_info tags as things currently recognized. 
    Picture_info = [[labels_list], matched_keypoints, ratio, [boundary_points]] 
    with labels_list = [label_0, label_1, ..., label_N-1] and label_n belongs to label_n+1 
    and boundary_points = [[x0,y0], [x1,y1], ..., [xN,yN]] 
 
    - Labels are the names given to the picture (e.g. "cover/my book", or "fridge corner/kitchen/my flat"). 
    - matched_keypoints corresponds to the number of keypoints retrieved in the current frame. 
    - ratio represents the number of keypoints found for the object in the current frame divided by the number of keypoints found during the learning stage. 
    - boundary_points is a list of points coordinates in angle values representing the reprojection in the current image of the boundaries selected during the learning stage. 

    """
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    @overload
    async def setFrameRate(self, subscriberName: str, framerate: int) -> bool:
        """Sets the extractor framerate for a chosen subscriber

        Args:
            subscriberName (str): Name of the subcriber
            framerate (int): New framerate

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    @overload
    async def setFrameRate(self, framerate: int) -> bool:
        """Sets the extractor framerate for all the subscribers

        Args:
            framerate (int): New framerate

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def setResolution(self, resolution: int) -> bool:
        """Sets extractor resolution

        Args:
            resolution (int): New resolution

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def setActiveCamera(self, cameraId: int) -> bool:
        """Sets extractor active camera

        Args:
            cameraId (int): Id of the camera that will become the active camera

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    @deprecated('DEPRECATED: Sets pause and resolution')
    async def setParameter(self, paramName: str, value: Any) -> None:
        """DEPRECATED: Sets pause and resolution

        Args:
            paramName (str): Name of the parameter to set
            value (Any): New value
        """
        ...
    async def getFrameRate(self) -> int:
        """Gets extractor framerate

        Returns:
            int: Current value of the framerate of the extractor
        """
        ...
    async def getResolution(self) -> int:
        """Gets extractor resolution

        Returns:
            int: Current value of the resolution of the extractor
        """
        ...
    async def getActiveCamera(self) -> int:
        """Gets extractor active camera

        Returns:
            int: Id of the current active camera of the extractor
        """
        ...
    async def isPaused(self) -> bool:
        """Gets extractor pause status

        Returns:
            bool: True if the extractor is paused, False if not
        """
        ...
    async def isProcessing(self) -> bool:
        """Gets extractor running status

        Returns:
            bool: True if the extractor is currently processing images, False if not
        """
        ...
    async def pause(self, paused: bool) -> None:
        """Changes the pause status of the extractor

        Args:
            paused (bool): New pause satus
        """
        ...
    async def changeDatabase(self, databasePath: str, databaseName: str) -> bool:
        """By default the database has the name "current" and is on the robot in /home/nao/naoqi/share/naoqi/vision/visionrecognition/ folder. This bound method allows to choose both another name and another folder for the database. 


        Args:
            databasePath (str): Absolute path of the database on the robot, or "" to set default path.
            databaseName (str): Name of the database folder, or "" to set default database folder.

        Returns:
            bool: True if the operation succeded, false otherwise.
        """
        ...
    async def clearCurrentDatabase(self) -> None:
        """Clear the current database, the user has to be warned before calling this function."""
        ...
    @deprecated('No reason provided')
    async def setParam(self, paramName: str, paramValue: Any) -> None:
        """Set vision recognition parameters (deprecated in 1.22)

        Args:
            paramName (str): Name of the parameter to be changed. Only "resolution" can be used.
            paramValue (Any): Value of the resolution as an integer: 0(QQVGA) 1(QVGA) 2(VGA)
        """
        ...
    async def getParam(self, paramName: str) -> Any:
        """Get some vision recognition parameters.

        Args:
            paramName (str): The name of the parameter to get. "db_path" and "db_name" can be used.

        Returns:
            Any: Value of the parameter as a string for "db_path" and "db_name"
        """
        ...
    async def learnFromFile(self, filename: str, name: str, tags: list[str], isWholeImage: bool, forced: bool) -> bool:
        """Load an image and interpret it as an object.

        Args:
            filename (str): The filename of the image that will be interpreted as a planar object.
            name (str): The name of the object (used as a unique identifier).
            tags (list[str]): A list of tags (as strings) containing any met-data about your object.
            isWholeImage (bool): indicates if the object occupies the whole image. If set to false, visionrecognition will try to detect the border of the object automatically. This works with unicolor background where object stands out well from the background. By default, this is set to true.
            forced (bool): indicates if learned object will replace existing object (having the same original name) if any.

        Returns:
            bool: True if the operation succeded, false otherwise.
        """
        ...
    async def setMaxOutObjs(self, iMaxOutObjs: int) -> None:
        """Set the maximal number (not more than 10) of detected objects for each detection. By default, this is set to 1.

        Args:
            iMaxOutObjs (int): number of desired objects to be detected.
        """
        ...
    async def getMaxOutObjs(self) -> int:
        """Get the maximal number of detected objects for each detection.

        Returns:
            int: number of maximal objects to be detected.
        """
        ...
    async def getSize(self) -> int:
        """Get number of objects in the current database.

        Returns:
            int: number of objects in the current database.
        """
        ...
    async def detectFromFile(self, image: str) -> None:
        """Load an image and search for known objects.

        Args:
            image (str): The image that will be searched for known objects.
        """
        ...
    async def getDefaultDatabaseDirectory(self) -> str:
        """Return the default directory used for databases storage.

        Returns:
            str: Default directory used for databases storage.
        """
        ...
    async def getDefaultDatabaseName(self) -> str:
        """Return the default database name.

        Returns:
            str: Default database name.
        """
        ...
    async def sendDatabase(self, name: str, file: Any) -> None:
        """Upload a zipped database to the robot.

        Args:
            name (str): Database name.
            file (Any): Archive (ZIP) containing the database file.
        """
        ...