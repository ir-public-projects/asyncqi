from typing import Any, overload
from typing_extensions import deprecated
class ALDialog:
    """ALDialog is the dialog module. It allows loading a dialog file (.top), starts/stops/loads/unloads the dialog"""
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getStoppable(self) -> bool:
        """Is engine stoppable

        Returns:
            bool: Is engine stoppable
        """
        ...
    async def setStoppable(self, stoppable: bool) -> None:
        """Is engine stoppable

        Args:
            stoppable (bool): set if engine can be stopped by user session
        """
        ...
    async def runTopics(self, stoppable: list[str]) -> list[str]:
        """Is engine stoppable

        Args:
            stoppable (list[str]): set if engine can be stopped by user session

        Returns:
            list[str]: No description provided
        """
        ...
    async def stopTopics(self, stoppable: list[str]) -> None:
        """Is engine stoppable

        Args:
            stoppable (list[str]): set if engine can be stopped by user session
        """
        ...
    async def sayError(self, enable: list[str]) -> None:
        """Say an error

        Args:
            enable (list[str]): true to pause
        """
        ...
    async def say(self, stoppable: str) -> None:
        """say a sentence from a topic

        Args:
            stoppable (str): set if engine can be stopped by user session
        """
        ...
    async def resetLanguage(self) -> None:
        """ResetLanguage"""
        ...
    async def addBlockingEvent(self, eventName: str) -> None:
        """The event will stop current TSS

        Args:
            eventName (str): Event name
        """
        ...
    async def removeBlockingEvent(self, eventName: str) -> None:
        """The event will removed from the blocking list

        Args:
            eventName (str): Event name
        """
        ...
    async def wordsRecognizedCallback(self, grammar: Any, utterance_Size: int) -> None:
        """Asr callback for recognized words

        Args:
            grammar (Any): recognized grammar
            utterance_Size (int): Utterance size
        """
        ...
    async def endOfUtteranceCallback(self) -> bool:
        """End of utterance asr callback

        Returns:
            bool: true if reprocess buffer
        """
        ...
    async def gotoTag(self, topicName: str, tagName: str) -> None:
        """Callback when ASR status changes

        Args:
            topicName (str): topic name
            tagName (str): tag name
        """
        ...
    async def noPick(self, topicName: str) -> None:
        """noPick

        Args:
            topicName (str): Topic name
        """
        ...
    async def compileAll(self) -> None:
        """compile all for ASR"""
        ...
    async def compileBundle(self) -> None:
        """compile all for ASR"""
        ...
    async def createContext(self) -> None:
        """Create a context"""
        ...
    async def loadTopic(self, topicPath: str) -> str:
        """Load a topic

        Args:
            topicPath (str): topic full path and filename

        Returns:
            str: Topic path and filename
        """
        ...
    async def loadTopicContent(self, topicContent: str) -> str:
        """Load a topic

        Args:
            topicContent (str): topic content

        Returns:
            str: Topic name
        """
        ...
    async def loadTopicContentIntoBundleWithContext(self, topicContent: Any) -> str:
        """Load a topic

        Args:
            topicContent (Any): topic content

        Returns:
            str: Topic name
        """
        ...
    async def deactivateTopic(self, topicName: str) -> None:
        """Activate a topic

        Args:
            topicName (str): topic name
        """
        ...
    async def activateTopic(self, topicName: str) -> None:
        """Activate a topic

        Args:
            topicName (str): topic name
        """
        ...
    async def activateTopicInBundle(self, topicName: str, bundleName: str) -> None:
        """Activate a topic in the specified bundle

        Args:
            topicName (str): topic name
            bundleName (str): bundle name
        """
        ...
    async def deactivateTopicInBundle(self, topicName: str, bundleName: str) -> None:
        """Dectivate a topic in the specified bundle

        Args:
            topicName (str): topic name
            bundleName (str): bundle name
        """
        ...
    async def unloadTopic(self, topicName: str) -> None:
        """unload a dialog

        Args:
            topicName (str): topic name
        """
        ...
    async def unloadTopicFromBundle(self, topicName: str, bundleName: str) -> None:
        """unload a dialog

        Args:
            topicName (str): topic name
            bundleName (str): bundle name
        """
        ...
    async def forceOutput(self) -> None:
        """Get a proposal"""
        ...
    async def forceInput(self, input: str) -> None:
        """Give a sentence to the dialog and get the answer

        Args:
            input (str): input string that simulate humain sentence
        """
        ...
    async def tell(self, input: str) -> None:
        """Give a sentence to the dialog and get the answer

        Args:
            input (str): input string that simulate humain sentence
        """
        ...
    async def setASRConfidenceThreshold(self, threshold: float) -> None:
        """Set the minimum confidence required to recognize words. Better to use confidence by asr model

        Args:
            threshold (float): input string that simulate humain sentence
        """
        ...
    async def getASRConfidenceThreshold(self) -> float:
        """Get the minimum confidence required to recognize words

        Returns:
            float: current asr confidence
        """
        ...
    @overload
    async def setConfidenceThreshold(self, strategy: str, confidence: float) -> None:
        """Set the confidence threshold

        Args:
            strategy (str): BNF / SLM / REMOTE
            confidence (float): desired confidence threshold \\[0, 1\\]
        """
        ...
    @overload
    async def setConfidenceThreshold(self, strategy: str, confidence: float, language: str) -> None:
        """Set the confidence threshold

        Args:
            strategy (str): BNF / SLM / REMOTE
            confidence (float): desired confidence threshold \\[0, 1\\]
            language (str): language for which we set the threshold
        """
        ...
    async def getAllConfidenceThresholds(self) -> dict[str, dict[str, float]]:
        """Get all the confidence thresholds

        Returns:
            dict[str, dict[str, float]]: No description provided
        """
        ...
    async def getConfidenceThreshold(self) -> float:
        """Get all the confidence thresholds

        Returns:
            float: No description provided
        """
        ...
    async def removeAllLanguageThresholds(self) -> None:
        """Remove all language specific confidence thresholds"""
        ...
    async def openSession(self, id: int) -> None:
        """Open a session

        Args:
            id (int): user id
        """
        ...
    async def closeSession(self) -> None:
        """Close the current session"""
        ...
    async def closeTestSession(self) -> None:
        """Close the test session"""
        ...
    async def setDelay(self, eventName: str, Delay: int) -> None:
        """change event's delay

        Args:
            eventName (str): Event name
            Delay (int): Delay in second
        """
        ...
    async def setNumberOfScopes(self, numberOfScope: int) -> None:
        """Set how many scopes remains open

        Args:
            numberOfScope (int): number of scope
        """
        ...
    @overload
    async def setConcept(self, conceptName: str, language: str, content: list[str]) -> None:
        """Set the content of a dynamic concept

        Args:
            conceptName (str): Name of the concept
            language (str): Language of the concept
            content (list[str]): content of the concept
        """
        ...
    @overload
    async def setConcept(self, conceptName: str, language: str, content: list[str], store: bool) -> None:
        """Set the content of a dynamic concept

        Args:
            conceptName (str): Name of the concept
            language (str): Language of the concept
            content (list[str]): content of the concept
            store (bool): determine if the concept will be save in the database
        """
        ...
    async def setConceptKeepInCache(self, conceptName: str, language: str, content: list[str]) -> None:
        """set the content of a dynamic concept

        Args:
            conceptName (str): concept name
            language (str): language
            content (list[str]): concept content
        """
        ...
    async def setConceptInBundle(self, conceptName: str, bundleName: str, conceptContent: list[str]) -> None:
        """Set the content of a dynamic concept in specific bundle (non persistent)

        Args:
            conceptName (str): The concept name
            bundleName (str): The bundle name
            conceptContent (list[str]): The new concept content
        """
        ...
    async def addToConcept(self, conceptName: str, language: str, content: list[str]) -> None:
        """add to the content of a dynamic concept

        Args:
            conceptName (str): Name of the concept
            language (str): Language of the concept
            content (list[str]): content of the concept
        """
        ...
    async def getConcept(self, conceptName: str, language: str) -> list[str]:
        """get the content of a dynamic concept

        Args:
            conceptName (str): Name of the concept
            language (str): Language of the concept

        Returns:
            list[str]: No description provided
        """
        ...
    async def enableTriggerSentences(self, enableTriggerSentences: bool) -> None:
        """enableTriggerSentences

        Args:
            enableTriggerSentences (bool): Enable trigger sentences if true
        """
        ...
    async def enableCategory(self, enableCategory: bool) -> None:
        """enableCategory

        Args:
            enableCategory (bool): Enable category if true
        """
        ...
    async def startPush(self) -> None:
        """Start push mode"""
        ...
    async def stopPush(self) -> None:
        """Stop push mode"""
        ...
    async def setAnimatedSpeechConfiguration(self, animatedSpeechConfiguration: dict[str, Any]) -> None:
        """Set the configuration of animated speech for the current dialog.

        Args:
            animatedSpeechConfiguration (dict[str, Any]): See animated speech documentation
        """
        ...
    async def getAnimatedSpeechConfiguration(self) -> Any:
        """Get the configuration of animated speech for the current dialog.

        Returns:
            Any: See animated speech documentation
        """
        ...
    async def applicationBlackList(self, applicationList: list[str]) -> None:
        """Black list a list of application

        Args:
            applicationList (list[str]): List of applications that cannot be launched by dialog
        """
        ...
    async def isContentNeedsUpdate(self) -> bool:
        """True if new content was installed

        Returns:
            bool: True if content was updated since last compilation
        """
        ...
    async def exportContent(self) -> list[str]:
        """Export shared topics

        Returns:
            list[str]: No description provided
        """
        ...
    async def runDialog(self) -> None:
        """run main dialog"""
        ...
    async def stopDialog(self) -> None:
        """stop main dialog"""
        ...
    async def setVariablePath(self, topic: str, event: str, path: str) -> None:
        """setVariablePath redifine a variable name on the fly

        Args:
            topic (str): Source topic name
            event (str): Event name
            path (str): New event name
        """
        ...
    async def setLanguage(self, Language: str) -> None:
        """setLanguage

        Args:
            Language (str): Set dialog language (frf, enu, jpj...)
        """
        ...
    async def getLanguage(self) -> str:
        """getLanguage

        Returns:
            str: get the dialog language
        """
        ...
    async def dialogAnswered(self, variableName: str, variableValue: Any, message: str) -> None:
        """dialogAnswered

        Args:
            variableName (str): variable name
            variableValue (Any): variable value
            message (str): message
        """
        ...
    async def setFocus(self, topicName: str) -> None:
        """Give focus to a dialog

        Args:
            topicName (str): Topic name
        """
        ...
    async def getFocus(self) -> str:
        """Give focus to a dialog

        Returns:
            str: Current focus name
        """
        ...
    async def gotoTopic(self, topicName: str) -> None:
        """Set the focus to a topic and make a proposal

        Args:
            topicName (str): Topic name
        """
        ...
    async def addFallback(self, language: str, name: str) -> None:
        """Add a fallback plugin

        Args:
            language (str): The language of the plugin
            name (str): The name of the plugin
        """
        ...
    async def removeFallback(self, language: str, name: str) -> None:
        """Remove a fallback plugin

        Args:
            language (str): The language of the plugin
            name (str): The name of the plugin
        """
        ...
    async def getLoadedTopics(self, language: str) -> list[str]:
        """List loaded topics

        Args:
            language (str): Language name

        Returns:
            list[str]: List of loaded topics
        """
        ...
    async def getAllLoadedTopics(self) -> list[str]:
        """List loaded topics independent of language

        Returns:
            list[str]: List of loaded topics
        """
        ...
    async def getActivatedTopics(self) -> list[str]:
        """Get activated topics

        Returns:
            list[str]: List of activated topics
        """
        ...
    async def activateTag(self, tagName: str, topicName: str) -> None:
        """activate a tag

        Args:
            tagName (str): tag name
            topicName (str): topic name
        """
        ...
    async def deactivateTag(self, tagName: str, topicName: str) -> None:
        """deactivate a tag

        Args:
            tagName (str): tag name
            topicName (str): topic name
        """
        ...
    async def resetAll(self) -> None:
        """Reset all engine"""
        ...
    async def insertUserData(self, variableName: str, variableValue: str, UserID: int) -> None:
        """insert user data into dialog database

        Args:
            variableName (str): Variable name
            variableValue (str): Variable value
            UserID (int): User ID
        """
        ...
    async def getUserData(self, variableName: str, UserID: int) -> str:
        """get user data from dialog database

        Args:
            variableName (str): Variable name
            UserID (int): User ID

        Returns:
            str: Value
        """
        ...
    async def getUserDataList(self, UserID: int) -> list[str]:
        """get user data list from dialog database

        Args:
            UserID (int): User ID

        Returns:
            list[str]: Variable list
        """
        ...
    async def getUserList(self) -> list[int]:
        """get user list from dialog database

        Returns:
            list[int]: User list
        """
        ...
    async def removeUserData(self, UserID: int) -> None:
        """remove a user from the database

        Args:
            UserID (int): User ID
        """
        ...
    async def clearConcepts(self) -> None:
        """clear concepts in DB"""
        ...
    async def enableSendingLogToCloud(self, EnableLog: bool) -> None:
        """let the robot send log the cloud

        Args:
            EnableLog (bool): Enable log
        """
        ...
    async def isSendingLogToCloud(self) -> bool:
        """check if the robot is sending the log to the cloud

        Returns:
            bool: True if currently logging
        """
        ...
    async def enableLogAudio(self) -> None:
        """enable sending log audio (recorded conversation) to the cloud"""
        ...
    async def mute(self) -> None:
        """mute dialog"""
        ...
    @overload
    async def generateSentences(self, destination: str, topic: str, language: str) -> None:
        """Generate sentences

        Args:
            destination (str): file destination
            topic (str): source topic
            language (str): source language
        """
        ...
    @overload
    async def generateSentences(self, destination: str, topic: str, language: str, sampling: int) -> None:
        """Generate sentences

        Args:
            destination (str): file destination
            topic (str): source topic
            language (str): source language
            sampling (int): The number of generated rules (-1= all the rules)
        """
        ...
    async def enableSimulatedApps(self, simulateApps: bool) -> None:
        """Define if applications will be launched or not

        Args:
            simulateApps (bool): set simulated apps
        """
        ...
    async def getGlobalRecommandations(self) -> list[str]:
        """Get recommandations

        Returns:
            list[str]: No description provided
        """
        ...
    async def getLocalRecommandations(self) -> list[str]:
        """Get recommandations

        Returns:
            list[str]: No description provided
        """
        ...
    async def getFocusRecommandations(self) -> list[str]:
        """Get recommandations

        Returns:
            list[str]: No description provided
        """
        ...
    async def setVariableValue(self, vName: str, value: str) -> None:
        """Set a new value for a QiChatVariable

        Args:
            vName (str): The name of the QiChat variable
            value (str): The new value of the QiChat variable
        """
        ...
    async def getVariableValue(self, vName: str) -> str:
        """Get the current value of a QiChat variable

        Args:
            vName (str): The name of the QiChat variable to retrieve

        Returns:
            str: The current value of the QiChat variable
        """
        ...
    async def getBookmarkActivationStatus(self, bName: str, bTopic: str) -> bool:
        """Indicates whether the bookmark is activated

        Args:
            bName (str): The name of the bookmark
            bTopic (str): The name of the corresponding topic

        Returns:
            bool: True if the bookmark is activated
        """
        ...
    async def loadTopicContentIntoBundle(self, topicContent: str, bundleName: str, newSDK: bool, forcedLanguage: str) -> str:
        """Load a topic

        Args:
            topicContent (str): topic content
            bundleName (str): name of the bundle
            newSDK (bool): Whether to load the topic with the new SDK parser
            forcedLanguage (str): if the topic was loaded with the new SDK Parser, specify the language to use

        Returns:
            str: Topic name
        """
        ...
    async def goToBookmark(self, name: str, topic: str, failIfNotReached: bool) -> bool:
        """Go to the bookmark named 'name' in the topic 'topic'

        Args:
            name (str): The name of the bookmark
            topic (str): The topic we need to find the bookmark in
            failIfNotReached (bool): If true, will not match the rules bookmarked if the tag can't be reached

        Returns:
            bool: Whether the bookmark was reached in the rule
        """
        ...
    async def getTopicDescription(self, topicName: str, bundle: str, language: str) -> str:
        """Get the description of the corresponding topic

        Args:
            topicName (str): The name of the topic
            bundle (str): The name of the bundle
            language (str): language of the topic

        Returns:
            str: The description of the topic
        """
        ...
    async def isListening(self) -> bool:
        """Get the description of the corresponding topic

        Returns:
            bool: No description provided
        """
        ...
    async def isDynamicConcept(self, conceptName: str, language: str, bundleName: str) -> bool:
        """Indicates whether the concept with the given name is dynamic

        Args:
            conceptName (str): The name of the concept
            language (str): The language of the concept
            bundleName (str): The name of the bundle

        Returns:
            bool: True if the concept is dynamic
        """
        ...
    async def compileBundleWithConceptErrorCheck(self, bundleName: str) -> None:
        """Compiles the bundle with the given name, and checks that all used concepts are defined

        Args:
            bundleName (str): The name of the bundle
        """
        ...
    async def getConceptInBundle(self, conceptName: str, bundleName: str, language: str) -> list[str]:
        """Get the concept named conceptName from the bundle named bundleName in the given language

        Args:
            conceptName (str): The name of the concept
            bundleName (str): The name of the bundle
            language (str): The language of the concept

        Returns:
            list[str]:  a vector of strings representing the concept's contents
        """
        ...