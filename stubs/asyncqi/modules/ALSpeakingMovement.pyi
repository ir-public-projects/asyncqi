from typing import Any, overload
from typing_extensions import deprecated
class ALSpeakingMovement:
    """"""
    def __init__(self) -> None:
        ...
    async def setEnabled(self) -> None:
        """"""
        ...
    async def isEnabled(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def setMode(self) -> None:
        """"""
        ...
    async def getMode(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def addTagsToWords(self) -> None:
        """"""
        ...
    async def resetTagsToWords(self) -> None:
        """"""
        ...