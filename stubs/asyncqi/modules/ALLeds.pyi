from typing import Any, overload
from typing_extensions import deprecated
class ALLeds:
    """This module allows you to control NAO's LEDs. It provides simple ways of setting or fading the intensity of single LEDs and groups of LEDs. 
    Groups of LEDs typically include face LEDs, ear LEDs etc. It is also possible to control each LED separately (for example, each of the 8 LEDs in one NAO's eyes).
    There are three primary colors of LEDs available - red, green and blue, so it is possible to combine them to obtain different colors. The ears contain blue LEDs only.
    It is possible to control the LED's intensity (between 0 and 100%).
    """
    def __init__(self) -> None:
        ...
    async def createGroup(self, groupName: str, ledNames: list[str]) -> None:
        """Makes a group name for ease of setting multiple LEDs.

        Args:
            groupName (str): The name of the group.
            ledNames (list[str]): A vector of the names of the LEDs in the group.
        """
        ...
    async def earLedsSetAngle(self, degrees: int, duration: float, leaveOnAtEnd: bool) -> None:
        """An animation to show a direction with the ears.

        Args:
            degrees (int): The angle you want to show in degrees (int). 0 is up, 90 is forwards, 180 is down and 270 is back.
            duration (float): The duration in seconds of the animation.
            leaveOnAtEnd (bool): If true the last led is left on at the end of the animation.
        """
        ...
    async def fade(self, name: str, intensity: float, duration: float) -> None:
        """Sets the intensity of a LED or Group of LEDs within a given time.

        Args:
            name (str): The name of the LED or Group.
            intensity (float): The intensity of the LED or Group (a value between 0 and 1).
            duration (float): The duration of the fade in seconds
        """
        ...
    async def fadeListRGB(self, name: str, rgbList: Any, timeList: list[float]) -> None:
        """Chain a list of color for a device, as the motion.doMove command.

        Args:
            name (str): The name of the LED or Group.
            rgbList (Any): List of RGB led value, RGB as seen in hexa-decimal: 0x00RRGGBB.
            timeList (list[float]): List of time to go to given intensity.
        """
        ...
    @overload
    async def fadeRGB(self, name: str, red: float, green: float, blue: float, duration: float) -> None:
        """Sets the color of an RGB led.

        Args:
            name (str): The name of the LED or Group.
            red (float): the intensity of red channel (0-1).
            green (float): the intensity of green channel (0-1).
            blue (float): the intensity of blue channel (0-1).
            duration (float): Time used to fade in seconds.
        """
        ...
    @overload
    async def fadeRGB(self, name: str, colorName: str, duration: float) -> None:
        """Sets the color of an RGB led.

        Args:
            name (str): The name of the LED or Group.
            colorName (str): the name of the color (supported colors: "white", "red", "green", "blue", "yellow", "magenta", "cyan")
            duration (float): Time used to fade in seconds.
        """
        ...
    @overload
    async def fadeRGB(self, name: str, rgb: int, duration: float) -> None:
        """Sets the color of an RGB led.

        Args:
            name (str): The name of the LED or Group.
            rgb (int): The RGB value led, RGB as seen in hexa-decimal: 0x00RRGGBB.
            duration (float): Time used to fade in seconds.
        """
        ...
    async def reset(self, name: str) -> None:
        """Resets the state of the leds to default (for ex, eye LEDs are white and fully on by default).

        Args:
            name (str): The name of the LED or Group (for now, only "AllLeds" are implemented).
        """
        ...
    async def getIntensity(self, name: str) -> Any:
        """Gets the intensity of a LED or device

        Args:
            name (str): The name of the LED or Group.

        Returns:
            Any: The intensity of the LED or Group.
        """
        ...
    async def listLEDs(self) -> list[str]:
        """Lists the short LED names.

        Returns:
            list[str]: A vector of LED names.
        """
        ...
    async def listLED(self, name: str) -> list[str]:
        """Lists the devices aliased by a short LED name.

        Args:
            name (str): The name of the LED to list

        Returns:
            list[str]: A vector of device names.
        """
        ...
    async def listGroup(self, groupName: str) -> list[str]:
        """Lists the devices in the group.

        Args:
            groupName (str): The name of the Group.

        Returns:
            list[str]: A vector of string device names.
        """
        ...
    async def listGroups(self) -> list[str]:
        """Lists available group names.

        Returns:
            list[str]: A vector of group names.
        """
        ...
    async def off(self, name: str) -> None:
        """Switch to a minimum intensity a LED or Group of LEDs.

        Args:
            name (str): The name of the LED or Group.
        """
        ...
    async def on(self, name: str) -> None:
        """Switch to a maximum intensity a LED or Group of LEDs.

        Args:
            name (str): The name of the LED or Group.
        """
        ...
    async def rasta(self, duration: float) -> None:
        """Launch a green/yellow/red rasta animation on all body.

        Args:
            duration (float): Approximate duration of the animation in seconds.
        """
        ...
    async def rotateEyes(self, rgb: int, timeForRotation: float, totalDuration: float) -> None:
        """Launch a rotation using the leds of the eyes.

        Args:
            rgb (int): the RGB value led, RGB as seen in hexa-decimal: 0x00RRGGBB.
            timeForRotation (float): Approximate time to make one turn.
            totalDuration (float): Approximate duration of the animation in seconds.
        """
        ...
    async def randomEyes(self, duration: float) -> None:
        """Launch a random animation in eyes

        Args:
            duration (float): Approximate duration of the animation in seconds.
        """
        ...
    async def setIntensity(self, name: str, intensity: float) -> None:
        """Sets the intensity of a LED or Group of LEDs.

        Args:
            name (str): The name of the LED or Group.
            intensity (float): The intensity of the LED or Group (a value between 0 and 1).
        """
        ...