from typing import Any, overload
from typing_extensions import deprecated
class ALVideoDevice:
    """ALVideoDevice, formerly called Video Input systemis architectured in order to provide every client related to vision, a direct access to raw images from video source, or an access to images transformed in the requested format.
      Extension name of the methods providing images depends on wether clients are local (dynamic library) or remote (executable).
    """
    def __init__(self) -> None:
        ...
    async def subscribeCamera(self, name: str, cameraIndex: int, resolution: int, colorSpace: int, fps: int) -> str:
        """

        Args:
            name (str): Name of the subscribing vision module
            cameraIndex (int): Camera requested.
            resolution (int): Resolution requested.{0=kQQVGA, 1=kQVGA, 2=kVGA, 3=k4VGA}
            colorSpace (int): Colorspace requested.{0=kYuv, 9=kYUV422, 10=kYUV, 11=kRGB, 12=kHSY, 13=kBGR}
            fps (int): Fps (frames per second) requested.{5, 10, 15, 30}

        Returns:
            str: Name under which the vision module is known from ALVideoDevice
        """
        ...
    async def subscribeCameras(self, name: str, cameraIndexes: Any, resolutions: Any, colorSpaces: Any, fps: int) -> str:
        """

        Args:
            name (str): Name of the subscribing vision module
            cameraIndexes (Any): Cameras requested.
            resolutions (Any): Resolutions requested.{0=kQQVGA, 1=kQVGA, 2=kVGA, 3=k4VGA}
            colorSpaces (Any): Colorspaces requested.{0=kYuv, 9=kYUV422, 10=kYUV, 11=kRGB, 12=kHSY, 13=kBGR}
            fps (int): Fps (frames per second) requested.{5, 10, 15, 30}

        Returns:
            str: Name under which the vision module is known from ALVideoDevice
        """
        ...
    async def unsubscribe(self, nameId: str) -> bool:
        """

        Args:
            nameId (str): Name under which the vision module is known from ALVideoDevice.

        Returns:
            bool: True if success, false otherwise
        """
        ...
    async def getSubscribers(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    async def getCameraIndexes(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def getActiveCamera(self) -> int:
        """Tells which camera is the default one

        Returns:
            int:  0: top camera - 1: bottom camera
        """
        ...
    @overload
    async def setActiveCamera(self, activeCamera: int) -> bool:
        """Set the active camera

        Args:
            activeCamera (int):  0: top camera - 1: bottom camera

        Returns:
            bool: No description provided
        """
        ...
    async def getCameraModel(self, cameraIndex: int) -> int:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            int:  1(kOV7670): VGA camera - 2(kMT9M114): HD camera
        """
        ...
    async def isCameraSimulated(self, cameraIndex: int) -> bool:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            bool:  True: Camera is simulated - False: Camera is real
        """
        ...
    async def getCameraName(self, cameraIndex: int) -> str:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            str: CameraTop - CameraBottom - CameraDepth
        """
        ...
    @overload
    async def getFrameRate(self, cameraIndex: int) -> int:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            int: No description provided
        """
        ...
    @overload
    async def getResolution(self, cameraIndex: int) -> int:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            int: No description provided
        """
        ...
    @overload
    async def getColorSpace(self, cameraIndex: int) -> int:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            int: No description provided
        """
        ...
    async def getHorizontalFOV(self, cameraIndex: int) -> float:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            float: No description provided
        """
        ...
    async def getVerticalFOV(self, cameraIndex: int) -> float:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            float: No description provided
        """
        ...
    async def getParameterList(self, cameraIndex: int) -> list[int]:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            list[int]: No description provided
        """
        ...
    async def getParameter(self, cameraIndex: int, parameterId: int) -> int:
        """

        Args:
            cameraIndex (int): Camera requested.
            parameterId (int): Camera parameter requested.

        Returns:
            int: No description provided
        """
        ...
    async def getParameterRange(self, cameraIndex: int, parameterId: int) -> Any:
        """

        Args:
            cameraIndex (int): Camera requested.
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getParameterInfo(self, cameraIndex: int, parameterId: int) -> Any:
        """

        Args:
            cameraIndex (int): Camera requested.
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def setParameter(self, cameraIndex: int, parameterId: int, value: int) -> bool:
        """

        Args:
            cameraIndex (int): Camera requested.
            parameterId (int): Camera parameter requested.
            value (int): value requested.

        Returns:
            bool: No description provided
        """
        ...
    async def setParameterToDefault(self, cameraIndex: int, parameterId: int) -> bool:
        """

        Args:
            cameraIndex (int): Camera requested.
            parameterId (int): Camera parameter requested.

        Returns:
            bool: No description provided
        """
        ...
    async def setCameraCalibration(self, cameraIndex: int, filenames: list[str]) -> bool:
        """

        Args:
            cameraIndex (int): Camera requested.
            filenames (list[str]): Vector of files to retrieve in /media/internal/share/naoqi/vision or ~/.local/share/naoqi/vision

        Returns:
            bool: true if succeeded, false otherwise
        """
        ...
    async def setAllParametersToDefault(self, cameraIndex: int) -> bool:
        """

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            bool: No description provided
        """
        ...
    async def openCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def closeCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def isCameraOpen(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def startCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def stopCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def isCameraStarted(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def resetCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def hasDepthCamera(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getFrameRate(self, name: str) -> int:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            int: No description provided
        """
        ...
    async def setFrameRate(self, name: str, frameRate: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            frameRate (int): Frame Rate requested.

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getActiveCamera(self, name: str) -> int:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            int: No description provided
        """
        ...
    @overload
    async def setActiveCamera(self, name: str, cameraIndex: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            cameraIndex (int): Camera requested.

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getResolution(self, name: str) -> int:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            int: No description provided
        """
        ...
    async def setResolution(self, name: str, resolution: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            resolution (int): Resolution requested.

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getColorSpace(self, name: str) -> int:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            int: No description provided
        """
        ...
    async def setColorSpace(self, name: str, colorSpace: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            colorSpace (int): Color Space requested.

        Returns:
            bool: No description provided
        """
        ...
    async def getCameraParameterList(self, name: str) -> list[int]:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            list[int]: No description provided
        """
        ...
    async def getCameraParameter(self, name: str, parameterId: int) -> int:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            int: No description provided
        """
        ...
    async def getCameraParameterRange(self, name: str, parameterId: int) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getCameraParameterInfo(self, name: str, parameterId: int) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def setCameraParameter(self, name: str, parameterId: int, value: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.
            value (int): value requested.

        Returns:
            bool: No description provided
        """
        ...
    async def setCameraParameterToDefault(self, name: str, parameterId: int) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            bool: No description provided
        """
        ...
    async def setAllCameraParametersToDefault(self, name: str) -> bool:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            bool: No description provided
        """
        ...
    async def getImageLocal(self, name: str) -> Any:
        """Applies transformations to the last image from video source and returns a pointer to a locked ALImage.
        When image is not necessary anymore, a call to releaseImage() is requested.


        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: Pointer of the locked image buffer, NULL if error.Warning, image pointer is valid only for C++ dynamic library.
        """
        ...
    async def getImageRemote(self, name: str) -> Any:
        """Applies transformations to the last image from video source and fills pFrameOut.


        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: Array containing image informations :      \\[0\\] : width;     \\[1\\] : height;     \\[2\\] : number of layers;     \\[3\\] : ColorSpace;     \\[4\\] : time stamp (highest 32 bits);     \\[5\\] : time stamp (lowest 32 bits);     \\[6\\] : array of size height * width * nblayers containing image data;     \\[7\\] : cameraID;     \\[8\\] : left angle;     \\[9\\] : top angle;     \\[10\\] : right angle;     \\[11\\] : bottom angle; 
        """
        ...
    async def releaseImage(self, name: str) -> bool:
        """Release image buffer locked by getImageLocal().
        If G.V.M. had no locked image buffer, does nothing.

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            bool: true if success
        """
        ...
    async def getActiveCameras(self, name: str) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: No description provided
        """
        ...
    async def setActiveCameras(self, name: str, cameraIndexes: Any) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            cameraIndexes (Any): Cameras requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getResolutions(self, name: str) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: No description provided
        """
        ...
    async def setResolutions(self, name: str, resolutions: Any) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            resolutions (Any): Resolutions requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getColorSpaces(self, name: str) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: No description provided
        """
        ...
    async def setColorSpaces(self, name: str, colorSpaces: Any) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            colorSpaces (Any): Color Spaces requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getCamerasParameter(self, name: str, parameterId: int) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def setCamerasParameter(self, name: str, parameterId: int, values: Any) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.
            values (Any): values requested.

        Returns:
            Any: No description provided
        """
        ...
    async def setCamerasParameterToDefault(self, name: str, parameterId: int) -> Any:
        """

        Args:
            name (str): Name of the subscribing vision module
            parameterId (int): Camera parameter requested.

        Returns:
            Any: No description provided
        """
        ...
    async def getImagesLocal(self, name: str) -> Any:
        """Applies transformations to the last image from video source and returns a pointer to a locked ALImage.
        When image is not necessary anymore, a call to releaseImage() is requested.


        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: Array of pointer of the locked image buffer, NULL if error.Warning, image pointer is valid only for C++ dynamic library.
        """
        ...
    async def getImagesRemote(self, name: str) -> Any:
        """Applies transformations to the last image from video source and fills pFrameOut.


        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: Array containing image informations :      \\[0\\] : width;     \\[1\\] : height;     \\[2\\] : number of layers;     \\[3\\] : ColorSpace;     \\[4\\] : time stamp (highest 32 bits);     \\[5\\] : time stamp (lowest 32 bits);     \\[6\\] : array of size height * width * nblayers containing image data;     \\[7\\] : cameraID;     \\[8\\] : left angle;     \\[9\\] : top angle;     \\[10\\] : right angle;     \\[11\\] : bottom angle; 
        """
        ...
    async def releaseImages(self, name: str) -> Any:
        """Release image buffer locked by getImageLocal().
        If G.V.M. had no locked image buffer, does nothing.

        Args:
            name (str): Name of the subscribing vision module

        Returns:
            Any: true if success
        """
        ...
    async def getAngularPositionFromImagePosition(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def getImagePositionFromAngularPosition(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def getAngularSizeFromImageSize(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def getImageSizeFromAngularSize(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def getImageInfoFromAngularInfo(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def getImageInfoFromAngularInfoWithResolution(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def putImage(self, cameraIndex: int, timeStamp: Any, width: int, height: int, colorSpace: int, imageBuffer: Any) -> bool:
        """Allow image buffer pushing

        Args:
            cameraIndex (int): Camera requested.
            timeStamp (Any): time stamp of the image. If empty, use current time.
            width (int): int width of image among 1280*960, 640*480, 320*240, 240*160
            height (int): int height of image
            colorSpace (int): colorSpace of image.
            imageBuffer (Any): Image buffer in bitmap form

        Returns:
            bool: true if the put succeeded
        """
        ...
    async def getExpectedImageParameters(self, cameraIndex: int) -> Any:
        """called by the simulator to know expected image parameters

        Args:
            cameraIndex (int): Camera requested.

        Returns:
            Any: ALValue of expected parameters, \\[int resolution, int framerate\\]
        """
        ...
    @overload
    async def putImage(self, cameraIndex: int, width: int, height: int, imageBuffer: Any) -> bool:
        """Allow image buffer pushing

        Args:
            cameraIndex (int): Camera requested.
            width (int): int width of image among 1280*960, 640*480, 320*240, 240*160
            height (int): int height of image
            imageBuffer (Any): Image buffer in bitmap form

        Returns:
            bool: true if the put succeeded
        """
        ...