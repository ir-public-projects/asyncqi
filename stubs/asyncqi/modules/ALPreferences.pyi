from typing import Any, overload
from typing_extensions import deprecated
class ALPreferences:
    """ALPreferences allows access to xml preference files. 
    A preference is defined as follows : 
    pParams[0] Name of the preference; 
    pParams[1] Description of the preference; 
    pParams[2] The value of the preference (can contain other preferences); 
    pParams[3] (optional) The name of the data when inserted into memory.
    """
    def __init__(self) -> None:
        ...
    async def readPrefFile(self, fileName: str, autoGenerateMemoryNames: bool) -> Any:
        """Reads all preferences from an xml files and stores them in an ALValue.

        Args:
            fileName (str): Name of the module associated to the preference.
            autoGenerateMemoryNames (bool): If true a memory name will be generated for each non-array preference.

        Returns:
            Any: array reprenting the whole file.
        """
        ...
    async def writePrefFile(self, fileName: str, prefs: Any, ignoreMemoryNames: bool) -> None:
        """Writes all preferences from ALValue to an xml file.

        Args:
            fileName (str): Name of the module associated to the preference.
            prefs (Any): array reprenting the whole file.
            ignoreMemoryNames (bool): If true all memory names will be removed before saving.
        """
        ...
    async def removePrefFile(self, fileName: str) -> None:
        """Remove the xml file.

        Args:
            fileName (str): Name of the module associated to the preference.
        """
        ...
    async def saveToMemory(self, prefs: Any) -> bool:
        """Writes all preferences from ALValue to an xml file.

        Args:
            prefs (Any): array representing the whole file.

        Returns:
            bool: True upon success.
        """
        ...