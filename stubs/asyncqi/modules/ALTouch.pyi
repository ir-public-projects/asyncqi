from typing import Any, overload
from typing_extensions import deprecated
class ALTouch:
    """This module is dedicated to inform if the robot is touched [joints or button]"""
    def __init__(self) -> None:
        ...
    async def getSensorList(self) -> list[str]:
        """Return the list of sensors managed by touch module and return by TouchChangedevent.

        Returns:
            list[str]: A vector<std::string> of sensor names manage by ALTouch.
        """
        ...
    async def getStatus(self) -> Any:
        """Return the current status of all Touch groups.

        Returns:
            Any: A vector of pair \\[name, bool\\], similar to TouchChanged event.
        """
        ...