from typing import Any, overload
from typing_extensions import deprecated
class ALRobotModel:
    """This module gives access to configuration of the robot defined in xml format

    """
    def __init__(self) -> None:
        ...
    async def getConfig(self) -> str:
        """Return the RobotConfig key/value pairs serialized in xml format

        Returns:
            str: the RobotConfig key/value pairs in xml format
        """
        ...
    async def getRobotType(self) -> str:
        """Get the robot type. Could be: Nao, Romeo, Juliette....

        Returns:
            str: The robot type.
        """
        ...
    async def hasArms(self) -> bool:
        """Determine if the robot has arms.

        Returns:
            bool: True if the robot has arms.
        """
        ...
    async def hasHands(self) -> bool:
        """Determine if the robot has hands.

        Returns:
            bool: True if the robot has hands.
        """
        ...
    async def hasLegs(self) -> bool:
        """Determine if the robot has legs.

        Returns:
            bool: True if the robot has legs.
        """
        ...