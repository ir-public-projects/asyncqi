from typing import Any, overload
from typing_extensions import deprecated
class ALMotionRecorder:
    """ALMotionRecorder is a very specific module for real-time motion recording in Choregraphe. Users can get a simpler interface for motion recording by using the Animation Mode. ALMotionRecorder also supports recording modes using bumpers or torso button, and selective motion replay."""
    def __init__(self) -> None:
        ...
    async def startInteractiveRecording(self, jointsToRecord: list[str], nbPoses: int, extensionAllowed: bool, mode: int) -> None:
        """Start recording the motion in an interactive mode

        Args:
            jointsToRecord (list[str]): Names of joints that must be recorded
            nbPoses (int): Default number of poses to record
            extensionAllowed (bool): Set to true to ignore nbPoses and keep recording new poses as long as record is not manually stopped
            mode (int): Indicates which interactive mode must be used. 1 : Use right bumper to enslave and left bumper to store the pose  (deprecated); 2 : Use head tap to store the pose
        """
        ...
    async def startPeriodicRecording(self, jointsToRecord: list[str], nbPoses: int, extensionAllowed: bool, timeStep: float, jointsToReplay: list[str], replayData: Any) -> None:
        """Start recording the motion in a periodic mode

        Args:
            jointsToRecord (list[str]): Names of joints that must be recorded
            nbPoses (int): Default number of poses to record
            extensionAllowed (bool): set to true to ignore nbPoses and keep recording new poses as long as record is not manually stopped
            timeStep (float): Time in seconds to wait between two poses
            jointsToReplay (list[str]): Names of joints that must be replayed
            replayData (Any): An ALValue holding data for replayed joints. It holds two ALValues. The first one is an ALValue where each line corresponds to a joint, and column elements are times of control points The second one is also an ALValue where each line corresponds to a joint, but column elements are arrays containing \\[float angle, Handle1, Handle2\\] elements, where Handle is \\[int InterpolationType, float dAngle, float dTime\\] describing the handle offsets relative to the angle and time of the point. The first bezier param describes the handle that controls the curve preceding the point, the second describes the curve following the point.
        """
        ...
    async def stopAndGetRecording(self) -> Any:
        """Stop recording the motion and return data

        Returns:
            Any: Returns the recorded data as an ALValue: \\[\\[JointName1,\\[pos1, pos2, ...\\]\\], \\[JointName2,\\[pos1, pos2, ...\\]\\], ...\\]
        """
        ...
    async def dataChanged(self, dataName: str, data: Any, message: str) -> None:
        """Called by ALMemory when subcription data is updated. INTERNAL

        Args:
            dataName (str): Name of the subscribed data.
            data (Any): Value of the the subscribed data
            message (str): The message give when subscribing.
        """
        ...