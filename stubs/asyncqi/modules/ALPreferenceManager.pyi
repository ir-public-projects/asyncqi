from typing import Any, overload
from typing_extensions import deprecated
class ALPreferenceManager:
    """"""
    def __init__(self) -> None:
        ...
    async def getValue(self, domain: str, setting: str) -> Any:
        """Get specified preference

        Args:
            domain (str): Preference domain
            setting (str): Preference setting

        Returns:
            Any: corresponding preferences value
        """
        ...
    async def setValue(self, domain: str, setting: str, value: Any) -> None:
        """Set specified preference

        Args:
            domain (str): Preference domain
            setting (str): Preference setting
            value (Any): Preference value
        """
        ...
    async def getValueList(self, domain: str) -> list[list[Unknown]]:
        """Get preferences names and values for a given domain

        Args:
            domain (str): Preference domain

        Returns:
            list[list[Unknown]]: a list of preferences names and values associated to the domain
        """
        ...
    async def getDomainList(self) -> list[str]:
        """Get available preferences domain

        Returns:
            list[str]: a list containing all the available domain names
        """
        ...
    async def removeValue(self, domain: str, setting: str) -> None:
        """Remove specified preference

        Args:
            domain (str): Preference domain
            setting (str): Preference setting
        """
        ...
    async def setValues(self, values: dict[str, dict[str, Unknown]]) -> None:
        """Add many values at once.

        Args:
            values (dict[str, dict[str, Unknown]]): A map (domain as index) of map (setting name as index) of values.
        """
        ...
    async def removeDomainValues(self, domain: str) -> None:
        """Remove an entire preference domain

        Args:
            domain (str): Preference domain
        """
        ...
    async def importPrefFile(self, domain: str, applicationName: str, filename: str, override: bool) -> None:
        """Import a preferences XML file

        Args:
            domain (str): Preference domain: all preferences values found in XML file will be imported in that domain.
            applicationName (str): Application name: will be used to search for preference file on disk (in location of type <configurationdirectory>/applicationName/filename).
            filename (str): Preference XML filename
            override (bool): Set this to true if you want to override preferences that already exist with preferences in imported XML file
        """
        ...
    async def update(self) -> None:
        """Synchronizes local preferences with preferences stored on a server."""
        ...