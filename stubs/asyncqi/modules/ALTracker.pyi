from typing import Any, overload
from typing_extensions import deprecated
class ALTracker:
    """Use ALTracker module to make the robot track an object or a person with head and arms or not."""
    def __init__(self) -> None:
        ...
    @overload
    async def getTargetPosition(self, pFrame: int) -> list[float]:
        """Returns the [x, y, z] position of the target in FRAME_TORSO. This is done assuming an average target size, so it might not be very accurate.

        Args:
            pFrame (int): target frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.

        Returns:
            list[float]: Vector of 3 floats corresponding to the target position 3D. 
        """
        ...
    async def getRobotPosition(self) -> list[float]:
        """Only work with LandMarks target name. Returns the [x, y, z, wx, wy, wz] position of the robot in coordinate system setted with setMap API. This is done assuming an average target size, so it might not be very accurate.

        Returns:
            list[float]: Vector of 6 floats corresponding to the robot position 6D.
        """
        ...
    async def isActive(self) -> bool:
        """Return true if Tracker is running.

        Returns:
            bool: True if Tracker is running.
        """
        ...
    async def isNewTargetDetected(self) -> bool:
        """Return true if a new target was detected.

        Returns:
            bool: True if a new target was detected since the last getTargetPosition().
        """
        ...
    async def setRelativePosition(self, target: Any) -> None:
        """Set the robot position relative to target in Move mode.

        Args:
            target (Any): Set the final goal of the tracking. Could be \\[distance, thresholdX, thresholdY\\] or with LandMarks target name \\[coordX, coordY, coordWz, thresholdX, thresholdY, thresholdWz\\].
        """
        ...
    async def getRelativePosition(self) -> Any:
        """Get the robot position relative to target in Move mode.

        Returns:
            Any: The final goal of the tracking. Could be \\[distance, thresholdX, thresholdY\\] or with LandMarks target name \\[coordX, coordY, coordWz, thresholdX, thresholdY, thresholdWz\\].
        """
        ...
    async def setTargetCoordinates(self, pCoord: Any) -> None:
        """Only work with LandMarks target name. Set objects coordinates. Could be [[first object coordinate], [second object coordinate]] [[x1, y1, z1], [x2, y2, z2]]

        Args:
            pCoord (Any): objects coordinates.
        """
        ...
    async def getTargetCoordinates(self) -> Any:
        """Only work with LandMarks target name. Get objects coordinates. Could be [[first object coordinate], [second object coordinate]] [[x1, y1, z1], [x2, y2, z2]]

        Returns:
            Any: objects coordinates.
        """
        ...
    async def setMode(self, pMode: str) -> None:
        """Set the tracker in the predefined mode.Could be "Head", "WholeBody" or "Move".

        Args:
            pMode (str): a predefined mode.
        """
        ...
    async def getMode(self) -> str:
        """Get the tracker current mode.

        Returns:
            str: The current tracker mode.
        """
        ...
    async def getAvailableModes(self) -> list[str]:
        """Get the list of predefined mode.

        Returns:
            list[str]: List of predefined mode.
        """
        ...
    async def toggleSearch(self, pSearch: bool) -> None:
        """Enables/disables the target search process. Target search process occurs only when the target is lost.

        Args:
            pSearch (bool): If true and if the target is lost, the robot moves the head in order to find the target. If false and if the target is lost the robot does not move.
        """
        ...
    async def setSearchFractionMaxSpeed(self, pFractionMaxSpeed: float) -> None:
        """Set search process fraction max speed.

        Args:
            pFractionMaxSpeed (float): a fraction max speed.
        """
        ...
    async def getSearchFractionMaxSpeed(self) -> float:
        """Get search process fraction max speed.

        Returns:
            float: a fraction max speed.
        """
        ...
    async def isSearchEnabled(self) -> bool:
        """Return true if the target search process is enabled.

        Returns:
            bool: If true the target search process is enabled.
        """
        ...
    async def stopTracker(self) -> None:
        """Stop the tracker."""
        ...
    async def isTargetLost(self) -> bool:
        """Return true if the target was lost.

        Returns:
            bool: True if the target was lost.
        """
        ...
    async def track(self, pTarget: str) -> None:
        """Set the predefided target to track and start the tracking process if not started.

        Args:
            pTarget (str): a predefined target to track.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
        """
        ...
    async def trackEvent(self, pEventName: str) -> None:
        """Track event and start the tracking process if not started.

        Args:
            pEventName (str): a event name to track.
        """
        ...
    async def registerTarget(self, pTarget: str, pParams: Any) -> None:
        """Register a predefined target. Subscribe to corresponding extractor if Tracker is running..

        Args:
            pTarget (str): a predefined target to track.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
            pParams (Any): a target parameters. (RedBall : set diameter of ball.
        """
        ...
    async def setExtractorPeriod(self, pTarget: str, pPeriod: int) -> None:
        """Set a period for the corresponding target name extractor.

        Args:
            pTarget (str): a predefined target name.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
            pPeriod (int): a period in milliseconds
        """
        ...
    async def getExtractorPeriod(self, pTarget: str) -> int:
        """Get the period of corresponding target name extractor.

        Args:
            pTarget (str): a predefined target name.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".

        Returns:
            int: a period in milliseconds
        """
        ...
    async def unregisterTarget(self, pTarget: str) -> None:
        """Unregister target name and stop corresponding extractor.

        Args:
            pTarget (str): a predefined target to remove.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
        """
        ...
    async def unregisterTargets(self, pTarget: list[str]) -> None:
        """Unregister a list of target names and stop corresponding extractor.

        Args:
            pTarget (list[str]): a predefined target list to remove.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
        """
        ...
    async def unregisterAllTargets(self) -> None:
        """Unregister all targets except active target and stop corresponding extractor."""
        ...
    async def getActiveTarget(self) -> str:
        """Return active target name.

        Returns:
            str: Tracked target name.
        """
        ...
    async def getSupportedTargets(self) -> list[str]:
        """Return a list of supported targets names.

        Returns:
            list[str]: Supported targets names.
        """
        ...
    async def getRegisteredTargets(self) -> list[str]:
        """Return a list of registered targets names.

        Returns:
            list[str]: Registered targets names.
        """
        ...
    @overload
    async def lookAt(self, pPosition: list[float], pFrame: int, pFractionMaxSpeed: float, pUseWholeBody: bool) -> None:
        """Look at the target position with head.


        Args:
            pPosition (list[float]): position 3D \\[x, y, z\\] x position must be striclty positif.
            pFrame (int): target frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            pFractionMaxSpeed (float): The fraction of maximum speed to use. Must be between 0 and 1.
            pUseWholeBody (bool): If true, use whole body constraints.
        """
        ...
    @overload
    async def pointAt(self, pEffector: str, pPosition: list[float], pFrame: int, pFractionMaxSpeed: float) -> None:
        """Point at the target position with arms.


        Args:
            pEffector (str): effector name. Could be "Arms", "LArm", "RArm".
            pPosition (list[float]): position 3D \\[x, y, z\\] to point in FRAME_TORSO. x position must be striclty positif.
            pFrame (int): target frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            pFractionMaxSpeed (float): The fraction of maximum speed to use. Must be between 0 and 1.
        """
        ...
    async def getMoveConfig(self) -> Any:
        """Get the config for move modes.

        Returns:
            Any: ALMotion GaitConfig
        """
        ...
    async def setMoveConfig(self, config: dict[str, Any]) -> None:
        """set a config for move modes.

        Args:
            config (dict[str, Any]): ALMotion GaitConfig
        """
        ...
    async def getTimeOut(self) -> int:
        """get the timeout parameter for target lost.

        Returns:
            int: time in milliseconds.
        """
        ...
    async def setTimeOut(self, pTimeMs: int) -> None:
        """set the timeout parameter for target lost.

        Args:
            pTimeMs (int): time in milliseconds.
        """
        ...
    async def getMaximumDistanceDetection(self) -> float:
        """get the maximum distance for target detection in meter.

        Returns:
            float: The maximum distance for target detection in meter.
        """
        ...
    async def setMaximumDistanceDetection(self, pMaxDistance: float) -> None:
        """set the maximum target detection distance in meter.

        Args:
            pMaxDistance (float): The maximum distance for target detection in meter.
        """
        ...
    async def getEffector(self) -> str:
        """Get active effector.

        Returns:
            str: Active effector name. Could be: "Arms", "LArm", "RArm" or "None". 
        """
        ...
    async def setEffector(self, pEffector: str) -> None:
        """Set an end-effector to move for tracking.

        Args:
            pEffector (str): Name of effector. Could be: "Arms", "LArm", "RArm" or "None". 
        """
        ...
    async def initialize(self) -> None:
        """Initialize tracker parameters with default values."""
        ...
    async def setMaximumVelocity(self, pVelocity: float) -> None:
        """Set the maximum velocity for tracking.

        Args:
            pVelocity (float): The maximum velocity in rad.s-1 .
        """
        ...
    async def getMaximumVelocity(self) -> float:
        """Get the maximum velocity for tracking.

        Returns:
            float: The maximum velocity in rad.s-1 .
        """
        ...
    async def setMaximumAcceleration(self, pAcceleration: float) -> None:
        """Set the maximum acceleration for tracking.

        Args:
            pAcceleration (float): The maximum acceleration in rad.s-2 .
        """
        ...
    async def getMaximumAcceleration(self) -> float:
        """Get the maximum acceleration for tracking.

        Returns:
            float: The maximum acceleration in rad.s-2 .
        """
        ...
    @overload
    @deprecated('Use lookAt with frame instead. Look at the target position with head.')
    async def lookAt(self, pPosition: list[float], pFractionMaxSpeed: float, pUseWholeBody: bool) -> None:
        """DEPRECATED. Use lookAt with frame instead. Look at the target position with head.


        Args:
            pPosition (list[float]): position 3D \\[x, y, z\\] to look in FRAME_TORSO. x position must be striclty positif.
            pFractionMaxSpeed (float): The fraction of maximum speed to use. Must be between 0 and 1.
            pUseWholeBody (bool): If true, use whole body constraints.
        """
        ...
    @overload
    @deprecated('Use pointAt with frame instead. Point at the target position with arms.')
    async def pointAt(self, pEffector: str, pPosition: list[float], pFractionMaxSpeed: float) -> None:
        """DEPRECATED. Use pointAt with frame instead. Point at the target position with arms.


        Args:
            pEffector (str): effector name. Could be "Arms", "LArm", "RArm".
            pPosition (list[float]): position 3D \\[x, y, z\\] to point in FRAME_TORSO. x position must be striclty positif.
            pFractionMaxSpeed (float): The fraction of maximum speed to use. Must be between 0 and 1.
        """
        ...
    @overload
    @deprecated('Use pointAt with frame instead. Returns the [x, y, z] position of the target in FRAME_TORSO. This is done assuming an average target size, so it might not be very accurate.')
    async def getTargetPosition(self) -> list[float]:
        """DEPRECATED. Use pointAt with frame instead. Returns the [x, y, z] position of the target in FRAME_TORSO. This is done assuming an average target size, so it might not be very accurate.

        Returns:
            list[float]: Vector of 3 floats corresponding to the target position 3D. 
        """
        ...
    @deprecated('Use getSupportedTargets() instead. Return a list of targets names.')
    async def getTargetNames(self) -> list[str]:
        """DEPRECATED. Use getSupportedTargets() instead. Return a list of targets names.

        Returns:
            list[str]: Valid targets names.
        """
        ...
    @deprecated('Use getRegisteredTargets() instead. Return a list of managed targets names.')
    async def getManagedTargets(self) -> list[str]:
        """DEPRECATED. Use getRegisteredTargets() instead. Return a list of managed targets names.

        Returns:
            list[str]: Managed targets names.
        """
        ...
    @deprecated('Use registerTarget() instead. Add a predefined target. Subscribe to corresponding extractor if Tracker is running..')
    async def addTarget(self, pTarget: str, pParams: Any) -> None:
        """DEPRECATED. Use registerTarget() instead. Add a predefined target. Subscribe to corresponding extractor if Tracker is running..

        Args:
            pTarget (str): a predefined target to track.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
            pParams (Any): a target parameters. (RedBall : set diameter of ball.
        """
        ...
    @deprecated('Use unregisterTarget() instead. Remove target name and stop corresponding extractor.')
    async def removeTarget(self, pTarget: str) -> None:
        """DEPRECATED. Use unregisterTarget() instead. Remove target name and stop corresponding extractor.

        Args:
            pTarget (str): a predefined target to remove.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
        """
        ...
    @deprecated('Use unregisterTargets() instead. Remove a list of target names and stop corresponding extractor.')
    async def removeTargets(self, pTarget: list[str]) -> None:
        """DEPRECATED. Use unregisterTargets() instead. Remove a list of target names and stop corresponding extractor.

        Args:
            pTarget (list[str]): a predefined target list to remove.Could be "RedBall", "Face", "LandMark", "LandMarks", "People" or "Sound".
        """
        ...
    @deprecated('Use unregisterAllTargets() instead. Remove all managed targets except active target and stop corresponding extractor.')
    async def removeAllTargets(self) -> None:
        """DEPRECATED. Use unregisterAllTargets() instead. Remove all managed targets except active target and stop corresponding extractor."""
        ...
    @deprecated('Use setEffector instead. Add an end-effector to move for tracking.')
    async def addEffector(self, pEffector: str) -> None:
        """DEPRECATED. Use setEffector instead. Add an end-effector to move for tracking.

        Args:
            pEffector (str): Name of effector. Could be: "Arms", "LArm" or "RArm". 
        """
        ...
    @deprecated('Use setEffector("None") instead. Remove an end-effector from tracking.')
    async def removeEffector(self, pEffector: str) -> None:
        """DEPRECATED. Use setEffector("None") instead. Remove an end-effector from tracking.

        Args:
            pEffector (str): Name of effector. Could be: "Arms", "LArm" or "RArm". 
        """
        ...