from typing import Any, overload
from typing_extensions import deprecated
class ALMovementDetection:
    """"""
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def isPaused(self) -> bool:
        """Gets extractor pause status

        Returns:
            bool: True if the extractor is paused, False if not
        """
        ...
    async def pause(self, status: bool) -> None:
        """Changes the pause status of the extractor

        Args:
            status (bool): New pause satus
        """
        ...
    async def isProcessing(self) -> bool:
        """Gets extractor running status

        Returns:
            bool: True if the extractor is currently processing images, False if not
        """
        ...
    async def setFrameRate(self, value: int) -> bool:
        """Sets extractor framerate

        Args:
            value (int): New framerate

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def getFrameRate(self) -> int:
        """Gets extractor framerate

        Returns:
            int: Current value of the framerate of the extractor
        """
        ...
    async def setResolution(self, resolution: int) -> bool:
        """Sets extractor resolution

        Args:
            resolution (int): New resolution

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def getResolution(self) -> int:
        """Gets extractor resolution

        Returns:
            int: Current value of the resolution of the extractor
        """
        ...
    async def setActiveCamera(self, cameraId: int) -> bool:
        """Sets extractor active camera

        Args:
            cameraId (int): Id of the camera that will become the active camera

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def getActiveCamera(self) -> int:
        """Gets extractor active camera

        Returns:
            int: Id of the current active camera of the extractor
        """
        ...
    async def resetDetection(self) -> None:
        """Enables to reset the movement detection when desired"""
        ...
    async def setColorSensitivity(self, sensitivity: float) -> None:
        """Sets the value of the color sensitivity used for the 2D detection  of moving pixels in the image.

        Args:
            sensitivity (float): New color sensitivity (between 0 and 1)
        """
        ...
    async def getColorSensitivity(self) -> float:
        """Gets the value of the color sensitivity

        Returns:
            float: Current color sensitivity (between 0 and 1)
        """
        ...
    async def setDepthSensitivity(self, sensitivity: float) -> None:
        """Sets the value of the depth sensitivity (in meters) used for the detection 3D of moving pixels in the image.

        Args:
            sensitivity (float): New depth sensitivity (in meters)
        """
        ...
    async def getDepthSensitivity(self) -> float:
        """gets the value of the depth sensitivity (in meters)

        Returns:
            float: Current depth sensitivity (in meters)
        """
        ...