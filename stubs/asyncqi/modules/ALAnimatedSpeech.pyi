from typing import Any, overload
from typing_extensions import deprecated
class ALAnimatedSpeech:
    """The Animated Speech module makes NAO interpret a text annotated with behaviors."""
    def __init__(self) -> None:
        ...
    @overload
    async def say(self, text: str) -> None:
        """Say the annotated text given in parameter and animate it with animations inserted in the text.

        Args:
            text (str): An annotated text (for example: "Hello. ^start(Hey_1) My name is NAO").
        """
        ...
    @overload
    async def say(self, text: str, configuration: dict[str, Any]) -> None:
        """Say the annotated text given in parameter and animate it with animations inserted in the text.

        Args:
            text (str): An annotated text (for example: "Hello. ^start(Hey_1) My name is NAO").
            configuration (dict[str, Any]): The animated speech configuration.
        """
        ...
    async def sayWithoutInstructions(self, text: str, locale: int) -> None:
        """Say the annotated text given in parameter and animate it with animations inserted in the text.Do not execute ^call and do not not raise event

        Args:
            text (str): text An annotated text (for example: "Hello. \mrk=1\ My name is NAO").
            locale (int): Locale of the spoken sentence.
        """
        ...
    @deprecated('DEPRECATED since 1.18: use ALSpeakingMovement.setEnabled instead.Enable or disable the automatic body talk on the speech.If it is enabled, anywhere you have not annotate your text with animation,the robot will fill the gap with automatically calculated gestures.If it is disabled, the robot will move only where you annotate it withanimations.')
    async def setBodyTalkEnabled(self, enable: bool) -> None:
        """DEPRECATED since 1.18: use ALSpeakingMovement.setEnabled instead.Enable or disable the automatic body talk on the speech.If it is enabled, anywhere you have not annotate your text with animation,the robot will fill the gap with automatically calculated gestures.If it is disabled, the robot will move only where you annotate it withanimations.

        Args:
            enable (bool): The boolean value: true to enable, false to disable.
        """
        ...
    @deprecated('DEPRECATED since 1.22: use ALSpeakingMovement.setEnabled instead.Enable or disable the automatic body language on the speech.If it is enabled, anywhere you have not annotate your text with animation,the robot will fill the gap with automatically calculated gestures.If it is disabled, the robot will move only where you annotate it withanimations.')
    async def setBodyLanguageEnabled(self, enable: bool) -> None:
        """DEPRECATED since 1.22: use ALSpeakingMovement.setEnabled instead.Enable or disable the automatic body language on the speech.If it is enabled, anywhere you have not annotate your text with animation,the robot will fill the gap with automatically calculated gestures.If it is disabled, the robot will move only where you annotate it withanimations.

        Args:
            enable (bool): The boolean value: true to enable, false to disable.
        """
        ...
    @deprecated('DEPRECATED since 2.4: use ALSpeakingMovement.setMode && ALSpeakingMovement.setEnabled instead.Set the current body language mode.')
    async def setBodyLanguageModeFromStr(self, stringBodyLanguageMode: str) -> None:
        """DEPRECATED since 2.4: use ALSpeakingMovement.setMode && ALSpeakingMovement.setEnabled instead.Set the current body language mode.
        3 modes exist: "disabled", "random" and "contextual"
        (see BodyLanguageMode enum for more details)

        Args:
            stringBodyLanguageMode (str): The choosen body language mode.
        """
        ...
    @deprecated('DEPRECATED since 2.4: use ALSpeakingMovement.setMode && ALSpeakingMovement.setEnabled instead.Set the current body language mode.')
    async def setBodyLanguageMode(self, bodyLanguageMode: int) -> None:
        """DEPRECATED since 2.4: use ALSpeakingMovement.setMode && ALSpeakingMovement.setEnabled instead.Set the current body language mode.
        3 modes exist: SPEAKINGMOVEMENT_MODE_DISABLED,SPEAKINGMOVEMENT_MODE_RANDOM and SPEAKINGMOVEMENT_MODE_CONTEXTUAL
        (see BodyLanguageMode enum for more details)

        Args:
            bodyLanguageMode (int): The choosen body language mode.
        """
        ...
    @deprecated('DEPRECATED since 2.4: use ALSpeakingMovement.getMode && ALSpeakingMovement.isEnabled instead.Set the current body language mode.')
    async def getBodyLanguageModeToStr(self) -> str:
        """DEPRECATED since 2.4: use ALSpeakingMovement.getMode && ALSpeakingMovement.isEnabled instead.Set the current body language mode.
        3 modes exist: "disabled", "random" and "contextual"
        (see BodyLanguageMode enum for more details)

        Returns:
            str: The current body language mode.
        """
        ...
    @deprecated('DEPRECATED since 2.4: use ALSpeakingMovement.getMode && ALSpeakingMovement.isEnabled instead.Set the current body language mode.')
    async def getBodyLanguageMode(self) -> int:
        """DEPRECATED since 2.4: use ALSpeakingMovement.getMode && ALSpeakingMovement.isEnabled instead.Set the current body language mode.
        3 modes exist: SPEAKINGMOVEMENT_MODE_DISABLED,SPEAKINGMOVEMENT_MODE_RANDOM and SPEAKINGMOVEMENT_MODE_CONTEXTUAL
        (see BodyLanguageMode enum for more details)

        Returns:
            int: The current body language mode.
        """
        ...
    @deprecated('DEPRECATED since 2.2: use ALAnimationPlayer.declarePathForTags instead.Add a new package that contains animations.')
    async def declareAnimationsPackage(self, animationsPackage: str) -> None:
        """DEPRECATED since 2.2: use ALAnimationPlayer.declarePathForTags instead.Add a new package that contains animations.

        Args:
            animationsPackage (str): The new package that contains animations.
        """
        ...
    @deprecated('DEPRECATED since 2.4: use ALSpeakingMovement.addTagsToWords instead.Add some new links between tags and words.')
    async def addTagsToWords(self, tagsToWords: Any) -> None:
        """DEPRECATED since 2.4: use ALSpeakingMovement.addTagsToWords instead.Add some new links between tags and words.

        Args:
            tagsToWords (Any): Map of tags to words.
        """
        ...
    @deprecated('DEPRECATED since 2.2: use ALAnimationPlayer.addTagForAnimations instead.Declare some tags with the associated animations.')
    async def declareTagForAnimations(self, tagsToAnimations: Any) -> None:
        """DEPRECATED since 2.2: use ALAnimationPlayer.addTagForAnimations instead.Declare some tags with the associated animations.

        Args:
            tagsToAnimations (Any): Map of Tags to Animations.
        """
        ...
    @deprecated('DEPRECATED since 1.18: use ALSpeakingMovement.isEnabled instead.Indicate if the body talk is enabled or not.')
    async def isBodyTalkEnabled(self) -> bool:
        """DEPRECATED since 1.18: use ALSpeakingMovement.isEnabled instead.Indicate if the body talk is enabled or not.

        Returns:
            bool: The boolean value: true means it is enabled, false means it is disabled.
        """
        ...
    @deprecated('DEPRECATED since 1.22: use ALSpeakingMovement.isEnabled instead.Indicate if the body language is enabled or not.')
    async def isBodyLanguageEnabled(self) -> bool:
        """DEPRECATED since 1.22: use ALSpeakingMovement.isEnabled instead.Indicate if the body language is enabled or not.

        Returns:
            bool: The boolean value: true means it is enabled, false means it is disabled.
        """
        ...