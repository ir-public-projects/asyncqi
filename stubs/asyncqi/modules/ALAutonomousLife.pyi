from typing import Any, overload
from typing_extensions import deprecated
class ALAutonomousLife:
    """Manages the focused Activity and Autonomous Life state"""
    def __init__(self) -> None:
        ...
    async def setState(self, state: str) -> None:
        """Programatically control the state of Autonomous Life

        Args:
            state (str): The possible states of AutonomousLife are: interactive, solitary, safeguard, disabled
        """
        ...
    async def getState(self) -> str:
        """Returns the current state of AutonomousLife

        Returns:
            str: Can be: solitary, interactive, safeguard, disabled
        """
        ...
    async def focusedActivity(self) -> str:
        """Returns the currently focused activity

        Returns:
            str: The name of the focused activity
        """
        ...
    @overload
    async def switchFocus(self, activity_name: str) -> None:
        """Set an activity as running with user focus

        Args:
            activity_name (str): The package_name/activity_name to run
        """
        ...
    @overload
    async def switchFocus(self, activity_name: str, flags: int) -> None:
        """Set an activity as running with user focus

        Args:
            activity_name (str): The package_name/activity_name to run
            flags (int): Int flags for focus changing. STOP_CURRENT(0) or STOP_AND_STACK_CURRENT(1)
        """
        ...
    @overload
    async def switchFocus(self, activity_name: str, flags: int, parameters: Any) -> None:
        """Set an activity as running with user focus

        Args:
            activity_name (str): The package_name/activity_name to run
            flags (int): Int flags for focus changing. STOP_CURRENT(0) or STOP_AND_STACK_CURRENT(1)
            parameters (Any): AnyValue to be passed to activity
        """
        ...
    async def stopFocus(self) -> None:
        """"""
        ...
    async def stopAll(self) -> None:
        """"""
        ...
    async def getFocusContext(self, name: str) -> Any:
        """Get a value of an ALMemory key that is used in a condition, which is the value at the previous autonomous activity focus.

        Args:
            name (str): Name of the ALMemory key to get.  Will throw if key is not used in any activity conditions.

        Returns:
            Any: An array of the ALValue of the memory key and timestamp of when it was set: \\[seconds, microseconds, value\\]
        """
        ...
    async def getActivityContextPermissionViolations(self, name: str) -> list[str]:
        """Get a list of permissions that would be violated by a given activity in the current context.

        Args:
            name (str): The name of the activity to check.

        Returns:
            list[str]: An array of strings of the violated permissions. EG: \\["nature", "canRunOnPod", "canRunInSleep"\\]
        """
        ...
    async def getActivityNature(self, activity_name: str) -> str:
        """Returns the nature of an activity

        Args:
            activity_name (str): The package_name/activity_name to check

        Returns:
            str: Possible values are: solitary, interactive
        """
        ...
    async def getActivityStatistics(self) -> dict[str, dict[str, int]]:
        """

        Returns:
            dict[str, dict[str, int]]: No description provided
        """
        ...
    async def getAutonomousActivityStatistics(self) -> dict[str, dict[str, int]]:
        """Get launch count, last completion time, etc for activities with autonomous launch trigger conditions.

        Returns:
            dict[str, dict[str, int]]: A map of activity names, with a cooresponding map of  "prevStartTime", "prevCompletionTime", "startCount", "totalDuration". Times are 0 for unlaunched Activities
        """
        ...
    @overload
    async def getFocusHistory(self) -> list[tuple[ParamType, Ellipsis]]:
        """

        Returns:
            list[tuple[ParamType, Ellipsis]]: No description provided
        """
        ...
    @overload
    async def getFocusHistory(self) -> list[tuple[ParamType, Ellipsis]]:
        """

        Returns:
            list[tuple[ParamType, Ellipsis]]: No description provided
        """
        ...
    @overload
    async def getStateHistory(self) -> list[tuple[ParamType, Ellipsis]]:
        """

        Returns:
            list[tuple[ParamType, Ellipsis]]: No description provided
        """
        ...
    @overload
    async def getStateHistory(self) -> list[tuple[ParamType, Ellipsis]]:
        """

        Returns:
            list[tuple[ParamType, Ellipsis]]: No description provided
        """
        ...
    async def getLifeTime(self) -> int:
        """Get the time in seconds as life sees it.  Based on gettimeofday()

        Returns:
            int: The int time in seconds as Autonomous Life sees it
        """
        ...
    async def setAutonomousAbilityEnabled(self, autonomousAbility: str, enabled: bool) -> None:
        """

        Args:
            autonomousAbility (str): The Autonomous Ability.
            enabled (bool): True to enable the Autonomous Ability, False to disable it.
        """
        ...
    async def getAutonomousAbilityEnabled(self, autonomousAbility: str) -> bool:
        """Know is an Autonomous Ability is enabled or not

        Args:
            autonomousAbility (str): The Autonomous Ability.

        Returns:
            bool: True if the Autonomous Ability is enabled, False otherwise.
        """
        ...
    async def getAutonomousAbilitiesStatus(self) -> list[tuple[ParamType, Ellipsis]]:
        """Get the Autonomous Abilities status (get the autonomous abilities name and booleans to know if they are enabled or running

        Returns:
            list[tuple[ParamType, Ellipsis]]: The Autonomous Abilities status. A vector containing a status for each autonomous ability. Each status is composed of the autonomous ability name, a boolean to know if it's enabled and another boolean to know if it's running.
        """
        ...
    async def startMonitoringLaunchpadConditions(self) -> None:
        """Start monitoring ALMemory and reporting conditional triggers with AutonomousLaunchpad."""
        ...
    async def stopMonitoringLaunchpadConditions(self) -> None:
        """Stop monitoring ALMemory and reporting conditional triggers with AutonomousLaunchpad."""
        ...
    async def isMonitoringLaunchpadConditions(self) -> bool:
        """Gets running status of AutonomousLaunchpad

        Returns:
            bool: True if AutonomousLaunchpad is monitoring ALMemory and reporting conditional triggers.
        """
        ...
    async def setLaunchpadPluginEnabled(self, plugin_name: str, enabled: bool) -> None:
        """Temporarily enables/disables AutonomousLaunchpad Plugins

        Args:
            plugin_name (str): The name of the plugin to enable/disable
            enabled (bool): Whether or not to enable this plugin
        """
        ...
    async def getEnabledLaunchpadPlugins(self) -> list[str]:
        """Get a list of enabled AutonomousLaunchpad Plugins.  Enabled plugins will run when AutonomousLaunchpad is started

        Returns:
            list[str]: A list of strings of enabled plugins.
        """
        ...
    async def getLaunchpadPluginsForGroup(self, group: str) -> list[str]:
        """Get a list of AutonomousLaunchpad Plugins that belong to specified group

        Args:
            group (str): The group to search for the plugins

        Returns:
            list[str]: A list of strings of the plugins belonging to the group.
        """
        ...
    async def setRobotOffsetFromFloor(self, offset: float) -> None:
        """Set the vertical offset (in meters) of the base of the robot with respect to the floor

        Args:
            offset (float): The new vertical offset (in meters)
        """
        ...
    async def getRobotOffsetFromFloor(self) -> float:
        """Get the vertical offset (in meters) of the base of the robot with respect to the floor

        Returns:
            float: Current vertical offset (in meters)
        """
        ...
    async def setSafeguardEnabled(self, name: str, enabled: bool) -> None:
        """Set if a given safeguard will be handled by Autonomous Life or not.

        Args:
            name (str): Name of the safeguard to consider: RobotPushed, RobotFell,CriticalDiagnosis
            enabled (bool): True if life handles the safeguard.
        """
        ...
    async def isSafeguardEnabled(self, name: str) -> bool:
        """Get if a given safeguard will be handled by Autonomous Life or not.

        Args:
            name (str): Name of the safeguard to consider: RobotPushed, RobotFell,CriticalDiagnosis

        Returns:
            bool: True if life handles the safeguard.
        """
        ...