from typing import Any, overload
from typing_extensions import deprecated
class ALRedBallDetection:
    """ALRedBallDetection is a module which can detect red ball based on color saturation.
      The output value is written in ALMemory in the redBallDetected microEvent.
       It contains an array of tags, with the following format.
      [ [time_info], [ball_info], [camera_info_torsoFrame] [camera_info_robotFrame] [camera_id] ]
 
       Tag time_info = [timestamp_seconds, timestamp_microseconds]
      The time Stamp when image was taken.

       Tag ball_info = [ballAngleWz, ballAngleWy, ballSizeInRadianX, ballSizeInRadianY]
      ballAngleWz and ballAngleWy are the angular coordinates in camera angles  (in radians), corresponding to the direct (counter-clokwise) rotations along  the Z axis and the Y axis.
      ballSizeInRadianX and ballSizeInRadianY correspond to the size of the ball in camera angles.

       Tag camera_info_torsoFrame = [x, y, z, wx, wy, wz] in FRAME_TORSO (see motion documentation)
      Tag camera_info_robotFrame = [x, y, z, wx, wy, wz] in FRAME_ROBOT (see motion documentation)
      Tag camera_id = id of the active camera (see videodevice documentation)

    """
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...