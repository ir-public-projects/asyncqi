from typing import Any, overload
from typing_extensions import deprecated
class ALFsr:
    """Deals with FSR sensors.
     A MicroEvent is generated when the foot contact changed.
    The ALMemory Key is footContactChanged, its a boolean which is set to "True" if one of the foot touched the ground
    Also some fast access Memory key are available : 
     footContact (1.0f if one of the foot touched the ground)
     leftFootContact (1.0f if the left foot touched the ground)
     rightFootContact (1.0f if the right foot touched the ground)
     leftFootTotalWeight (the average weight on the left foot in Kg)
     rightFootTotalWeight (the average weight on the right foot in Kg)

    """
    def __init__(self) -> None:
        ...