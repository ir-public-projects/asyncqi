from typing import Any, overload
from typing_extensions import deprecated
class ALBasicAwareness:
    """"""
    def __init__(self) -> None:
        ...
    async def setEnabled(self, enabled: bool) -> None:
        """Enable/Disable BasicAwareness.

        Args:
            enabled (bool): True to enable BasicAwareness, False to disable BasicAwareness.
        """
        ...
    async def isEnabled(self) -> bool:
        """Return whether BasicAwareness is enabled or not.

        Returns:
            bool: Boolean value, true if enabled else false
        """
        ...
    @deprecated('DEPRECATED since 2.4: use setEnabled instead.Start Basic Awareness.')
    async def startAwareness(self) -> None:
        """DEPRECATED since 2.4: use setEnabled instead.Start Basic Awareness."""
        ...
    @deprecated('DEPRECATED since 2.4: use setEnabled instead.Stop Basic Awareness.')
    async def stopAwareness(self) -> None:
        """DEPRECATED since 2.4: use setEnabled instead.Stop Basic Awareness."""
        ...
    @deprecated('DEPRECATED since 2.4: use isEnabled instead.Get the status (started or not) of the module.')
    async def isAwarenessRunning(self) -> bool:
        """DEPRECATED since 2.4: use isEnabled instead.Get the status (started or not) of the module.

        Returns:
            bool: Boolean value, true if running else false
        """
        ...
    async def pauseAwareness(self) -> None:
        """Pause BasicAwareness."""
        ...
    async def resumeAwareness(self) -> None:
        """Resume BasicAwareness."""
        ...
    async def isAwarenessPaused(self) -> bool:
        """Get the pause status of the module.

        Returns:
            bool: No description provided
        """
        ...
    async def setStimulusDetectionEnabled(self, stimulusName: str, isStimulusDetectionEnabled: bool) -> None:
        """Enable/Disable Stimulus Detection.

        Args:
            stimulusName (str): Name of the stimulus to enable/disable
            isStimulusDetectionEnabled (bool): Boolean value: true to enable, false to disable.
        """
        ...
    async def isStimulusDetectionEnabled(self, stimulusName: str) -> bool:
        """Get status enabled/disabled for Stimulus Detection.

        Args:
            stimulusName (str): Name of the stimulus to check

        Returns:
            bool: Boolean value for status enabled/disabled
        """
        ...
    async def setParameter(self, paramName: str, newVal: Any) -> None:
        """Set the specified parameter 

        Args:
            paramName (str): "LookStimulusSpeed" : Speed of head moves when looking at a stimulus, as fraction of max speed  "LookBackSpeed" : Speed of head moves when looking back to previous position, as fraction of max speed  "NobodyFoundTimeOut" : timeout to send HumanLost event when no blob is found, in seconds  "MinTimeTracking" : Minimum Time for the robot to be focused on someone, without listening to other stimuli, in seconds  "TimeSleepBeforeResumeMS" : Slept time before automatically resuming BasicAwareness when an automatic pause has been made, in milliseconds  "TimeOutResetHead" : Timeout to reset the head, in seconds  "AmplitudeYawTracking" : max absolute value for head yaw in tracking, in degrees  "PeoplePerceptionPeriod" : Period for people perception, in milliseconds  "SlowPeoplePerceptionPeriod" : Period for people perception in FullyEngaged mode, in milliseconds  "HeadThreshold" : Yaw threshold for tracking, in degrees  "BodyRotationThreshold" : Angular threshold for BodyRotation tracking mode, in degrees  "BodyRotationThresholdNao" : Angular threshold for BodyRotation tracking mode on Nao, in degrees  "MoveDistanceX" : X Distance for the Move tracking mode, in meters  "MoveDistanceY" : Y Distance for the Move tracking mode, in meters  "MoveAngleTheta" : Angle for the Move tracking mode, in degrees  "MoveThresholdX" : Threshold for the Move tracking mode, in meters  "MoveThresholdY" : Threshold for the Move tracking mode, in meters  "MoveThresholdTheta" : Theta Threshold for the Move tracking mode, in degrees  "MaxDistanceFullyEngaged" : Maximum distance for someone to be tracked for FullyEngaged mode, in meters  "MaxDistanceNotFullyEngaged" : Maximum distance for someone to be tracked for modes different from FullyEngaged, in meters  "MaxHumanSearchTime" : Maximum time to find a human after observing stimulus, in seconds  "DeltaPitchComfortZone" : Pitch width of the comfort zone, in degree  "CenterPitchComfortZone" : Pitch center of the confort zone, in degree  "SoundHeight" : Default Height for sound detection, in meters  "MoveSpeed" : Speed of the robot moves  "MC_Interactive_MinTime" : Minimum time between 2 contextual moves (when the robot is tracking somebody)  "MC_Interactive_MaxOffsetTime" : Maximum time that we can add to MC_Interactive_MinTime (when the robot is tracking somebody)  "MC_Interactive_DistanceXY" : Maximum offset distance in X and Y that the robot can apply when he tracks somebody  "MC_Interactive_MinTheta" : Minimum theta that the robot can apply when he tracks somebody  "MC_Interactive_MaxTheta" : Maximum theta that the robot can apply when he tracks somebody  "MC_Interactive_DistanceHumanRobot" : Distance between the human and the robot  "MC_Interactive_MaxDistanceHumanRobot" : Maximum distance human robot to allow the robot to move (in MoveContextually mode)  
            newVal (Any): "LookStimulusSpeed" : Float in range \\[0.01;1\\]  "LookBackSpeed" : Float in range \\[0.01;1\\]  "NobodyFoundTimeOut" : Float > 0  "MinTimeTracking" : Float in range \\[0;20\\]  "TimeSleepBeforeResumeMS" : Int > 0  "TimeOutResetHead" : Float in range \\[0;30\\]  "AmplitudeYawTracking" : Float in range \\[10;120\\]  "PeoplePerceptionPeriod" : Int > 1  "SlowPeoplePerceptionPeriod" : Int > 1  "HeadThreshold" : Float in range \\[0;180\\]  "BodyRotationThreshold" : Float in range \\[-180;180\\]  "BodyRotationThresholdNao" : Float in range \\[-180;180\\]  "MoveDistanceX" : Float in range \\[-5;5\\]  "MoveDistanceY" : Float in range \\[-5;5\\]  "MoveAngleTheta" : Float in range \\[-180;180\\]  "MoveThresholdX" : Float in range \\[0;5\\]  "MoveThresholdY" : Float in range \\[0;5\\]  "MoveThresholdTheta" : Float in range \\[-180;180\\]  "MaxDistanceFullyEngaged" : Float in range \\[0.5;5\\]  "MaxDistanceNotFullyEngaged" : Float in range \\[0.5;5\\]  "MaxHumanSearchTime" : Float in range \\[0.1;10\\]  "DeltaPitchComfortZone" : Float in range \\[0;90\\]  "CenterPitchComfortZone" : Float in range \\[-90;90\\]  "SoundHeight" : Float in range \\[0;2\\]  "MoveSpeed" : Float in range \\[0.1;0.55\\]  "MC_Interactive_MinTime" : Int in range \\[0;100\\]  "MC_Interactive_MaxOffsetTime" : Int in range \\[0;100\\]  "MC_Interactive_DistanceXY" : Float in range \\[0;1\\]  "MC_Interactive_MinTheta" : Float in range \\[-40;0\\]  "MC_Interactive_MaxTheta" : Float in range \\[0;40\\]  "MC_Interactive_DistanceHumanRobot" : Float in range \\[0.1;2\\]  "MC_Interactive_MaxDistanceHumanRobot" : Float in range \\[0.1;3\\]  
        """
        ...
    async def resetAllParameters(self) -> None:
        """Reset all parameters, including enabled/disabled stimulus."""
        ...
    async def getParameter(self, paramName: str) -> Any:
        """Get the specified parameter.

        Args:
            paramName (str): "LookStimulusSpeed" : Speed of head moves when looking at a stimulus, as fraction of max speed  "LookBackSpeed" : Speed of head moves when looking back to previous position, as fraction of max speed  "NobodyFoundTimeOut" : timeout to send HumanLost event when no blob is found, in seconds  "MinTimeTracking" : Minimum Time for the robot to be focused on someone, without listening to other stimuli, in seconds  "TimeSleepBeforeResumeMS" : Slept time before automatically resuming BasicAwareness when an automatic pause has been made, in milliseconds  "TimeOutResetHead" : Timeout to reset the head, in seconds  "AmplitudeYawTracking" : max absolute value for head yaw in tracking, in degrees  "PeoplePerceptionPeriod" : Period for people perception, in milliseconds  "SlowPeoplePerceptionPeriod" : Period for people perception in FullyEngaged mode, in milliseconds  "HeadThreshold" : Yaw threshold for tracking, in degrees  "BodyRotationThreshold" : Angular threshold for BodyRotation tracking mode, in degrees  "BodyRotationThresholdNao" : Angular threshold for BodyRotation tracking mode on Nao, in degrees  "MoveDistanceX" : X Distance for the Move tracking mode, in meters  "MoveDistanceY" : Y Distance for the Move tracking mode, in meters  "MoveAngleTheta" : Angle for the Move tracking mode, in degrees  "MoveThresholdX" : Threshold for the Move tracking mode, in meters  "MoveThresholdY" : Threshold for the Move tracking mode, in meters  "MoveThresholdTheta" : Theta Threshold for the Move tracking mode, in degrees  "MaxDistanceFullyEngaged" : Maximum distance for someone to be tracked for FullyEngaged mode, in meters  "MaxDistanceNotFullyEngaged" : Maximum distance for someone to be tracked for modes different from FullyEngaged, in meters  "MaxHumanSearchTime" : Maximum time to find a human after observing stimulus, in seconds  "DeltaPitchComfortZone" : Pitch width of the comfort zone, in degree  "CenterPitchComfortZone" : Pitch center of the confort zone, in degree  "SoundHeight" : Default Height for sound detection, in meters  "MoveSpeed" : Speed of the robot moves  "MC_Interactive_MinTime" : Minimum time between 2 contextual moves (when the robot is tracking somebody)  "MC_Interactive_MaxOffsetTime" : Maximum time that we can add to MC_Interactive_MinTime (when the robot is tracking somebody)  "MC_Interactive_DistanceXY" : Maximum offset distance in X and Y that the robot can apply when he tracks somebody  "MC_Interactive_MinTheta" : Minimum theta that the robot can apply when he tracks somebody  "MC_Interactive_MaxTheta" : Maximum theta that the robot can apply when he tracks somebody  "MC_Interactive_DistanceHumanRobot" : Distance between the human and the robot  "MC_Interactive_MaxDistanceHumanRobot" : Maximum distance human robot to allow the robot to move (in MoveContextually mode)  

        Returns:
            Any: ALValue format for required parameter
        """
        ...
    async def setEngagementMode(self, modeName: str) -> None:
        """Set engagement mode.

        Args:
            modeName (str): Name of the mode
        """
        ...
    async def getEngagementMode(self) -> str:
        """Set engagement mode.

        Returns:
            str: Name of current engagement mode as a string
        """
        ...
    async def setTrackingMode(self, modeName: str) -> None:
        """Set tracking mode.

        Args:
            modeName (str): Name of the mode
        """
        ...
    async def getTrackingMode(self) -> str:
        """Set tracking mode.

        Returns:
            str: Name of current tracking mode as a string
        """
        ...
    async def engagePerson(self, engagePerson: int) -> bool:
        """Force Engage Person.

        Args:
            engagePerson (int): ID of the person as found in PeoplePerception.

        Returns:
            bool: true if the robot succeeded to engage the person, else false.
        """
        ...
    async def triggerStimulus(self, stimulusPosition: list[float]) -> int:
        """Trigger a custom stimulus.

        Args:
            stimulusPosition (list[float]): Position of the stimulus, in Frame World

        Returns:
            int: If someone was found, return value is its ID, else it's -1
        """
        ...
    async def triggerStimulusFromFrame(self, stimulusFrame: Any) -> int:
        """Trigger a custom stimulus.

        Args:
            stimulusFrame (Any): Frame of the stimulus

        Returns:
            int: If someone was found, return value is its ID, else it's -1
        """
        ...