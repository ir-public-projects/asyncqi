from typing import Any, overload
from typing_extensions import deprecated
class ALNavigation:
    """Use ALNavigation module to make the robot go safely to the asked pose2D."""
    def __init__(self) -> None:
        ...
    @overload
    async def navigateTo(self, x: float, y: float) -> bool:
        """Makes the robot navigate to a relative metrical target pose2D expressed in FRAME_ROBOT. The robot computes a path to avoid obstacles.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def navigateTo(self, x: float, y: float, theta: float) -> bool:
        """Makes the robot navigate to a relative metrical target pose2D expressed in FRAME_ROBOT. The robot computes a path to avoid obstacles.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].
            theta (float): Orientation of the robot (rad).

        Returns:
            bool: No description provided
        """
        ...
    async def stopNavigateTo(self) -> None:
        """Stops the navigateTo."""
        ...
    @deprecated('Use ALMotion.moveAlong() instead.')
    async def moveAlong(self, trajectory: Any) -> bool:
        """DEPRECATED. Use ALMotion.moveAlong() instead.

        Args:
            trajectory (Any): An ALValue describing a trajectory.

        Returns:
            bool: No description provided
        """
        ...
    async def getFreeZone(self, desiredRadius: float, maximumDisplacement: float) -> Any:
        """Returns [errorCode, result radius[centerWorldMotionX, centerWorldMotionY]]

        Args:
            desiredRadius (float): The radius of the space we want in meters \\[m\\].
            maximumDisplacement (float): The max distance we accept to move toreach the found place \\[m\\].

        Returns:
            Any: Returns \\[errorCode, result radius (m), \\[worldMotionToRobotCenterX (m), worldMotionToRobotCenterY (m)\\]\\]
        """
        ...
    async def findFreeZone(self, desiredRadius: float, maximumDisplacement: float) -> Any:
        """Returns [errorCode, result radius[centerWorldMotionX, centerWorldMotionY]]

        Args:
            desiredRadius (float): The radius of the space we want in meters \\[m\\].
            maximumDisplacement (float): The max distance we accept to move toreach the found place \\[m\\].

        Returns:
            Any: Returns \\[errorCode, result radius (m), \\[worldMotionToRobotCenterX (m), worldMotionToRobotCenterY (m)\\]\\]
        """
        ...
    async def startFreeZoneUpdate(self) -> None:
        """ Starts a loop to update the mapping of the free space around the robot. """
        ...
    async def stopAndComputeFreeZone(self, desiredRadius: float, maximumDisplacement: float) -> Any:
        """Stops and returns free zone.

        Args:
            desiredRadius (float): The radius of the space we want in meters \\[m\\].
            maximumDisplacement (float): The max distance we accept to move toreach the found place \\[m\\].

        Returns:
            Any: Returns \\[errorCode, result radius (m), \\[worldMotionToRobotCenterX (m), worldMotionToRobotCenterY (m)\\]\\]
        """
        ...
    async def explore(self) -> None:
        """Start exploration."""
        ...
    async def stopExploration(self) -> None:
        """Stop exploration."""
        ...
    async def startMapping(self) -> None:
        """Start mapping process, without autonomous exploration."""
        ...
    async def getRobotPositionInMap(self) -> Any:
        """.

        Returns:
            Any: No description provided
        """
        ...
    async def getExplorationPath(self) -> Any:
        """.

        Returns:
            Any: No description provided
        """
        ...
    async def loadExploration(self) -> bool:
        """.

        Returns:
            bool: No description provided
        """
        ...
    async def navigateToInMap(self) -> bool:
        """.

        Returns:
            bool: No description provided
        """
        ...
    async def computePathInMap(self) -> list[Unknown]:
        """.

        Returns:
            list[Unknown]: No description provided
        """
        ...
    async def saveExploration(self) -> str:
        """.

        Returns:
            str: No description provided
        """
        ...
    async def setDefaultExploration(self) -> None:
        """."""
        ...
    async def getMetricalMap(self) -> Any:
        """.

        Returns:
            Any: No description provided
        """
        ...
    async def relocalizeInMapWithHint(self) -> None:
        """."""
        ...
    async def relocalizeInMap(self) -> None:
        """."""
        ...
    async def startLocalization(self) -> None:
        """."""
        ...
    async def stopLocalization(self) -> None:
        """."""
        ...