from typing import Any, overload
from typing_extensions import deprecated
class ALMood:
    """"""
    def __init__(self) -> None:
        ...
    async def subscribe(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def unsubscribe(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getSubscribersInfo(self) -> dict[str, str]:
        """

        Returns:
            dict[str, str]: No description provided
        """
        ...
    async def currentPersonState(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def personStateFromPeoplePerception(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def personStateFromUserSession(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def personDescriptorFromPeoplePerception(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def persons(self) -> list[tuple[ParamType, Ellipsis]]:
        """

        Returns:
            list[tuple[ParamType, Ellipsis]]: No description provided
        """
        ...
    async def ambianceState(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def ambianceDescriptor(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def getEmotionalReaction(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...