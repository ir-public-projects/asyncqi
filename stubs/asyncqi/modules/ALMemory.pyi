from typing import Any, overload
from typing_extensions import deprecated
class ALMemory:
    """ALMemory provides a centralized memory that can be used to store and retrieve named values. It also acts as a hub for the distribution of event notifications."""
    def __init__(self) -> None:
        ...
    @overload
    async def declareEvent(self, eventName: str) -> None:
        """Declares an event to allow future subscriptions to the event

        Args:
            eventName (str): The name of the event
        """
        ...
    @overload
    async def declareEvent(self, eventName: str, extractorName: str) -> None:
        """Declares an event to allow future subscriptions to the event

        Args:
            eventName (str): The name of the event
            extractorName (str): The name of the extractor capable of creating the event
        """
        ...
    @overload
    async def getData(self, key: str) -> Any:
        """Gets the value of a key-value pair stored in memory

        Args:
            key (str): Name of the value.

        Returns:
            Any: The data as an ALValue. This can often be cast transparently into the original type.
        """
        ...
    @overload
    @deprecated('DEPRECATED - Gets the value of a key-value pair stored in memory. Please use the version of this method with no second parameter.')
    async def getData(self, key: str, deprecatedParameter: int) -> Any:
        """DEPRECATED - Gets the value of a key-value pair stored in memory. Please use the version of this method with no second parameter.

        Args:
            key (str): Name of the value.
            deprecatedParameter (int): DEPRECATED - This parameter has no effect, but is left for compatibility reason.

        Returns:
            Any: The data as an ALValue
        """
        ...
    async def subscriber(self, eventName: str) -> Any:
        """Get an object wrapping a signal bound to the given ALMemory event. Creates the event if it does not exist.

        Args:
            eventName (str): Name of the ALMemory event

        Returns:
            Any: An AnyObject with a signal named "signal"
        """
        ...
    async def getTimestamp(self, key: str) -> Any:
        """Get data value and timestamp

        Args:
            key (str): Name of the variable

        Returns:
            Any: A list of all the data key names that contain the given string.
        """
        ...
    async def getEventHistory(self, key: str) -> Any:
        """Get data value and timestamp

        Args:
            key (str): Name of the variable

        Returns:
            Any: A list of all the data key names that contain the given string.
        """
        ...
    async def getDataList(self, filter: str) -> list[str]:
        """Gets a list of all key names that contain a given string

        Args:
            filter (str): A string used as the search term

        Returns:
            list[str]: A list of all the data key names that contain the given string.
        """
        ...
    async def getDataListName(self) -> list[str]:
        """Gets the key names for all the key-value pairs in memory

        Returns:
            list[str]: A list containing the keys in memory
        """
        ...
    @deprecated('No reason provided')
    async def getDataOnChange(self, key: str, deprecatedParameter: int) -> Any:
        """DEPRECATED - Blocks the caller until the value of a key changes

        Args:
            key (str): Name of the data.
            deprecatedParameter (int): DEPRECATED - this parameter has no effect

        Returns:
            Any: an array containing all the retrieved data
        """
        ...
    async def getDataPtr(self, key: str) -> Any:
        """Gets a pointer to 32 a bit data item. Beware, the pointer will only be valid during the lifetime of the ALMemory object. Use with care, at initialization, not every loop. Insert a data item if needed. Throw if the data item has not the expected type. Only meaningful when called from code running in the same process as ALMemory.

        Args:
            key (str): Name of the data.

        Returns:
            Any: A pointer converted to int
        """
        ...
    async def getIntPtr(self, key: str) -> Any:
        """Gets a pointer to a int data item. Beware, the pointer will only be valid during the lifetime of the ALMemory object. Use with care, at initialization, not every loop. Insert a data item if needed. Throw if the data item has not the expected type. Only meaningful when called from code running in the same process as ALMemory.

        Args:
            key (str): Name of the data.

        Returns:
            Any: A pointer to int
        """
        ...
    async def getFloatPtr(self, key: str) -> Any:
        """Gets a pointer to a float data item. Beware, the pointer will only be valid during the lifetime of the ALMemory object. Use with care, at initialization, not every loop. Insert a data item if needed. Throw if the data item has not the expected type. Only meaningful when called from code running in the same process as ALMemory.

        Args:
            key (str): Name of the data.

        Returns:
            Any: A pointer to float
        """
        ...
    async def getEventList(self) -> list[str]:
        """Gets a list containing the names of all the declared events

        Returns:
            list[str]: A list containing the names of all events
        """
        ...
    async def getExtractorEvent(self, extractorName: str) -> list[str]:
        """Gets the list of all events generated by a given extractor

        Args:
            extractorName (str): The name of the extractor

        Returns:
            list[str]: A list containing the names of the events associated with the given extractor
        """
        ...
    async def getListData(self, keyList: Any) -> Any:
        """Gets the values associated with the given list of keys. This is more efficient than calling getData many times, especially over the network.

        Args:
            keyList (Any): An array containing the key names.

        Returns:
            Any: An array containing all the values corresponding to the given keys.
        """
        ...
    async def getMicroEventList(self) -> list[str]:
        """Gets a list containing the names of all the declared micro events

        Returns:
            list[str]: A list containing the names of all the microEvents
        """
        ...
    async def getSubscribers(self, name: str) -> list[str]:
        """Gets a list containing the names of subscribers to an event.

        Args:
            name (str): Name of the event or micro-event

        Returns:
            list[str]: List of subscriber names
        """
        ...
    async def getType(self, key: str) -> str:
        """Gets the storage class of the stored data. This is not the underlying POD type.

        Args:
            key (str): Name of the variable

        Returns:
            str: String type: Data, Event, MicroEvent
        """
        ...
    @overload
    async def insertData(self, key: str, value: int) -> None:
        """Inserts a key-value pair into memory, where value is an int

        Args:
            key (str): Name of the value to be inserted.
            value (int): The int to be inserted
        """
        ...
    @overload
    async def insertData(self, key: str, value: float) -> None:
        """Inserts a key-value pair into memory, where value is a float

        Args:
            key (str): Name of the value to be inserted.
            value (float): The float to be inserted
        """
        ...
    @overload
    async def insertData(self, key: str, value: str) -> None:
        """Inserts a key-value pair into memory, where value is a string

        Args:
            key (str): Name of the value to be inserted.
            value (str): The string to be inserted
        """
        ...
    @overload
    async def insertData(self, key: str, data: Any) -> None:
        """Inserts a key-value pair into memory, where value is an ALValue

        Args:
            key (str): Name of the value to be inserted.
            data (Any): The ALValue to be inserted. This could contain a basic type, or a more complex array. See the ALValue documentation for more information.
        """
        ...
    async def insertListData(self, list: Any) -> None:
        """Inserts a list of key-value pairs into memory.

        Args:
            list (Any): An ALValue list of the form \\[\\[Key, Value\\],...\\]. Each item will be inserted.
        """
        ...
    async def raiseEvent(self, name: str, value: Any) -> None:
        """Publishes the given data to all subscribers.

        Args:
            name (str): Name of the event to raise.
            value (Any): The data associated with the event. This could contain a basic type, or a more complex array. See the ALValue documentation for more information.
        """
        ...
    async def raiseMicroEvent(self, name: str, value: Any) -> None:
        """Publishes the given data to all subscribers.

        Args:
            name (str): Name of the event to raise.
            value (Any): The data associated with the event. This could contain a basic type, or a more complex array. See the ALValue documentation for more information.
        """
        ...
    async def removeData(self, key: str) -> None:
        """Removes a key-value pair from memory

        Args:
            key (str): Name of the data to be removed.
        """
        ...
    async def removeEvent(self, name: str) -> None:
        """Removes a event from memory and unsubscribes any exiting subscribers.

        Args:
            name (str): Name of the event to remove.
        """
        ...
    async def removeMicroEvent(self, name: str) -> None:
        """Removes a micro event from memory and unsubscribes any exiting subscribers.

        Args:
            name (str): Name of the event to remove.
        """
        ...
    @overload
    async def subscribeToEvent(self, name: str, callbackModule: str, callbackMethod: str) -> None:
        """Subscribes to an event and automaticaly launches the module that declared itself as the generator of the event if required.

        Args:
            name (str): The name of the event to subscribe to
            callbackModule (str): Name of the module to call with notifications
            callbackMethod (str): Name of the module's method to call when a data is changed
        """
        ...
    @overload
    @deprecated('DEPRECATED Subscribes to event and automaticaly launches the module capable of generating the event if it is not already running. Please use the version without the callbackMessage parameter.')
    async def subscribeToEvent(self, name: str, callbackModule: str, callbackMessage: str, callbacMethod: str) -> None:
        """DEPRECATED Subscribes to event and automaticaly launches the module capable of generating the event if it is not already running. Please use the version without the callbackMessage parameter.

        Args:
            name (str): The name of the event to subscribe to
            callbackModule (str): Name of the module to call with notifications
            callbackMessage (str): DEPRECATED Message included in the notification.
            callbacMethod (str): Name of the module's method to call when a data is changed
        """
        ...
    async def subscribeToMicroEvent(self, name: str, callbackModule: str, callbackMessage: str, callbackMethod: str) -> None:
        """Subscribes to a microEvent. Subscribed modules are notified on theircallback method whenever the data is updated, even if the new value is the same as the old value.

        Args:
            name (str): Name of the data.
            callbackModule (str): Name of the module to call with notifications
            callbackMessage (str): Message included in the notification. This can be used to disambiguate multiple subscriptions.
            callbackMethod (str): Name of the module's method to call when a data is changed
        """
        ...
    async def unregisterModuleReference(self, moduleName: str) -> None:
        """Informs ALMemory that a module doesn't exist anymore.

        Args:
            moduleName (str): Name of the departing module.
        """
        ...
    async def unsubscribeToEvent(self, name: str, callbackModule: str) -> None:
        """Unsubscribes a module from the given event. No further notifications will be received.

        Args:
            name (str): The name of the event
            callbackModule (str): The name of the module that was given when subscribing.
        """
        ...
    async def unsubscribeToMicroEvent(self, name: str, callbackModule: str) -> None:
        """Unsubscribes from the given event. No further notifications will be received.

        Args:
            name (str): Name of the event.
            callbackModule (str): The name of the module that was given when subscribing.
        """
        ...
    async def setDescription(self, name: str, description: str) -> None:
        """Describe a key

        Args:
            name (str): Name of the key.
            description (str): The description of the event (text format).
        """
        ...
    async def getDescriptionList(self, keylist: list[str]) -> Any:
        """Descriptions of all given keys

        Args:
            keylist (list[str]): List of keys. (empty to get all descriptions)

        Returns:
            Any: an array of tuple (name, type, description) describing all keys.
        """
        ...
    @overload
    async def addMapping(self, service: str, signal: str, event: str) -> None:
        """Add a mapping between signal and event

        Args:
            service (str): Name of the service
            signal (str): Name of the signal
            event (str): Name of the event
        """
        ...
    @overload
    async def addMapping(self, service: str, signalEvent: dict[str, str]) -> None:
        """Add a mapping between signal and event

        Args:
            service (str): Name of the service
            signalEvent (dict[str, str]): A map of signal corresponding to event
        """
        ...