from typing import Any, overload
from typing_extensions import deprecated
class ALRobotMood:
    """"""
    def __init__(self) -> None:
        ...
    async def getState(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...
    async def getFullState(self) -> tuple[ParamType, Ellipsis]:
        """

        Returns:
            tuple[ParamType, Ellipsis]: No description provided
        """
        ...