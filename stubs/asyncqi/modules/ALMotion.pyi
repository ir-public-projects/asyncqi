from typing import Any, overload
from typing_extensions import deprecated
class ALMotion:
    """ALMotion provides methods that help make Nao move. It contains commands for manipulating joint angles, joint stiffness, and a higher level API for controling walks."""
    def __init__(self) -> None:
        ...
    async def wakeUp(self) -> bool:
        """The robot will wake up: set Motor ON and go to initial position if needed

        Returns:
            bool: No description provided
        """
        ...
    async def rest(self) -> None:
        """The robot will rest: go to a relax and safe position and set Motor OFF"""
        ...
    async def robotIsWakeUp(self) -> bool:
        """return true if the robot is already wakeUp

        Returns:
            bool: True if the robot is already wakeUp.
        """
        ...
    async def stiffnessInterpolation(self, names: str | list[str], stiffnessLists: float | list[float] | list[list[float]], timeLists: float | list[float] | list[list[float]]) -> None:
        """Interpolates one or multiple joints to a target stiffness or along timed trajectories of stiffness. This is a blocking call.

        Args:
            names (str | list[str]): Name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators".
            stiffnessLists (float | list[float] | list[list[float]]): An stiffness, list of stiffnesses or list of list of stiffnesses
            timeLists (float | list[float] | list[list[float]]): A time, list of times or list of list of times.
        """
        ...
    async def setStiffnesses(self, names: Any, stiffnesses: Any) -> None:
        """Sets the stiffness of one or more joints. This is a non-blocking call.

        Args:
            names (Any): Names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators".
            stiffnesses (Any): One or more stiffnesses between zero and one.
        """
        ...
    async def getStiffnesses(self, jointName: Any) -> list[float]:
        """Gets stiffness of a joint or group of joints

        Args:
            jointName (Any): Name of the joints, chains, "Body", "Joints" or "Actuators".

        Returns:
            list[float]: One or more stiffnesses. 1.0 indicates maximum stiffness. 0.0 indicated minimum stiffness
        """
        ...
    async def angleInterpolation(self, names: Any, angleLists: list[float] | list[list[float]], timeLists: list[float], isAbsolute: bool) -> None:
        """Interpolates one or multiple joints to a target angle or along timed trajectories. This is a blocking call.

        Args:
            names (Any): Name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators". 
            angleLists (list[float] | list[list[float]]): An angle, list of angles or list of list of angles in radians
            timeLists (list[float]): A time, list of times or list of list of times in seconds
            isAbsolute (bool): If true, the movement is described in absolute angles, else the angles are relative to the current angle.
        """
        ...
    async def angleInterpolationWithSpeed(self, names: Any, targetAngles: list[float] | list[list[float]], maxSpeedFraction: float) -> None:
        """Interpolates one or multiple joints to a target angle, using a fraction of max speed. Only one target angle is allowed for each joint. This is a blocking call.

        Args:
            names (Any): Name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators".
            targetAngles (list[float] | list[list[float]]): An angle, or list of angles in radians
            maxSpeedFraction (float): A fraction.
        """
        ...
    async def angleInterpolationBezier(self, jointNames: list[str], times: Any, controlPoints: Any) -> None:
        """Interpolates a sequence of timed angles for several motors using bezier control points. This is a blocking call.

        Args:
            jointNames (list[str]): A vector of joint names
            times (Any): An ragged ALValue matrix of floats. Each line corresponding to a motor, and column element to a control point.
            controlPoints (Any): An ALValue array of arrays each containing \\[float angle, Handle1, Handle2\\], where Handle is \\[int InterpolationType, float dAngle, float dTime\\] descibing the handle offsets relative to the angle and time of the point. The first bezier param describes the handle that controls the curve preceeding the point, the second describes the curve following the point.
        """
        ...
    @overload
    async def setAngles(self, names: Any, angles: Any, fractionMaxSpeed: float) -> None:
        """Sets angles. This is a non-blocking call.

        Args:
            names (Any): The name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators". 
            angles (Any): One or more angles in radians
            fractionMaxSpeed (float): The fraction of maximum speed to use
        """
        ...
    @overload
    async def setAngles(self, names: Any, angles: Any, fractionMaxSpeeds: list[float]) -> None:
        """Sets angles. This is a non-blocking call.

        Args:
            names (Any): The name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators". 
            angles (Any): One or more angles in radians
            fractionMaxSpeeds (list[float]): The vector of fraction of maximum speed to use
        """
        ...
    async def changeAngles(self, names: Any, changes: Any, fractionMaxSpeed: float) -> None:
        """Changes Angles. This is a non-blocking call.

        Args:
            names (Any): The name or names of joints, chains, "Body", "JointActuators", "Joints" or "Actuators".
            changes (Any): One or more changes in radians
            fractionMaxSpeed (float): The fraction of maximum speed to use
        """
        ...
    async def getAngles(self, names: Any, useSensors: bool) -> list[float]:
        """Gets the angles of the joints

        Args:
            names (Any): Names the joints, chains, "Body", "JointActuators", "Joints" or "Actuators". 
            useSensors (bool): If true, sensor angles will be returned

        Returns:
            list[float]: Joint angles in radians.
        """
        ...
    async def openHand(self, handName: str) -> None:
        """NAO stiffens the motors of desired hand. Then, he opens the hand, then cuts motor current to conserve energy. This is a blocking call.

        Args:
            handName (str): The name of the hand. Could be: "RHand or "LHand"
        """
        ...
    async def closeHand(self, handName: str) -> None:
        """NAO stiffens the motors of desired hand. Then, he closes the hand, then cuts motor current to conserve energy. This is a blocking call.

        Args:
            handName (str): The name of the hand. Could be: "RHand" or "LHand"
        """
        ...
    @overload
    async def move(self, x: float, y: float, theta: float) -> None:
        """Makes the robot move at the given velocity. This is a non-blocking call.

        Args:
            x (float): The velocity along x axis \\[m.s-1\\].
            y (float): The velocity along y axis \\[m.s-1\\].
            theta (float): The velocity around z axis \\[rd.s-1\\].
        """
        ...
    @overload
    async def move(self, x: float, y: float, theta: float, moveConfig: dict[str, Any]) -> None:
        """Makes the robot move at the given velocity. This is a non-blocking call.

        Args:
            x (float): The velocity along x axis \\[m.s-1\\].
            y (float): The velocity along y axis \\[m.s-1\\].
            theta (float): The velocity around z axis \\[rd.s-1\\].
            moveConfig (dict[str, Any]): An ALValue with custom move configuration.
        """
        ...
    @overload
    async def moveToward(self, x: float, y: float, theta: float) -> None:
        """Makes the robot move at the given normalized velocity. This is a non-blocking call.

        Args:
            x (float): The normalized velocity along x axis (between -1 and 1).
            y (float): The normalized velocity along y axis (between -1 and 1).
            theta (float): The normalized velocity around z axis (between -1 and 1).
        """
        ...
    @overload
    async def moveToward(self, x: float, y: float, theta: float, moveConfig: dict[str, Any]) -> None:
        """Makes the robot move at the given normalized velocity. This is a non-blocking call.

        Args:
            x (float): The normalized velocity along x axis (between -1 and 1).
            y (float): The normalized velocity along y axis (between -1 and 1).
            theta (float): The normalized velocity around z axis (between -1 and 1).
            moveConfig (dict[str, Any]): An ALValue with custom move configuration.
        """
        ...
    @overload
    @deprecated('Use moveToward() function instead.')
    async def setWalkTargetVelocity(self, x: float, y: float, theta: float, frequency: float) -> None:
        """DEPRECATED. Use moveToward() function instead.

        Args:
            x (float): Fraction of MaxStepX. Use negative for backwards. \\[-1.0 to 1.0\\]
            y (float): Fraction of MaxStepY. Use negative for right. \\[-1.0 to 1.0\\]
            theta (float): Fraction of MaxStepTheta. Use negative for clockwise \\[-1.0 to 1.0\\]
            frequency (float): Fraction of MaxStepFrequency \\[0.0 to 1.0\\]
        """
        ...
    @overload
    @deprecated('Use moveToward() function instead.')
    async def setWalkTargetVelocity(self, x: float, y: float, theta: float, frequency: float, feetGaitConfig: dict[str, Any]) -> None:
        """DEPRECATED. Use moveToward() function instead.

        Args:
            x (float): Fraction of MaxStepX. Use negative for backwards. \\[-1.0 to 1.0\\]
            y (float): Fraction of MaxStepY. Use negative for right. \\[-1.0 to 1.0\\]
            theta (float): Fraction of MaxStepTheta. Use negative for clockwise \\[-1.0 to 1.0\\]
            frequency (float): Fraction of MaxStepFrequency \\[0.0 to 1.0\\]
            feetGaitConfig (dict[str, Any]): An ALValue with the custom gait configuration for both feet
        """
        ...
    @overload
    @deprecated('Use moveToward() function instead.')
    async def setWalkTargetVelocity(self, x: float, y: float, theta: float, frequency: float, leftFootMoveConfig: dict[str, Any], rightFootMoveConfig: dict[str, Any]) -> None:
        """DEPRECATED. Use moveToward() function instead.

        Args:
            x (float): Fraction of MaxStepX. Use negative for backwards. \\[-1.0 to 1.0\\]
            y (float): Fraction of MaxStepY. Use negative for right. \\[-1.0 to 1.0\\]
            theta (float): Fraction of MaxStepTheta. Use negative for clockwise \\[-1.0 to 1.0\\]
            frequency (float): Fraction of MaxStepFrequency \\[0.0 to 1.0\\]
            leftFootMoveConfig (dict[str, Any]): An ALValue with custom move configuration for the left foot
            rightFootMoveConfig (dict[str, Any]): An ALValue with custom move configuration for the right foot
        """
        ...
    @overload
    async def moveTo(self, x: float, y: float, theta: float) -> bool:
        """Makes the robot move at the given position. This is a blocking call.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].
            theta (float): The position around z axis \\[rd\\].

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveTo(self, x: float, y: float, theta: float, time: float) -> bool:
        """Makes the robot move at the given position in fixed time. This is a blocking call.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].
            theta (float): The position around z axis \\[rd\\].
            time (float): The time to reach the target position \\[s\\].

        Returns:
            bool: a boolean equal to true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveTo(self, x: float, y: float, theta: float, moveConfig: dict[str, Any]) -> bool:
        """Makes the robot move at the given position. This is a blocking call.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].
            theta (float): The position around z axis \\[rd\\].
            moveConfig (dict[str, Any]): An ALValue with custom move configuration.

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveTo(self, x: float, y: float, theta: float, time: float, moveConfig: dict[str, Any]) -> bool:
        """Makes the robot move at the given position in fixed time. This is a blocking call.

        Args:
            x (float): The position along x axis \\[m\\].
            y (float): The position along y axis \\[m\\].
            theta (float): The position around z axis \\[rd\\].
            time (float): The time to reach the target position \\[s\\].
            moveConfig (dict[str, Any]): An ALValue with custom move configuration.

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveTo(self, controlPoint: Any) -> bool:
        """Makes the robot move to the given relative positions. This is a blocking call.

        Args:
            controlPoint (Any): An ALValue with the control points in FRAME_ROBOT. Each control point is relative to the previous one. \\[\\[x1, y1, theta1\\], ..., \\[xN, yN, thetaN\\]

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveTo(self, controlPoint: Any, moveConfig: dict[str, Any]) -> bool:
        """Makes the robot move to the given relative positions. This is a blocking call.

        Args:
            controlPoint (Any): An ALValue with all the control points in FRAME_ROBOT. Each control point is relative to the previous one. \\[\\[x1, y1, theta1\\], ..., \\[xN, yN, thetaN\\]
            moveConfig (dict[str, Any]): An ALValue with custom move configuration.

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    async def moveAlong(self, trajectory: Any) -> bool:
        """Move along a trajectory

        Args:
            trajectory (Any): An ALValue describing a trajectory.

        Returns:
            bool: true if the moveAlong finished successfully
        """
        ...
    @overload
    async def moveAlong(self, trajectory: Any, scaleFactor: float) -> bool:
        """Move along a trajectory

        Args:
            trajectory (Any): An ALValue describing a trajectory.
            scaleFactor (float): A float between 0 and 1 scaling the trajectory.

        Returns:
            bool: true if the moveAlong finished successfully
        """
        ...
    @overload
    @deprecated('Use moveTo() function instead.')
    async def walkTo(self, x: float, y: float, theta: float) -> bool:
        """DEPRECATED. Use moveTo() function instead.

        Args:
            x (float): Distance along the X axis in meters.
            y (float): Distance along the Y axis in meters.
            theta (float): Rotation around the Z axis in radians \\[-3.1415 to 3.1415\\].

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    @deprecated('Use moveTo() function instead.')
    async def walkTo(self, x: float, y: float, theta: float, feetGaitConfig: dict[str, Any]) -> bool:
        """DEPRECATED. Use moveTo() function instead.

        Args:
            x (float): Distance along the X axis in meters.
            y (float): Distance along the Y axis in meters.
            theta (float): Rotation around the Z axis in radians \\[-3.1415 to 3.1415\\].
            feetGaitConfig (dict[str, Any]): An ALValue with the custom gait configuration for both feet.

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    @deprecated('Use moveTo() function instead.')
    async def walkTo(self, controlPoint: Any) -> bool:
        """DEPRECATED. Use moveTo() function instead.

        Args:
            controlPoint (Any): An ALValue with all the control point in NAO SPACE\\[\\[x1,y1,theta1\\], ..., \\[xN,yN,thetaN\\]

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    @overload
    @deprecated('Use moveTo() function instead.')
    async def walkTo(self, controlPoint: Any, feetGaitConfig: dict[str, Any]) -> bool:
        """DEPRECATED. Use moveTo() function instead.

        Args:
            controlPoint (Any): An ALValue with all the control point in NAO SPACE\\[\\[x1,y1,theta1\\], ..., \\[xN,yN,thetaN\\]
            feetGaitConfig (dict[str, Any]): An ALValue with the custom gait configuration for both feet

        Returns:
            bool: true if the moveTo finished successfully
        """
        ...
    async def setFootSteps(self, legName: list[str], footSteps: Any, timeList: list[float], clearExisting: bool) -> None:
        """Makes Nao do foot step planner. This is a non-blocking call.

        Args:
            legName (list[str]): name of the leg to move('LLeg'or 'RLeg')
            footSteps (Any): \\[x, y, theta\\], \\[Position along X/Y, Orientation round Z axis\\] of the leg relative to the other Leg in \\[meters, meters, radians\\]. Must be less than \\[MaxStepX, MaxStepY, MaxStepTheta\\]
            timeList (list[float]): time list of each foot step
            clearExisting (bool): Clear existing foot steps.
        """
        ...
    async def setFootStepsWithSpeed(self, legName: list[str], footSteps: Any, fractionMaxSpeed: list[float], clearExisting: bool) -> None:
        """Makes Nao do foot step planner with speed. This is a blocking call.

        Args:
            legName (list[str]): name of the leg to move('LLeg'or 'RLeg')
            footSteps (Any): \\[x, y, theta\\], \\[Position along X/Y, Orientation round Z axis\\] of the leg relative to the other Leg in \\[meters, meters, radians\\]. Must be less than \\[MaxStepX, MaxStepY, MaxStepTheta\\]
            fractionMaxSpeed (list[float]): speed of each foot step. Must be between 0 and 1.
            clearExisting (bool): Clear existing foot steps.
        """
        ...
    async def getFootSteps(self) -> Any:
        """Get the foot steps. This is a non-blocking call.

        Returns:
            Any: Give two list of foot steps. The first one give the unchangeable foot step. The second list give the changeable foot steps. Il you use setFootSteps or setFootStepsWithSpeed with clearExisting parmater equal true, walk engine execute unchangeable foot step and remove the other.
        """
        ...
    @deprecated('Use moveInit function instead.')
    async def walkInit(self) -> None:
        """DEPRECATED. Use moveInit function instead."""
        ...
    async def moveInit(self) -> None:
        """Initialize the move process. Check the robot pose and take a right posture. This is blocking called."""
        ...
    @deprecated('Use waitUntilMoveIsFinished function instead.')
    async def waitUntilWalkIsFinished(self) -> None:
        """DEPRECATED. Use waitUntilMoveIsFinished function instead."""
        ...
    async def waitUntilMoveIsFinished(self) -> None:
        """Waits until the move process is finished: This method can be used to block your script/code execution until the move task is totally finished."""
        ...
    @deprecated('Use moveIsActive function instead.')
    async def walkIsActive(self) -> bool:
        """DEPRECATED. Use moveIsActive function instead.

        Returns:
            bool: No description provided
        """
        ...
    async def moveIsActive(self) -> bool:
        """Check if the move process is actif.

        Returns:
            bool: True if move is active
        """
        ...
    @deprecated('Use stopMove function instead.')
    async def stopWalk(self) -> bool:
        """DEPRECATED. Use stopMove function instead.

        Returns:
            bool: No description provided
        """
        ...
    async def stopMove(self) -> bool:
        """Stop Move task safely as fast as possible. The move task is ended less brutally than killMove but more quickly than move(0.0, 0.0, 0.0).
        This is a blocking call.

        Returns:
            bool: No description provided
        """
        ...
    @deprecated('Use getMoveConfig function instead.')
    async def getFootGaitConfig(self, config: str) -> Any:
        """DEPRECATED. Use getMoveConfig function instead.
        Gets the foot Gait config ("MaxStepX", "MaxStepY", "MaxStepTheta",  "MaxStepFrequency", "StepHeight", "TorsoWx", "TorsoWy") 

        Args:
            config (str): a string should be "Max", "Min", "Default"

        Returns:
            Any: An ALvalue with the following form :\\[\\["MaxStepX", value\\],  \\["MaxStepY", value\\],  \\["MaxStepTheta", value\\],  \\["MaxStepFrequency", value\\],  \\["StepHeight", value\\],  \\["TorsoWx", value\\],  \\["TorsoWy", value\\]\\] 
        """
        ...
    async def getMoveConfig(self, config: str) -> Any:
        """Gets the move config.

        Args:
            config (str): a string should be "Max", "Min", "Default"

        Returns:
            Any: An ALvalue with the move config
        """
        ...
    async def getRobotPosition(self, useSensors: bool) -> list[float]:
        """Gets the World Absolute Robot Position.

        Args:
            useSensors (bool): If true, use the sensor values

        Returns:
            list[float]: A vector containing the World Absolute Robot Position. (Absolute Position X, Absolute Position Y, Absolute Angle Z)
        """
        ...
    async def getNextRobotPosition(self) -> list[float]:
        """Gets the World Absolute next Robot Position.
        In fact in the walk algorithm some foot futur foot step are incompressible due to preview control, so this function give the next robot position which is incompressible.
        If the robot doesn't walk this function is equivalent to getRobotPosition(false)


        Returns:
            list[float]: A vector containing the World Absolute next Robot position.(Absolute Position X, Absolute Position Y, Absolute Angle Z)
        """
        ...
    async def getRobotVelocity(self) -> list[float]:
        """Gets the World Absolute Robot Velocity.

        Returns:
            list[float]: A vector containing the World Absolute Robot Velocity. (Absolute Velocity Translation X \\[m.s-1\\], Absolute Velocity Translation Y\\[m.s-1\\], Absolute Velocity Rotation WZ \\[rd.s-1\\])
        """
        ...
    @deprecated('No reason provided')
    async def getWalkArmsEnabled(self) -> Any:
        """DEPRECATED. Gets if Arms Motions are enabled during the Walk Process.

        Returns:
            Any: True Arm Motions are controlled by the Walk Task.
        """
        ...
    @deprecated('No reason provided')
    async def setWalkArmsEnabled(self, leftArmEnabled: bool, rightArmEnabled: bool) -> None:
        """DEPRECATED. Sets if Arms Motions are enabled during the Walk Process.

        Args:
            leftArmEnabled (bool): if true Left Arm motions are controlled by the Walk Task
            rightArmEnabled (bool): if true Right Arm mMotions are controlled by the Walk Task
        """
        ...
    async def getMoveArmsEnabled(self, chainName: str) -> bool:
        """Gets if Arms Motions are enabled during the Move Process.

        Args:
            chainName (str): Name of the chain. Could be: "LArm", "RArm" or "Arms"

        Returns:
            bool: For LArm and RArm true if the corresponding arm is enabled. For Arms, true if both are enabled. False otherwise.
        """
        ...
    async def setMoveArmsEnabled(self, leftArmEnabled: bool, rightArmEnabled: bool) -> None:
        """Sets if Arms Motions are enabled during the Move Process.

        Args:
            leftArmEnabled (bool): if true Left Arm motions are controlled by the Move Task
            rightArmEnabled (bool): if true Right Arm mMotions are controlled by the Move Task
        """
        ...
    @deprecated('Use positionInterpolations function instead.')
    async def positionInterpolation(self, chainName: str, space: int, path: list[list[float]], axisMask: int, durations: Any, isAbsolute: bool) -> None:
        """DEPRECATED. Use positionInterpolations function instead.

        Args:
            chainName (str): Name of the chain. Could be: "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso" 
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            path (list[list[float]]): Vector of 6D position arrays (x,y,z,wx,wy,wz) in meters and radians
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            durations (Any): Vector of times in seconds corresponding to the path points
            isAbsolute (bool): If true, the movement is absolute else relative
        """
        ...
    @overload
    @deprecated('Use the other positionInterpolations function instead.')
    async def positionInterpolations(self, effectorNames: list[str], taskSpaceForAllPaths: int, paths: list[list[float]], axisMasks: list[list[float]], relativeTimes: Any, isAbsolute: bool) -> None:
        """DEPRECATED. Use the other positionInterpolations function instead.

        Args:
            effectorNames (list[str]): Vector of chain names. Could be: "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso" 
            taskSpaceForAllPaths (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            paths (list[list[float]]): Vector of 6D position arrays (x,y,z,wx,wy,wz) in meters and radians
            axisMasks (list[list[float]]): Vector of Axis Masks. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            relativeTimes (Any): Vector of times in seconds corresponding to the path points
            isAbsolute (bool): If true, the movement is absolute else relative
        """
        ...
    @overload
    async def positionInterpolations(self, effectorNames: Any, taskSpaceForAllPaths: Any, paths: list[list[float]], axisMasks: list[list[float]], relativeTimes: Any) -> None:
        """Moves end-effectors to the given positions and orientations over time. This is a blocking call.

        Args:
            effectorNames (Any): Vector of chain names. Could be: "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso" 
            taskSpaceForAllPaths (Any): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            paths (list[list[float]]): Vector of 6D position arrays (x,y,z,wx,wy,wz) in meters and radians
            axisMasks (list[list[float]]): Vector of Axis Masks. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            relativeTimes (Any): Vector of times in seconds corresponding to the path points
        """
        ...
    @deprecated('Use setPositions function instead.')
    async def setPosition(self, chainName: str, space: int, position: list[float], fractionMaxSpeed: float, axisMask: int) -> None:
        """Moves an end-effector to DEPRECATED. Use setPositions function instead.

        Args:
            chainName (str): Name of the chain. Could be: "Head", "LArm","RArm", "LLeg", "RLeg", "Torso"
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            position (list[float]): 6D position array (x,y,z,wx,wy,wz) in meters and radians
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    async def setPositions(self, names: Any, spaces: Any, positions: list[list[float]], fractionMaxSpeed: float, axisMask: Any) -> None:
        """Moves multiple end-effectors to the given position and orientation position6d. This is a non-blocking call.

        Args:
            names (Any): The name or names of effector.
            spaces (Any): The task frame or task frames {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            positions (list[list[float]]): Position6D arrays
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (Any): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    @deprecated('Use setPositions function instead.')
    async def changePosition(self, effectorName: str, space: int, positionChange: list[float], fractionMaxSpeed: float, axisMask: int) -> None:
        """DEPRECATED. Use setPositions function instead.

        Args:
            effectorName (str): Name of the effector.
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            positionChange (list[float]): 6D position change array (xd, yd, zd, wxd, wyd, wzd) in meters and radians
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    async def getPosition(self, name: str, space: int, useSensorValues: bool) -> list[float]:
        """Gets a Position relative to the FRAME. Axis definition: the x axis is positive toward Nao's front, the y from right to left and the z is vertical. The angle convention of Position6D is Rot_z(wz).Rot_y(wy).Rot_x(wx).

        Args:
            name (str): Name of the item. Could be: Head, LArm, RArm, LLeg, RLeg, Torso, CameraTop, CameraBottom, MicroFront, MicroRear, MicroLeft, MicroRight, Accelerometer, Gyrometer, Laser, LFsrFR, LFsrFL, LFsrRR, LFsrRL, RFsrFR, RFsrFL, RFsrRR, RFsrRL, USSensor1, USSensor2, USSensor3, USSensor4. Use getSensorNames for the list of sensors supported on your robot.
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            useSensorValues (bool): If true, the sensor values will be used to determine the position.

        Returns:
            list[float]: Vector containing the Position6D using meters and radians (x, y, z, wx, wy, wz)
        """
        ...
    @deprecated('Use the other transformInterpolations function instead.')
    async def transformInterpolation(self, chainName: str, space: int, path: list[list[float]], axisMask: int, duration: Any, isAbsolute: bool) -> None:
        """DEPRECATED. Use the other transformInterpolations function instead.

        Args:
            chainName (str): Name of the chain. Could be: "Head", "LArm","RArm", "LLeg", "RLeg", "Torso"
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            path (list[list[float]]): Vector of Transform arrays
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            duration (Any): Vector of times in seconds corresponding to the path points
            isAbsolute (bool): If true, the movement is absolute else relative
        """
        ...
    @overload
    @deprecated('Use the other transformInterpolations function instead.')
    async def transformInterpolations(self, effectorNames: list[str], taskSpaceForAllPaths: int, paths: list[list[float]], axisMasks: list[list[float]], relativeTimes: Any, isAbsolute: bool) -> None:
        """DEPRECATED. Use the other transformInterpolations function instead.

        Args:
            effectorNames (list[str]): Vector of chain names. Could be: "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso" 
            taskSpaceForAllPaths (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            paths (list[list[float]]): Vector of transforms arrays.
            axisMasks (list[list[float]]): Vector of Axis Masks. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            relativeTimes (Any): Vector of times in seconds corresponding to the path points
            isAbsolute (bool): If true, the movement is absolute else relative
        """
        ...
    @overload
    async def transformInterpolations(self, effectorNames: Any, taskSpaceForAllPaths: Any, paths: list[list[float]], axisMasks: list[list[float]], relativeTimes: Any) -> None:
        """Moves end-effectors to the given positions and orientations over time. This is a blocking call.

        Args:
            effectorNames (Any): Vector of chain names. Could be: "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso" 
            taskSpaceForAllPaths (Any): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            paths (list[list[float]]): Vector of 6D position arrays (x,y,z,wx,wy,wz) in meters and radians
            axisMasks (list[list[float]]): Vector of Axis Masks. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
            relativeTimes (Any): Vector of times in seconds corresponding to the path points
        """
        ...
    @deprecated('Use setTransforms function instead.')
    async def setTransform(self, chainName: str, space: int, transform: list[float], fractionMaxSpeed: float, axisMask: int) -> None:
        """Moves an end-effector to DEPRECATED. Use setTransforms function instead.

        Args:
            chainName (str): Name of the chain. Could be: "Head", "LArm","RArm", "LLeg", "RLeg", "Torso"
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            transform (list[float]): Transform arrays
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    async def setTransforms(self, names: Any, spaces: Any, transforms: list[list[float]], fractionMaxSpeed: float, axisMask: Any) -> None:
        """Moves multiple end-effectors to the given position and orientation transforms. This is a non-blocking call.

        Args:
            names (Any): The name or names of effector.
            spaces (Any): The task frame or task frames {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            transforms (list[list[float]]): Transform arrays
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (Any): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    @deprecated('Use setTransforms function instead.')
    async def changeTransform(self, chainName: str, space: int, transform: list[float], fractionMaxSpeed: float, axisMask: int) -> None:
        """DEPRECATED. Use setTransforms function instead.

        Args:
            chainName (str): Name of the chain. Could be: "Head", "LArm","RArm", "LLeg", "RLeg", "Torso"
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            transform (list[float]): Transform arrays
            fractionMaxSpeed (float): The fraction of maximum speed to use
            axisMask (int): Axis mask. True for axes that you wish to control. e.g. 7 for position only, 56 for rotation only and 63 for both 
        """
        ...
    async def getTransform(self, name: str, space: int, useSensorValues: bool) -> list[float]:
        """Gets an Homogenous Transform relative to the FRAME. Axis definition: the x axis is positive toward Nao's front, the y from right to left and the z is vertical.

        Args:
            name (str): Name of the item. Could be: any joint or chain or sensor (Head, LArm, RArm, LLeg, RLeg, Torso, HeadYaw, ..., CameraTop, CameraBottom, MicroFront, MicroRear, MicroLeft, MicroRight, Accelerometer, Gyrometer, Laser, LFsrFR, LFsrFL, LFsrRR, LFsrRL, RFsrFR, RFsrFL, RFsrRR, RFsrRL, USSensor1, USSensor2, USSensor3, USSensor4. Use getSensorNames for the list of sensors supported on your robot.
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            useSensorValues (bool): If true, the sensor values will be used to determine the position.

        Returns:
            list[float]: Vector of 16 floats corresponding to the values of the matrix, line by line.
        """
        ...
    async def wbEnable(self, isEnabled: bool) -> None:
        """UserFriendly Whole Body API: enable Whole Body Balancer. It's a Generalized Inverse Kinematics which deals with cartesian control, balance, redundancy and task priority. The main goal is to generate and stabilized consistent motions without precomputed trajectories and adapt nao's behaviour to the situation. The generalized inverse kinematic problem takes in account equality constraints (keep foot fix), inequality constraints (joint limits, balance, ...) and quadratic minimization (cartesian / articular desired trajectories). We solve each step a quadratic programming on the robot.

        Args:
            isEnabled (bool): Active / Disactive Whole Body Balancer.
        """
        ...
    async def wbFootState(self, stateName: str, supportLeg: str) -> None:
        """UserFriendly Whole Body API: set the foot state: fixed foot, constrained in a plane or free.

        Args:
            stateName (str): Name of the foot state. "Fixed" set the foot fixed. "Plane" constrained the Foot in the plane. "Free" set the foot free.
            supportLeg (str): Name of the foot. "LLeg", "RLeg" or "Legs".
        """
        ...
    async def wbEnableBalanceConstraint(self, isEnable: bool, supportLeg: str) -> None:
        """UserFriendly Whole Body API: enable to keep balance in support polygon.

        Args:
            isEnable (bool): Enable Robot to keep balance.
            supportLeg (str): Name of the support leg: "Legs", "LLeg", "RLeg".
        """
        ...
    async def wbGoToBalance(self, supportLeg: str, duration: float) -> bool:
        """Advanced Whole Body API: "Com" go to a desired support polygon. This is a blocking call.

        Args:
            supportLeg (str): Name of the support leg: "Legs", "LLeg", "RLeg".
            duration (float): Time in seconds. Must be upper 0.5 s.

        Returns:
            bool: A boolean of the success of the go to balance.
        """
        ...
    async def wbGoToBalanceWithSpeed(self, supportLeg: str, fractionMaxSpeed: float) -> bool:
        """Advanced Whole Body API: "Com" go to a desired support polygon. This is a blocking call.

        Args:
            supportLeg (str): Name of the support leg: "Legs", "LLeg", "RLeg".
            fractionMaxSpeed (float): The fraction of maximum speed to use.

        Returns:
            bool: A boolean of the success of the go to balance.
        """
        ...
    async def wbEnableEffectorControl(self, effectorName: str, isEnabled: bool) -> None:
        """UserFriendly Whole Body API: enable whole body cartesian control of an effector.

        Args:
            effectorName (str): Name of the effector : "Head", "LArm" or "RArm". Nao goes to posture init. He manages his balance and keep foot fix. "Head" is controlled in rotation. "LArm" and "RArm" are controlled in position.
            isEnabled (bool): Active / Disactive Effector Control.
        """
        ...
    async def wbSetEffectorControl(self, effectorName: str, targetCoordinate: Any) -> None:
        """UserFriendly Whole Body API: set new target for controlled effector. This is a non-blocking call.

        Args:
            effectorName (str): Name of the effector : "Head", "LArm" or "RArm". Nao goes to posture init. He manages his balance and keep foot fix. "Head" is controlled in rotation. "LArm" and "RArm" are controlled in position.
            targetCoordinate (Any): "Head" is controlled in rotation (WX, WY, WZ). "LArm" and "RArm" are controlled in position (X, Y, Z). TargetCoordinate must be absolute and expressed in FRAME_ROBOT. If the desired position/orientation is unfeasible, target is resize to the nearest feasible motion.
        """
        ...
    async def wbEnableEffectorOptimization(self, effectorName: str, isActive: bool) -> None:
        """Advanced Whole Body API: enable to control an effector as an optimization.

        Args:
            effectorName (str): Name of the effector : "All", "Arms", "Legs", "Head", "LArm", "RArm", "LLeg", "RLeg", "Torso", "Com".
            isActive (bool): if true, the effector control is taken in acount in the optimization criteria.
        """
        ...
    async def setCollisionProtectionEnabled(self, pChainName: str, pEnable: bool) -> bool:
        """Enable Anticollision protection of the arms of the robot.

        Args:
            pChainName (str): The chain name {"Arms", "LArm" or "RArm"}.
            pEnable (bool): Activate or disactivate the anticollision of the desired Chain.

        Returns:
            bool: A bool which return always true.
        """
        ...
    async def getCollisionProtectionEnabled(self, pChainName: str) -> bool:
        """Allow to know if the collision protection is activated on the given chain.

        Args:
            pChainName (str): The chain name {"LArm" or "RArm"}.

        Returns:
            bool: Return true is the collision protection of the given Arm is activated.
        """
        ...
    async def setExternalCollisionProtectionEnabled(self, pName: str, pEnable: bool) -> None:
        """Enable Anticollision protection of the arms and base move  of the robot with external environment.

        Args:
            pName (str): The name {"All", "Move", "Arms", "LArm" or "RArm"}.
            pEnable (bool): Activate or disactivate the anticollision of the desired name.
        """
        ...
    async def getChainClosestObstaclePosition(self, pName: str, space: int) -> list[float]:
        """Gets chain closest obstacle Position .

        Args:
            pName (str): The Chain name {"LArm" or "RArm"}.
            space (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.

        Returns:
            list[float]: Vector containing the Position3D in meters (x, y, z)
        """
        ...
    async def getExternalCollisionProtectionEnabled(self, pName: str) -> bool:
        """Allow to know if the external collision protection is activated on the given name.

        Args:
            pName (str): The name {"All", "Move", "Arms", "LArm" or "RArm"}.

        Returns:
            bool: Return true is the external collision protection of the given name is activated.
        """
        ...
    async def setOrthogonalSecurityDistance(self, securityDistance: float) -> None:
        """Defines the orthogonal security distance used with external collision protection "Move".

        Args:
            securityDistance (float): The orthogonal security distance.
        """
        ...
    async def getOrthogonalSecurityDistance(self) -> float:
        """Gets the current orthogonal security distance.

        Returns:
            float: The current orthogonal security distance.
        """
        ...
    async def setTangentialSecurityDistance(self, securityDistance: float) -> None:
        """Defines the tangential security distance used with external collision protection "Move".

        Args:
            securityDistance (float): The tangential security distance.
        """
        ...
    async def getTangentialSecurityDistance(self) -> float:
        """Gets the current tangential security distance.

        Returns:
            float: The current tangential security distance.
        """
        ...
    async def setFallManagerEnabled(self, pEnable: bool) -> None:
        """Enable The fall manager protection for the robot. When a fall is detected the robot adopt a joint configuration to protect himself and cut the stiffness.
        . An memory event called "robotHasFallen" is generated when the fallManager have been activated.

        Args:
            pEnable (bool): Activate or disactivate the smart stiffness.
        """
        ...
    async def getFallManagerEnabled(self) -> bool:
        """Give the state of the fall manager.

        Returns:
            bool: Return true is the fall manager is activated.  
        """
        ...
    async def setPushRecoveryEnabled(self, pEnable: bool) -> None:
        """Enable The push recovery protection for the robot. 

        Args:
            pEnable (bool): Enable the push recovery.
        """
        ...
    async def getPushRecoveryEnabled(self) -> bool:
        """Give the state of the push recovery.

        Returns:
            bool: Return true is the push recovery is activated.  
        """
        ...
    async def setSmartStiffnessEnabled(self, pEnable: bool) -> None:
        """Enable Smart Stiffness for all the joints (True by default), the update take one motion cycle for updating. The smart Stiffness is a gestion of joint maximum torque. More description is available on the red documentation of ALMotion module.

        Args:
            pEnable (bool): Activate or disactivate the smart stiffness.
        """
        ...
    async def getSmartStiffnessEnabled(self) -> bool:
        """Give the state of the smart Stiffness.

        Returns:
            bool: Return true is the smart Stiffnes is activated.  
        """
        ...
    async def setDiagnosisEffectEnabled(self, pEnable: bool) -> None:
        """Enable or disable the diagnosis effect into ALMotion

        Args:
            pEnable (bool): Enable or disable the diagnosis effect.
        """
        ...
    async def getDiagnosisEffectEnabled(self) -> bool:
        """Give the state of the diagnosis effect.

        Returns:
            bool: Return true is the diagnosis reflex is activated.  
        """
        ...
    @deprecated('Use getBodyNames function instead.')
    async def getJointNames(self, name: str) -> list[str]:
        """DEPRECATED. Use getBodyNames function instead.

        Args:
            name (str): Name of a chain, "Arms", "Legs", "Body", "Chains", "JointActuators", "Joints" or "Actuators".

        Returns:
            list[str]: Vector of strings, one for each joint in the collection
        """
        ...
    async def getBodyNames(self, name: str) -> list[str]:
        """Gets the names of all the joints and actuators in the collection.

        Args:
            name (str): Name of a chain, "Arms", "Legs", "Body", "Chains", "JointActuators", "Joints" or "Actuators".

        Returns:
            list[str]: Vector of strings, one for each joint and actuator in the collection
        """
        ...
    async def getSensorNames(self) -> list[str]:
        """Gets the list of sensors supported on your robot.

        Returns:
            list[str]: Vector of sensor names
        """
        ...
    async def getLimits(self, name: str) -> Any:
        """Get the minAngle (rad), maxAngle (rad), and maxVelocity (rad.s-1) for a given joint or actuator in the body.

        Args:
            name (str): Name of a joint, chain, "Body", "JointActuators", "Joints" or "Actuators". 

        Returns:
            Any: Array of ALValue arrays containing the minAngle, maxAngle, maxVelocity and maxTorque for all the bodies specified.
        """
        ...
    async def getMotionCycleTime(self) -> int:
        """Get the motion cycle time in milliseconds.

        Returns:
            int: Expressed in milliseconds
        """
        ...
    @deprecated('use ALRobotModel')
    async def getRobotConfig(self) -> Any:
        """Get the robot configuration. DEPRECATED. use ALRobotModel

        Returns:
            Any: ALValue arrays containing the robot parameter names and the robot parameter values.
        """
        ...
    async def getSummary(self) -> str:
        """Returns a string representation of the Model's state

        Returns:
            str: A formated string
        """
        ...
    async def getMass(self, pName: str) -> float:
        """Gets the mass of a joint, chain, "Body" or "Joints".

        Args:
            pName (str): Name of the body which we want the mass. "Body", "Joints" and "Com" give the total mass of nao. For the chain, it gives the total mass of the chain.

        Returns:
            float: The mass in kg.
        """
        ...
    async def getCOM(self, pName: str, pSpace: int, pUseSensorValues: bool) -> list[float]:
        """Gets the COM of a joint, chain, "Body" or "Joints".

        Args:
            pName (str): Name of the body which we want the mass. In chain name case, this function give the com of the chain.
            pSpace (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            pUseSensorValues (bool): If true, the sensor values will be used to determine the position.

        Returns:
            list[float]: The COM position (meter).
        """
        ...
    async def getSupportPolygon(self, pSpace: int, pUseSensorValues: bool) -> list[list[float]]:
        """Gets the support polygon

        Args:
            pSpace (int): Task frame {FRAME_TORSO = 0, FRAME_WORLD = 1, FRAME_ROBOT = 2}.
            pUseSensorValues (bool): If true, the sensor values will be used to determine the position.

        Returns:
            list[list[float]]: A vector containing the x,y coordinates of each of the outer points of the support polygon in specified frame.
        """
        ...
    async def setMotionConfig(self, config: dict[str, Any]) -> None:
        """Internal Use.

        Args:
            config (dict[str, Any]): Internal: An array of ALValues \\[i\\]\\[0\\]: name, \\[i\\]\\[1\\]: value
        """
        ...
    async def setBreathEnabled(self, pChain: str, pIsEnabled: bool) -> None:
        """This function starts or stops breathing animation on a chain.
        Chain name can be "Body", "Arms", "LArm", "RArm", "Legs" or "Head".
        Head breathing animation will work only if Leg animation is active.

        Args:
            pChain (str): Chain name.
            pIsEnabled (bool): Enables / disables the chain.
        """
        ...
    async def getBreathEnabled(self, pChain: str) -> bool:
        """This function gets the status of breathing animation on a chain.
        Chain name can be "Body", "Arms", "LArm", "RArm", "Legs" or "Head".


        Args:
            pChain (str): Chain name.

        Returns:
            bool: True if breathing animation is enabled on the chain.
        """
        ...
    @deprecated('No reason provided')
    async def setBreathConfig(self, pConfig: dict[str, Any]) -> None:
        """DEPRECATED. This function configures the breathing animation.

        Args:
            pConfig (dict[str, Any]): Breath configuration. An ALValue of the form \\[\\["Bpm", pBpm\\], \\["Amplitude", pAmplitude\\]\\]. pBpm is a float between 10 and 50 setting the breathing frequency in beats per minute. pAmplitude is a float between 0 and 1 setting the amplitude of the breathing animation.
        """
        ...
    @deprecated('No reason provided')
    async def getBreathConfig(self) -> Any:
        """DEPRECATED. This function gets the current breathing configuration.

        Returns:
            Any: An ALValue of the form \\[\\["Bpm", bpm\\], \\["Amplitude", amplitude\\]\\]. bpm is the breathing frequency in beats per minute. amplitude is the normalized amplitude of the breathing animation, between 0 and 1.
        """
        ...
    async def setIdlePostureEnabled(self, pChain: str, pIsEnabled: bool) -> None:
        """Starts or stops idle posture management on a chain.
        Chain name can be "Body", "Arms", "LArm", "RArm", "Legs" or "Head".

        Args:
            pChain (str): Chain name.
            pIsEnabled (bool): Enables / disables the chain.
        """
        ...
    async def getIdlePostureEnabled(self, pChain: str) -> bool:
        """This function gets the status of idle posture management on a chain.
        Chain name can be "Body", "Arms", "LArm", "RArm", "Legs" or "Head".


        Args:
            pChain (str): Chain name.

        Returns:
            bool: True if idle posture management is enabled on the chain.
        """
        ...
    async def getTaskList(self) -> Any:
        """Gets an ALValue structure describing the tasks in the Task List

        Returns:
            Any: An ALValue containing an ALValue for each task. The inner ALValue contains: Name, MotionID
        """
        ...
    async def areResourcesAvailable(self, resourceNames: list[str]) -> bool:
        """Returns true if all the desired resources are available. Only motion API's' blocking call takes resources.

        Args:
            resourceNames (list[str]): A vector of resource names such as joints. Use getBodyNames("Body") to have the list of the available joint for your robot.

        Returns:
            bool: True if the resources are available
        """
        ...
    async def killTask(self, motionTaskID: int) -> bool:
        """Kills a motion task.

        Args:
            motionTaskID (int): TaskID of the motion task you want to kill.

        Returns:
            bool: Return true if the specified motionTaskId has been killed.
        """
        ...
    async def killTasksUsingResources(self, resourceNames: list[str]) -> None:
        """Kills all tasks that use any of the resources given. Only motion API's' blocking call takes resources and can be killed. Use getBodyNames("Body") to have the list of the available joint for your robot.

        Args:
            resourceNames (list[str]): A vector of resource joint names
        """
        ...
    @deprecated('Use killMove function instead.')
    async def killWalk(self) -> None:
        """DEPRECATED. Use killMove function instead."""
        ...
    async def killMove(self) -> None:
        """Emergency Stop on Move task: This method will end the move task brutally, without attempting to return to a balanced state. The robot could easily fall."""
        ...
    async def killAll(self) -> None:
        """Kills all tasks."""
        ...