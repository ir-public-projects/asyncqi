from typing import Any, overload
from typing_extensions import deprecated
class ALWorldRepresentation:
    """"""
    def __init__(self) -> None:
        ...
    async def addAttributeToCategory(self) -> int:
        """Add an attribute to a category.

        Returns:
            int: No description provided
        """
        ...
    async def clearObject(self) -> int:
        """Clear an object.

        Returns:
            int: No description provided
        """
        ...
    async def clearOldPositions(self) -> int:
        """Clear recording of old positions.

        Returns:
            int: No description provided
        """
        ...
    async def createObjectCategory(self) -> int:
        """Create a new object category

        Returns:
            int: No description provided
        """
        ...
    async def removeObjectCategory(self) -> int:
        """Remove an object category

        Returns:
            int: No description provided
        """
        ...
    async def objectCategoryExists(self) -> bool:
        """Tells if an object category exists

        Returns:
            bool: No description provided
        """
        ...
    async def deleteObjectAttribute(self) -> int:
        """Delete an object attribute

        Returns:
            int: No description provided
        """
        ...
    async def findObject(self) -> bool:
        """Check that an object is present.

        Returns:
            bool: No description provided
        """
        ...
    async def load(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def getAttributesFromCategory(self) -> Any:
        """Get all attributes from a category if it exists.

        Returns:
            Any: No description provided
        """
        ...
    async def getChildrenNames(self) -> list[str]:
        """Get the direct children of an object.

        Returns:
            list[str]: No description provided
        """
        ...
    async def getObjectNames(self) -> list[str]:
        """Get the name of the objects.

        Returns:
            list[str]: No description provided
        """
        ...
    async def getObjectAttributes(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    async def getObjectAttributeValues(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    async def getObjectLatestAttributes(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    async def getObjectParentName(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getObjectsInCategory(self) -> list[str]:
        """Get the name of the objects stored in the database.

        Returns:
            list[str]: No description provided
        """
        ...
    async def getObjectCategory(self) -> str:
        """Get the name of the database where the object is stored.

        Returns:
            str: No description provided
        """
        ...
    async def getPosition(self) -> Any:
        """Get the position of an object with quaternion / translation.

        Returns:
            Any: No description provided
        """
        ...
    async def getPosition6D(self) -> list[float]:
        """Get the position from one object to another.

        Returns:
            list[float]: No description provided
        """
        ...
    async def getPosition6DAtTime(self) -> list[float]:
        """Get the interpolated position of an object

        Returns:
            list[float]: No description provided
        """
        ...
    async def getRootName(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def save(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def select(self) -> Any:
        """Select information from a database.

        Returns:
            Any: No description provided
        """
        ...
    async def selectWithOrder(self) -> Any:
        """Select ordered information from a database.

        Returns:
            Any: No description provided
        """
        ...
    async def storeObject(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def storeObjectWithReference(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def storeObjectAttribute(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def updatePosition(self) -> int:
        """Update the position of an object.

        Returns:
            int: No description provided
        """
        ...
    async def updatePositionWithReference(self) -> int:
        """Update the position of an object relative to another.

        Returns:
            int: No description provided
        """
        ...
    async def updateAttribute(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...