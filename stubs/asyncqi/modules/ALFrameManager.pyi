from typing import Any, overload
from typing_extensions import deprecated
class ALFrameManager:
    """Frame manager is used to play choregraphe projects in naoqi. It needs Choregraphe projects in input and will return an ID for each project. It can also only read a given box/timeline in a complex behavior."""
    def __init__(self) -> None:
        ...
    @deprecated('No reason provided')
    async def newBehaviorFromFile(self, xmlFilePath: str, behName: str) -> str:
        """Creates a new behavior, from a box found in an xml file stored in the robot. DEPRECATED since 2.3

        Args:
            xmlFilePath (str): Path to Xml file, ex : "/home/youhou/mybehavior.xar".
            behName (str): 

        Returns:
            str: return a unique identifier for the created box (the box URI), that can be used by playBehavior
        """
        ...
    async def createBehavior(self, packageDir: str, behaviorPath: str, behName: str) -> str:
        """Creates a new behavior, from a box found in an xml file stored in the robot.

        Args:
            packageDir (str):  the base directory of the behavior's package, eg: "/home/myApp".
            behaviorPath (str): the relative path of the behavior inside the package, eg: "/behavior_1/behavior.xar".
            behName (str): 

        Returns:
            str: return a unique identifier for the created box, that can be used by playBehavior
        """
        ...
    @deprecated('No reason provided')
    async def newBehaviorFromChoregraphe(self) -> str:
        """Creates a new behavior, from the current Choregraphe behavior 0(uploaded to /tmp/currentChoregrapheBehavior/behavior.xar). DEPRECATED since 1.14

        Returns:
            str: return a unique identifier for the created behavior (the box URI)
        """
        ...
    async def completeBehavior(self, id: str) -> None:
        """It will play a behavior and block until the behavior is finished. Note that it can block forever if the behavior output is never called.

        Args:
            id (str): The id of the box (the box URI).
        """
        ...
    async def deleteBehavior(self, id: str) -> None:
        """Deletes a behavior (meaning a box). Stop the whole behavior contained in this box first.

        Args:
            id (str): The id of the box to delete (the box URI).
        """
        ...
    async def playBehavior(self, id: str) -> None:
        """Starts a behavior

        Args:
            id (str): The id of the box (the box URI).
        """
        ...
    async def exitBehavior(self, id: str) -> None:
        """Exit the reading of a timeline contained in a given box

        Args:
            id (str): The id of the box (the box URI).
        """
        ...
    async def isBehaviorRunning(self, id: str) -> bool:
        """Tells whether the behavior is running

        Args:
            id (str): The id of the behavior to check (The URI of the root box).

        Returns:
            bool: True if the behavior is running, false otherwise
        """
        ...
    async def cleanBehaviors(self) -> None:
        """Stop playing any behavior in FrameManager, and delete all of them."""
        ...
    async def getBehaviorPath(self, id: str) -> str:
        """Returns a playing behavior absolute path.

        Args:
            id (str): The id of the behavior (The URI of the root box).

        Returns:
            str: Returns the absolute path of given behavior.
        """
        ...
    async def createTimeline(self, timelineContent: str) -> str:
        """Creates a timeline.

        Args:
            timelineContent (str): The timeline content (in XML format).

        Returns:
            str: return a unique identifier for the created box that contains the timeline. You must call deleteBehavior on it at some point. DEPRECATED since 1.14
        """
        ...
    @deprecated('No reason provided')
    async def playTimeline(self, id: str) -> None:
        """Starts playing a timeline contained in a given box. If the box is a flow diagram, it will look for the first onStart input of type Bang, and stimulate it ! DEPRECATED since 1.14

        Args:
            id (str): The id of the box (the URI of the box).
        """
        ...
    @deprecated('No reason provided')
    async def stopTimeline(self, id: str) -> None:
        """Stops playing a timeline contained in a given box, at the current frame. DEPRECATED since 1.14

        Args:
            id (str): The id of the box (the URI of the box).
        """
        ...
    @deprecated('No reason provided')
    async def setTimelineFps(self, id: str, fps: int) -> None:
        """Sets the FPS of a given timeline. DEPRECATED since 1.14

        Args:
            id (str): The id of the timeline (the URI of the box).
            fps (int): The FPS to set.
        """
        ...
    @deprecated('No reason provided')
    async def getTimelineFps(self, id: str) -> int:
        """Gets the FPS of a given timeline. DEPRECATED since 1.14

        Args:
            id (str): The id of the timeline (the URI of the box).

        Returns:
            int: Returns the timeline's FPS.
        """
        ...
    @deprecated('No reason provided')
    async def getMotionLength(self, id: str) -> float:
        """Returns in seconds, the duration of a given movement stored in a box. Returns 0 if the behavior has no motion layers.  DEPRECATED since 1.14

        Args:
            id (str): The id of the box (the URI of the box).

        Returns:
            float: Returns the time in seconds.
        """
        ...
    async def behaviors(self) -> list[str]:
        """List all behaviors currently handled by the frame manager.

        Returns:
            list[str]: a set listing all behavior ids
        """
        ...
    @overload
    @deprecated('Goes to a certain frame and pause. DEPRECATED since 1.14')
    async def gotoAndStop(self, id: str, frame: str) -> None:
        """Goes to a certain frame and pause. DEPRECATED since 1.14

        Args:
            id (str): The id of the box containing the box (the URI of the box).
            frame (str): The behavior frame name we want the timeline to go to. If will jump to the starting index of the name given.
        """
        ...
    @overload
    @deprecated('Goes to a certain frame and pause. DEPRECATED since 1.14')
    async def gotoAndStop(self, id: str, frame: int) -> None:
        """Goes to a certain frame and pause. DEPRECATED since 1.14

        Args:
            id (str): The id of the box containing the box (the URI of the box).
            frame (int): The frame we want the timeline to go to.
        """
        ...
    @overload
    @deprecated('No reason provided')
    async def gotoAndPlay(self, id: str, frame: str) -> None:
        """Goes to a certain frame and continue playing. DEPRECATED since 1.14

        Args:
            id (str): The id of the box containing the box (the URI of the box).
            frame (str): The behavior frame name we want the timeline to go to. If will jump to the starting index of the name given.
        """
        ...
    @overload
    @deprecated('No reason provided')
    async def gotoAndPlay(self, id: str, frame: int) -> None:
        """Goes to a certain frame and continue playing. DEPRECATED since 1.14

        Args:
            id (str): The id of the box containing the box (the URI of the box).
            frame (int): The frame we want the timeline to go to.
        """
        ...
    async def getBehaviorDebuggerFor(self, behavior: str) -> Any:
        """get an object tracking transitions in a behavior

        Args:
            behavior (str): name of the behavior (the URI of the root box)

        Returns:
            Any: No description provided
        """
        ...
    async def getBox(self, box: str) -> Any:
        """get a box as an object

        Args:
            box (str): name of the box (the URI of the box).

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def callBoxInput(self, box: str, method: str, arg: Any) -> Any:
        """call an input on a box

        Args:
            box (str): name of the box (the URI of the box).
            method (str): name of the method
            arg (Any): input argument

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def callBoxInput(self, box: str, method: str, arg1: Any, arg2: Any) -> Any:
        """call an input on a box

        Args:
            box (str): name of the box (the URI of the box). A box URI is of the format 'behavior_name:/diagram_1/box_2'
            method (str): name of the method
            arg1 (Any): input argument
            arg2 (Any): input argument

        Returns:
            Any: No description provided
        """
        ...