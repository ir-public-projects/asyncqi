from typing import Any, overload
from typing_extensions import deprecated
class ALEngagementZones:
    """This module enables to compue the engagement zones of detected people or detected movements"""
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def isPaused(self) -> bool:
        """Gets extractor pause status

        Returns:
            bool: True if the extractor is paused, False if not
        """
        ...
    async def pause(self, status: bool) -> None:
        """Changes the pause status of the extractor

        Args:
            status (bool): New pause satus
        """
        ...
    async def isProcessing(self) -> bool:
        """Gets extractor running status

        Returns:
            bool: True if the extractor is currently processing images, False if not
        """
        ...
    async def setLimitAngle(self, angle: float) -> None:
        """Set the angle used for the delimitation of the engagement zones

        Args:
            angle (float): New angle (in degrees) for delimitation, it should be below 180
        """
        ...
    async def getLimitAngle(self) -> float:
        """Get the angle used for the delimitation of the engagement zones

        Returns:
            float: Current angle used for delimitation
        """
        ...
    async def setFirstLimitDistance(self, distance: float) -> None:
        """Set the first distance used for the delimitation of the engagement zones (nearest limit)

        Args:
            distance (float): New first distance (in meters) for delimitation (nearest limit), it should be positive and smaller than the second distance
        """
        ...
    async def getFirstLimitDistance(self) -> float:
        """Get the first distance used for the delimitation of the engagement zones (nearest limit)

        Returns:
            float: Current first distance (in meters) used for delimitation (nearest limit)
        """
        ...
    async def setSecondLimitDistance(self, distance: float) -> None:
        """Set the second distance used for the delimitation of the engagement zones (furthest limit)

        Args:
            distance (float): New second distance (in meters) for delimitation (furthest limit), it should be positive and bigger than the first distance
        """
        ...
    async def getSecondLimitDistance(self) -> float:
        """Get the second distance used for the delimitation of the engagement zones (furthest limit)

        Returns:
            float: Current second distance (in meters) used for delimitation (furthest limit)
        """
        ...
    @overload
    async def computeEngagementZone(self, x: float, y: float, z: float) -> int:
        """Computes the engagement zone in which an object is from its position in FRAME_ROBOT

        Args:
            x (float): X coordinate of the object in FRAME_ROBOT
            y (float): Y coordinate of the object in FRAME_ROBOT
            z (float): Z coordinate of the object in FRAME_ROBOT

        Returns:
            int: Engagement zone of the object
        """
        ...
    @overload
    async def computeEngagementZone(self, xAngle: float, yAngle: float, distance: float, cameraPositionRobot: Any) -> int:
        """Computes the engagement zone in which an object is from its anglular position in the camera image, its distance from the robot, and the position of the camera in FRAME_ROBOT

        Args:
            xAngle (float): X angular coordinate of the object in the image
            yAngle (float): Y angular coordinate of the object in the image
            distance (float): distance of the object from the robot
            cameraPositionRobot (Any): camera position in FRAME_ROBOT

        Returns:
            int: Engagement zone of the object
        """
        ...