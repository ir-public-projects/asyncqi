from typing import Any, overload
from typing_extensions import deprecated
class ALLauncher:
    """ALlauncher allows to link dynamicaly with library, run executable, unload library, check if module is loaded..."""
    def __init__(self) -> None:
        ...
    async def launchLocal(self, moduleName: str) -> list[str]:
        """Loads dynamicaly a module

        Args:
            moduleName (str): the name of the module to launch or the name of the python script to evaluate

        Returns:
            list[str]: list of modules loaded
        """
        ...
    async def launchExecutable(self, moduleName: str) -> bool:
        """runs an executable and connect it to current broker (executable)

        Args:
            moduleName (str): the name of the module to launch or the name of the script file to execute

        Returns:
            bool: true if ok
        """
        ...
    async def launchScript(self, moduleName: str) -> bool:
        """runs a script connected the current broker

        Args:
            moduleName (str): the name of the script to launch (python)

        Returns:
            bool: true if ok
        """
        ...
    async def launchPythonModule(self, moduleName: str) -> bool:
        """Import a python module

        Args:
            moduleName (str): the name of the module to launch

        Returns:
            bool: true if ok
        """
        ...
    async def isModulePresent(self, strPartOfModuleName: str) -> bool:
        """Tests the existence of an active module in the global system (in same executable or in another executable of the distributed system)

        Args:
            strPartOfModuleName (str): a part of the name of the module to test existence

        Returns:
            bool: the returned value is true if this module is present
        """
        ...
    async def getGlobalModuleList(self) -> list[str]:
        """get the list of modules loaded on the robot and connected on the robot

        Returns:
            list[str]: array of present modules
        """
        ...