from typing import Any, overload
from typing_extensions import deprecated
class ALPanoramaCompass:
    """"""
    def __init__(self) -> None:
        ...
    async def setupPanorama(self) -> int:
        """Shoot a panorama at the current position.

        Returns:
            int: No description provided
        """
        ...
    async def isDataAvailable(self) -> bool:
        """Returns true if there is some panorama data

        Returns:
            bool: No description provided
        """
        ...
    async def getCurrentPosition(self) -> Any:
        """Return the current orientation of the robot in the current panorama.

        Returns:
            Any: No description provided
        """
        ...
    async def localizeNoHint(self) -> list[float]:
        """Localize the robot using the scan,without hint.

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def localize(self) -> list[float]:
        """Localize the robot using the scan.

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def localize(self, pMode: bool) -> list[float]:
        """Localize the robot using the scan.

        Args:
            pMode (bool): Localization mode

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def localize(self, pMode: int) -> list[float]:
        """Localize the robot using the scan.

        Args:
            pMode (int): Localization mode

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def localize(self, pMode: int) -> list[float]:
        """Localize the robot using the scan.

        Args:
            pMode (int): Localization mode

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def isInPanorama(self) -> int:
        """Check if the robot is in the current Panorama.

        Returns:
            int: Error status.
        """
        ...
    async def isRelocalizationRequired(self) -> bool:
        """Is a relocalization movement required.

        Returns:
            bool: No description provided
        """
        ...
    async def loadPanorama(self, id: int) -> int:
        """Load the panorama corresponding to the input identity from the hard drive. It has to exist.

        Args:
            id (int): Identity of the requested Panorama.

        Returns:
            int: Error status.
        """
        ...
    async def getCurrentPanoramaDescriptor(self) -> Any:
        """

        Returns:
            Any: Return an ALValue containing Panorama identity and contained Frames identity.
        """
        ...
    async def getFrame(self, id: int) -> Any:
        """Return the Frame corresponding to the input identity. It have to be in the current Panorama

        Args:
            id (int): Identity of the resquested Frame.

        Returns:
            Any: ALValue containing the Frame image part.
        """
        ...
    @overload
    async def isInPanorama(self) -> int:
        """Check if the robot is in the current Panorama.

        Returns:
            int: Error status.
        """
        ...
    @overload
    async def clearAllPanoramas(self) -> int:
        """Delete all panorama files in the current working directory

        Returns:
            int: No description provided
        """
        ...
    @overload
    async def clearAllPanoramas(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def clearPanorama(self, pIdentity: int) -> int:
        """Delete all files related to a given panorama in the current working directory

        Args:
            pIdentity (int): Panorama identity

        Returns:
            int: No description provided
        """
        ...