from typing import Any, overload
from typing_extensions import deprecated
class ALNotificationManager:
    """Notification manager: Handle all notifications on the robot."""
    def __init__(self) -> None:
        ...
    async def add(self, notification: Any) -> int:
        """Add a notification.

        Args:
            notification (Any): Contain information for the notification

        Returns:
            int: Notification ID.
        """
        ...
    async def remove(self, notificationId: int) -> int:
        """Remove a notification.

        Args:
            notificationId (int): Notification ID to remove.

        Returns:
            int: No description provided
        """
        ...
    async def notifications(self) -> Any:
        """Get the all array of pending notifications.

        Returns:
            Any: An array of pending notification.
        """
        ...
    async def notification(self, notificationId: int) -> Any:
        """Get one notification.

        Args:
            notificationId (int): Notification ID.

        Returns:
            Any: ALValue containing a Notification.
        """
        ...