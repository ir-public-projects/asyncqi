from typing import Any, overload
from typing_extensions import deprecated
class ALUserSession:
    """"""
    def __init__(self) -> None:
        ...
    async def doesUserExist(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def doUsersExist(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getUserList(self) -> list[int]:
        """

        Returns:
            list[int]: No description provided
        """
        ...
    async def getNumUsers(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def getFocusedUser(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def getOpenUserSessions(self) -> list[int]:
        """

        Returns:
            list[int]: No description provided
        """
        ...
    async def isUserSessionOpen(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def areUserSessionsOpen(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def isUserPermanent(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def areUsersPermanent(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getPermanentUserList(self) -> list[int]:
        """

        Returns:
            list[int]: No description provided
        """
        ...
    async def getBindingList(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def doesBindingExist(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getUserBinding(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getUserBindings(self) -> dict[str, str]:
        """

        Returns:
            dict[str, str]: No description provided
        """
        ...
    async def findUsersWithBinding(self) -> list[int]:
        """

        Returns:
            list[int]: No description provided
        """
        ...
    async def getUsidFromPpid(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def getPpidFromUsid(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...
    async def getBindingSources(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def doesBindingSourceExist(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getUserDataSources(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def doesUserDataSourceExist(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getUserData(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def getUserData(self) -> dict[str, Unknown]:
        """

        Returns:
            dict[str, Unknown]: No description provided
        """
        ...
    async def setUserData(self) -> None:
        """"""
        ...
    async def getUserCreationDate(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getFirstEncounterDate(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getCurrentEncounterDate(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getLastEncounterDate(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getSecondsSinceLastEncounter(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...