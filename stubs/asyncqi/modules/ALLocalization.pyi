from typing import Any, overload
from typing_extensions import deprecated
class ALLocalization:
    """"""
    def __init__(self) -> None:
        ...
    async def getMessageFromErrorCode(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def stopAll(self) -> None:
        """Stop all robot movements."""
        ...
    async def learnHome(self) -> int:
        """Learn the robot home.

        Returns:
            int: No description provided
        """
        ...
    async def isInCurrentHome(self) -> bool:
        """Is the robot in its home?

        Returns:
            bool: No description provided
        """
        ...
    async def getCurrentPanoramaDescriptor(self) -> Any:
        """Get some information about the current panorama.

        Returns:
            Any: No description provided
        """
        ...
    async def getFrame(self) -> Any:
        """Get a frame buffer.

        Returns:
            Any: No description provided
        """
        ...
    async def clear(self, pDirectory: str) -> int:
        """Delete all panoramas in a directory.

        Args:
            pDirectory (str): Name of the directory

        Returns:
            int: No description provided
        """
        ...
    async def load(self, pDirectory: str) -> int:
        """Loads panoramas from a directory in the default one.

        Args:
            pDirectory (str): Name of the directory

        Returns:
            int: No description provided
        """
        ...
    async def save(self, pDirectory: str) -> int:
        """Save the temporary panoramas in a directory from the default one.

        Args:
            pDirectory (str): Name of the directory

        Returns:
            int: No description provided
        """
        ...
    async def isRelocalizationRequired(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getDriftPercentages(self) -> list[float]:
        """

        Returns:
            list[float]: No description provided
        """
        ...
    async def isDataAvailable(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def getRobotPosition(self) -> list[float]:
        """Get the robot position in world navigation.

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def getRobotPosition(self) -> list[float]:
        """Get the robot position in world navigation.

        Returns:
            list[float]: No description provided
        """
        ...
    @overload
    async def getRobotOrientation(self) -> Any:
        """Get the robot orientation.

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def getRobotOrientation(self) -> Any:
        """Get the robot orientation.

        Returns:
            Any: No description provided
        """
        ...
    async def isInGivenZone(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    async def goToHome(self) -> int:
        """Go to the robot home.

        Returns:
            int: No description provided
        """
        ...
    async def goToPosition(self) -> int:
        """Go to a given position.

        Returns:
            int: No description provided
        """
        ...