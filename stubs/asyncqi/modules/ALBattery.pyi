from typing import Any, overload
from typing_extensions import deprecated
class ALBattery:
    """"""
    def __init__(self) -> None:
        ...
    async def getBatteryCharge(self) -> int:
        """

        Returns:
            int: No description provided
        """
        ...