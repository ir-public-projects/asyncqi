from typing import Any, overload
from typing_extensions import deprecated
class ALBackgroundMovement:
    """"""
    def __init__(self) -> None:
        ...
    async def setEnabled(self) -> None:
        """"""
        ...
    async def isEnabled(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...