from typing import Any, overload
from typing_extensions import deprecated
class ALBodyTemperature:
    """Deals with motor temperature."""
    def __init__(self) -> None:
        ...