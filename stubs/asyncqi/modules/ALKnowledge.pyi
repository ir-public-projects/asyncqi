from typing import Any, overload
from typing_extensions import deprecated
class ALKnowledge:
    """"""
    def __init__(self) -> None:
        ...
    async def add(self) -> None:
        """"""
        ...
    async def addExpirationDate(self) -> None:
        """"""
        ...
    async def updateExpirationDate(self) -> None:
        """"""
        ...
    async def addHappeningDate(self) -> None:
        """"""
        ...
    async def updateHappeningDate(self) -> None:
        """"""
        ...
    async def getSubject(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def getPredicate(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def getObject(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def update(self) -> None:
        """"""
        ...
    async def queryTriplet(self) -> list[list[str]]:
        """

        Returns:
            list[list[str]]: No description provided
        """
        ...
    async def remove(self) -> None:
        """"""
        ...
    async def contains(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    @overload
    async def query(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    @overload
    async def query(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    @overload
    async def queryTripletWithMetadata(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def queryTripletWithMetadata(self) -> Any:
        """

        Returns:
            Any: No description provided
        """
        ...
    @overload
    async def addRule(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    @overload
    async def addRule(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getRules(self) -> dict[str, str]:
        """

        Returns:
            dict[str, str]: No description provided
        """
        ...
    async def removeRule(self) -> None:
        """"""
        ...
    @overload
    async def clearRules(self) -> None:
        """"""
        ...
    @overload
    async def clearRules(self) -> None:
        """"""
        ...
    async def clearInference(self) -> None:
        """"""
        ...
    async def performInference(self) -> None:
        """"""
        ...
    async def resetKnowledge(self) -> None:
        """"""
        ...
    async def exportModel(self) -> None:
        """"""
        ...
    async def importModel(self) -> None:
        """"""
        ...
    @overload
    async def sparqlQuery(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    @overload
    async def sparqlQuery(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def createBackupModel(self) -> None:
        """"""
        ...
    async def recoverBackupModel(self) -> None:
        """"""
        ...
    async def addMetadata(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def queryMetadata(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...
    async def updateMetadata(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def containsMetadata(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def removeMetadata(self) -> bool:
        """

        Returns:
            bool: No description provided
        """
        ...
    async def getId(self) -> str:
        """

        Returns:
            str: No description provided
        """
        ...
    async def getTripletFromId(self) -> list[str]:
        """

        Returns:
            list[str]: No description provided
        """
        ...