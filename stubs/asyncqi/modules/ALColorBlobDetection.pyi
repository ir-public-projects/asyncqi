from typing import Any, overload
from typing_extensions import deprecated
class ALColorBlobDetection:
    """ALColorBlobDetection is a module which can detect blobs of a certain color.
      The output value is written in ALMemory has a Tracker microEvent.
 
    """
    def __init__(self) -> None:
        ...
    @overload
    async def subscribe(self, name: str, period: int, precision: float) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
            period (int): Refresh period (in milliseconds) if relevant.
            precision (float): Precision of the extractor if relevant.
        """
        ...
    @overload
    async def subscribe(self, name: str) -> None:
        """Subscribes to the extractor. This causes the extractor to start writing information to memory using the keys described by getOutputNames(). These can be accessed in memory using ALMemory.getData("keyName"). In many cases you can avoid calling subscribe on the extractor by just calling ALMemory.subscribeToEvent() supplying a callback method. This will automatically subscribe to the extractor for you.

        Args:
            name (str): Name of the module which subscribes.
        """
        ...
    async def unsubscribe(self, name: str) -> None:
        """Unsubscribes from the extractor.

        Args:
            name (str): Name of the module which had subscribed.
        """
        ...
    async def updatePeriod(self, name: str, period: int) -> None:
        """Updates the period if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            period (int): Refresh period (in milliseconds).
        """
        ...
    async def updatePrecision(self, name: str, precision: float) -> None:
        """Updates the precision if relevant.

        Args:
            name (str): Name of the module which has subscribed.
            precision (float): Precision of the extractor.
        """
        ...
    async def getCurrentPeriod(self) -> int:
        """Gets the current period.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getCurrentPrecision(self) -> float:
        """Gets the current precision.

        Returns:
            float: Precision of the extractor.
        """
        ...
    async def getMyPeriod(self, name: str) -> int:
        """Gets the period for a specific subscription.

        Args:
            name (str): Name of the module which has subscribed.

        Returns:
            int: Refresh period (in milliseconds).
        """
        ...
    async def getMyPrecision(self, name: str) -> float:
        """Gets the precision for a specific subscription.

        Args:
            name (str): name of the module which has subscribed

        Returns:
            float: precision of the extractor
        """
        ...
    async def getSubscribersInfo(self) -> Any:
        """Gets the parameters given by the module.

        Returns:
            Any: Array of names and parameters of all subscribers.
        """
        ...
    async def getOutputNames(self) -> list[str]:
        """Get the list of values updated in ALMemory.

        Returns:
            list[str]: Array of values updated by this extractor in ALMemory
        """
        ...
    async def getEventList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    async def getMemoryKeyList(self) -> list[str]:
        """Get the list of events updated in ALMemory.

        Returns:
            list[str]: Array of events updated by this extractor in ALMemory
        """
        ...
    @overload
    async def setFrameRate(self, subscriberName: str, framerate: int) -> bool:
        """Sets the extractor framerate for a chosen subscriber

        Args:
            subscriberName (str): Name of the subcriber
            framerate (int): New framerate

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    @overload
    async def setFrameRate(self, framerate: int) -> bool:
        """Sets the extractor framerate for all the subscribers

        Args:
            framerate (int): New framerate

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def setResolution(self, resolution: int) -> bool:
        """Sets extractor resolution

        Args:
            resolution (int): New resolution

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    async def setActiveCamera(self, cameraId: int) -> bool:
        """Sets extractor active camera

        Args:
            cameraId (int): Id of the camera that will become the active camera

        Returns:
            bool: True if the update succeeded, False if not
        """
        ...
    @deprecated('DEPRECATED: Sets pause and resolution')
    async def setParameter(self, paramName: str, value: Any) -> None:
        """DEPRECATED: Sets pause and resolution

        Args:
            paramName (str): Name of the parameter to set
            value (Any): New value
        """
        ...
    async def getFrameRate(self) -> int:
        """Gets extractor framerate

        Returns:
            int: Current value of the framerate of the extractor
        """
        ...
    async def getResolution(self) -> int:
        """Gets extractor resolution

        Returns:
            int: Current value of the resolution of the extractor
        """
        ...
    async def getActiveCamera(self) -> int:
        """Gets extractor active camera

        Returns:
            int: Id of the current active camera of the extractor
        """
        ...
    async def isPaused(self) -> bool:
        """Gets extractor pause status

        Returns:
            bool: True if the extractor is paused, False if not
        """
        ...
    async def isProcessing(self) -> bool:
        """Gets extractor running status

        Returns:
            bool: True if the extractor is currently processing images, False if not
        """
        ...
    async def pause(self, paused: bool) -> None:
        """Changes the pause status of the extractor

        Args:
            paused (bool): New pause satus
        """
        ...
    async def setColor(self, r: int, g: int, b: int, colorThres: int) -> None:
        """Color parameter setting

        Args:
            r (int): The R component in RGB of the color to find
            g (int): The G component in RGB of the color to find
            b (int): The B component in RGB of the color to find
            colorThres (int): The color threshold
        """
        ...
    @overload
    async def setObjectProperties(self, minSize: int, span: float) -> None:
        """Object parameter setting

        Args:
            minSize (int): The minimum size of the cluster to find
            span (float): The span of the object in meters
        """
        ...
    @overload
    async def setObjectProperties(self, minSize: int, span: float, shape: str) -> None:
        """Object parameter setting

        Args:
            minSize (int): The minimum size of the cluster to find
            span (float): The span of the object in meters
            shape (str): The shape of the object
        """
        ...
    async def getCircle(self) -> Any:
        """Send back the x,y,radius of the circle if any

        Returns:
            Any: The circle as x,y,radius in image relative coordinates (x,radius divided by rows and y by cols)
        """
        ...
    async def getAutoExposure(self) -> bool:
        """Get the camera auto exposure mode

        Returns:
            bool: A flag saying the exposure is auto or not
        """
        ...
    async def setAutoExposure(self, mode: bool) -> None:
        """Set the camera auto exposure to on

        Args:
            mode (bool): Whether the exposure is auto or not
        """
        ...