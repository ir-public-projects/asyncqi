from typing import Any, overload
from typing_extensions import deprecated
class ALResourceManager:
    """Manage robot resources: Synchronize movement, led, sound. Run specific actions when another behavior wants your resources"""
    def __init__(self) -> None:
        ...
    async def waitForResource(self, resourceName: str, ownerName: str, callbackName: str, timeoutSeconds: int) -> None:
        """Wait resource

        Args:
            resourceName (str): Resource name
            ownerName (str): Module name
            callbackName (str): callback name
            timeoutSeconds (int): Timeout in seconds
        """
        ...
    async def acquireResource(self, resourceName: str, moduleName: str, callbackName: str, timeoutSeconds: int) -> None:
        """Wait and acquire a resource

        Args:
            resourceName (str): Resource name
            moduleName (str): Module name
            callbackName (str): callback name
            timeoutSeconds (int): Timeout in seconds
        """
        ...
    async def waitForOptionalResourcesTree(self) -> list[str]:
        """Wait resource

        Returns:
            list[str]: No description provided
        """
        ...
    async def waitForResourcesTree(self, resourceName: list[str], moduleName: str, callbackName: str, timeoutSeconds: int) -> None:
        """Wait for resource tree. Parent and children are not in conflict. Local function

        Args:
            resourceName (list[str]): Resource name
            moduleName (str): Module name
            callbackName (str): callback name
            timeoutSeconds (int): Timeout in seconds
        """
        ...
    async def acquireResourcesTree(self, resourceName: list[str], moduleName: str, callbackName: str, timeoutSeconds: int) -> None:
        """Wait for resource tree. Parent and children are not in conflict. Local function

        Args:
            resourceName (list[str]): Resource name
            moduleName (str): Module name
            callbackName (str): callback name
            timeoutSeconds (int): Timeout in seconds
        """
        ...
    async def areResourcesOwnedBy(self, resourceNameList: list[str], ownerName: str) -> bool:
        """True if all the specified resources are owned by the owner

        Args:
            resourceNameList (list[str]): Resource name
            ownerName (str): Owner pointer with hierarchy

        Returns:
            bool: True if all the specify resources are owned by the owner
        """
        ...
    async def releaseResource(self, resourceName: str, ownerName: str) -> None:
        """Release resource

        Args:
            resourceName (str): Resource name
            ownerName (str): Existing owner name
        """
        ...
    async def releaseResources(self, resourceNames: list[str], ownerName: str) -> None:
        """Release  resources list

        Args:
            resourceNames (list[str]): Resource names
            ownerName (str): Owner name
        """
        ...
    async def enableStateResource(self, resourceName: str, enabled: bool) -> None:
        """Enable or disable a state resource

        Args:
            resourceName (str): The name of the resource that you wish enable of disable. e.g. Standing
            enabled (bool): True to enable, false to disable
        """
        ...
    async def checkStateResourceFree(self, resourceName: list[str]) -> bool:
        """check if all the state resource in the list are free

        Args:
            resourceName (list[str]): Resource name

        Returns:
            bool: No description provided
        """
        ...
    async def areResourcesFree(self, resourceNames: list[str]) -> bool:
        """True if all resources are free

        Args:
            resourceNames (list[str]): Resource names

        Returns:
            bool: True if all the specify resources are free
        """
        ...
    async def isResourceFree(self, resourceNames: str) -> bool:
        """True if the resource is free

        Args:
            resourceNames (str): Resource name

        Returns:
            bool: True if the specify resources is free
        """
        ...
    async def createResource(self, resourceName: str, parentResourceName: str) -> None:
        """Create a resource

        Args:
            resourceName (str): Resource name to create
            parentResourceName (str): Parent resource name or empty string for root resource
        """
        ...
    async def deleteResource(self, resourceName: str, deleteChildResources: bool) -> None:
        """Delete a root resource

        Args:
            resourceName (str): Resource name to delete
            deleteChildResources (bool): DEPRECATED: Delete child resources if true
        """
        ...
    async def isInGroup(self, resourceGroupName: str, resourceName: str) -> bool:
        """True if a resource is in another parent resource

        Args:
            resourceGroupName (str): Group name. Ex: Arm
            resourceName (str): Resource name

        Returns:
            bool: No description provided
        """
        ...
    async def createResourcesList(self, resourceGroupName: list[str], resourceName: str) -> None:
        """True if a resource is in another parent resource

        Args:
            resourceGroupName (list[str]): Group name. Ex: Arm
            resourceName (str): Resource name
        """
        ...
    async def getResources(self) -> Any:
        """Get tree of resources

        Returns:
            Any: No description provided
        """
        ...
    async def ownersGet(self) -> Any:
        """The tree of the resources owners.

        Returns:
            Any: No description provided
        """
        ...