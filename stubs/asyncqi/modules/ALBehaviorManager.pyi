from typing import Any, overload
from typing_extensions import deprecated
class ALBehaviorManager:
    """This module is intended to manage behaviors. With this module, you can load, start, stop behaviors, add default behaviors or remove them. 

    """
    def __init__(self) -> None:
        ...
    async def preloadBehavior(self, behavior: str) -> bool:
        """Load a behavior

        Args:
            behavior (str): Behavior name 

        Returns:
            bool: Returns true if it was successfully loaded.
        """
        ...
    async def startBehavior(self, behavior: str) -> None:
        """Starts a behavior, returns when started.

        Args:
            behavior (str): Behavior name 
        """
        ...
    async def runBehavior(self, behavior: str) -> None:
        """Runs a behavior, returns when finished

        Args:
            behavior (str): Behavior name 
        """
        ...
    async def stopBehavior(self, behavior: str) -> None:
        """Stop a behavior

        Args:
            behavior (str): Behavior name 
        """
        ...
    async def stopAllBehaviors(self) -> None:
        """Stop all behaviors"""
        ...
    async def isBehaviorInstalled(self, name: str) -> bool:
        """Tell if supplied name corresponds to a behavior that has been installed

        Args:
            name (str): The behavior directory name

        Returns:
            bool: Returns true if it is a valid behavior
        """
        ...
    async def isBehaviorPresent(self, prefixedBehavior: str) -> bool:
        """Tell if the supplied namecorresponds to an existing behavior.

        Args:
            prefixedBehavior (str): Prefixed behavior or just behavior's name (latter usage deprecated, in this case the behavior is searched for amongst user's behaviors, then in system behaviors) DEPRECATED in favor of ALBehaviorManager.isBehaviorInstalled.

        Returns:
            bool: Returns true if it is an existing behavior
        """
        ...
    async def getBehaviorNames(self) -> list[str]:
        """Get behaviors

        Returns:
            list[str]: Returns the list of behaviors prefixed by their type (User/ or System/). DEPRECATED in favor of ALBehaviorManager.getInstalledBehaviors.
        """
        ...
    async def getUserBehaviorNames(self) -> list[str]:
        """Get user's behaviors

        Returns:
            list[str]: Returns the list of user's behaviors prefixed by User/. DEPRECATED in favor of ALBehaviorManager.getInstalledBehaviors.
        """
        ...
    async def getSystemBehaviorNames(self) -> list[str]:
        """Get system behaviors

        Returns:
            list[str]: Returns the list of system behaviors prefixed by System/. DEPRECATED in favor of ALBehaviorManager.getInstalledBehaviors.
        """
        ...
    async def getInstalledBehaviors(self) -> list[str]:
        """Get installed behaviors directories names

        Returns:
            list[str]: Returns the behaviors list
        """
        ...
    async def getBehaviorsByTag(self, tag: str) -> list[str]:
        """Get installed behaviors directories names and filter it by tag.

        Args:
            tag (str): A tag to filter the list with.

        Returns:
            list[str]: Returns the behaviors list
        """
        ...
    async def isBehaviorRunning(self, behavior: str) -> bool:
        """Tell if supplied name corresponds to a running behavior

        Args:
            behavior (str): Behavior name 

        Returns:
            bool: Returns true if it is a running behavior
        """
        ...
    async def isBehaviorLoaded(self, behavior: str) -> bool:
        """Tell if supplied name corresponds to a loaded behavior

        Args:
            behavior (str): Behavior name 

        Returns:
            bool: Returns true if it is a loaded behavior
        """
        ...
    async def getRunningBehaviors(self) -> list[str]:
        """Get running behaviors

        Returns:
            list[str]: Return running behaviors
        """
        ...
    async def getLoadedBehaviors(self) -> list[str]:
        """Get loaded behaviors

        Returns:
            list[str]: Return loaded behaviors
        """
        ...
    async def getTagList(self) -> list[str]:
        """Get tags found on installed behaviors.

        Returns:
            list[str]: The list of tags found.
        """
        ...
    async def getBehaviorTags(self, behavior: str) -> list[str]:
        """Get tags found on the given behavior.

        Args:
            behavior (str): The local path towards a behavior or a directory.

        Returns:
            list[str]: The list of tags found.
        """
        ...
    async def getBehaviorNature(self, behavior: str) -> str:
        """Get the nature of the given behavior.

        Args:
            behavior (str): The local path towards a behavior or a directory.

        Returns:
            str: The nature of the behavior.
        """
        ...
    async def addDefaultBehavior(self, behavior: str) -> None:
        """Set the given behavior as default

        Args:
            behavior (str): Behavior name 
        """
        ...
    async def removeDefaultBehavior(self, behavior: str) -> None:
        """Remove the given behavior from the default behaviors

        Args:
            behavior (str): Behavior name 
        """
        ...
    async def getDefaultBehaviors(self) -> list[str]:
        """Get default behaviors

        Returns:
            list[str]: Return default behaviors
        """
        ...
    async def playDefaultProject(self) -> None:
        """Play default behaviors"""
        ...
    async def resolveBehaviorName(self, name: str) -> str:
        """Find out the actual <package>/<behavior> path behind a behavior name.

        Args:
            name (str): name of a behavior

        Returns:
            str: The actual <package>/<behavior> path if found, else an empty string. Throws an ALERROR if two behavior names conflicted.
        """
        ...