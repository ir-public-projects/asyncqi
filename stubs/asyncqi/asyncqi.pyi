from .modules import *

import qi

class AsyncQi:
    al_file_manager: ALFileManager
    al_memory: ALMemory
    al_logger: ALLogger
    al_preferences: ALPreferences
    al_launcher: ALLauncher
    al_debug: ALDebug
    al_autonomous_moves: ALAutonomousMoves
    al_notification_manager: ALNotificationManager
    al_preference_manager: ALPreferenceManager
    al_autonomous_blinking: ALAutonomousBlinking
    al_resource_manager: ALResourceManager
    al_knowledge: ALKnowledge
    al_user_info: ALUserInfo
    al_thinking_expression: ALThinkingExpression
    al_expression_watcher: ALExpressionWatcher
    al_robot_model: ALRobotModel
    al_sonar: ALSonar
    al_fsr: ALFsr
    al_sensors: ALSensors
    al_body_temperature: ALBodyTemperature
    al_robot_posture: ALRobotPosture
    al_motion: ALMotion
    al_touch: ALTouch
    al_battery: ALBattery
    al_motion_recorder: ALMotionRecorder
    al_leds: ALLeds
    al_world_representation: ALWorldRepresentation
    al_audio_player: ALAudioPlayer
    al_text_to_speech: ALTextToSpeech
    al_frame_manager: ALFrameManager
    al_python_bridge: ALPythonBridge
    al_video_device: ALVideoDevice
    al_red_ball_detection: ALRedBallDetection
    al_vision_recognition: ALVisionRecognition
    al_behavior_manager: ALBehaviorManager
    al_color_blob_detection: ALColorBlobDetection
    al_navigation: ALNavigation
    al_visual_space_history: ALVisualSpaceHistory
    al_animated_speech: ALAnimatedSpeech
    al_speaking_movement: ALSpeakingMovement
    al_listening_movement: ALListeningMovement
    al_background_movement: ALBackgroundMovement
    al_animation_player: ALAnimationPlayer
    al_tracker: ALTracker
    al_movement_detection: ALMovementDetection
    al_people_perception: ALPeoplePerception
    al_sitting_people_detection: ALSittingPeopleDetection
    al_user_session: ALUserSession
    al_engagement_zones: ALEngagementZones
    al_gaze_analysis: ALGazeAnalysis
    al_waving_detection: ALWavingDetection
    al_mood: ALMood
    al_visual_compass: ALVisualCompass
    al_robot_mood: ALRobotMood
    al_localization: ALLocalization
    al_basic_awareness: ALBasicAwareness
    al_panorama_compass: ALPanoramaCompass
    al_dialog: ALDialog
    al_autonomous_life: ALAutonomousLife
    def __init__(self, ip: str = "nao.local", port: int = 9559): ...

    def connect_signal(self, signal: str) -> AsyncSignal: ...

    def get_session(self) -> qi.Session: ...
