import asyncio
import threading
from typing import Any, Callable
from contextlib import AbstractContextManager
import qi
import logging

logger = logging.getLogger(__name__)


def parse_service_name(service_name: str) -> str:
    """Convert service name to NAOqi format (e.g. 'al_motion' -> 'ALMotion')."""
    if service_name.startswith("al_"):
        components = service_name[3:].split("_")
        return "AL" + "".join(x.title() for x in components)
    components = service_name.split("_")
    return "".join(x.title() for x in components)


class AsyncQi:
    def __init__(self, ip: str = "nao.local", port: int = 9559):
        self._services: dict[str, "AsyncQiModule"] = {}
        self._session = qi.Session()
        self._session_url = f"tcp://{ip}:{port}"
        self._connected = asyncio.Event()
        self._main_loop = asyncio.get_event_loop()
        self._shutting_down = False

        def on_connect():
            self._main_loop.call_soon_threadsafe(self._connected.set)

        def on_disconnect(reason: str):
            self._main_loop.call_soon_threadsafe(self._handle_disconnect, reason)

        # Set up connection signals
        self._connection_signal = self._session.connected.connect(on_connect)
        self._disconnection_signal = self._session.disconnected.connect(on_disconnect)

        # Initial connection
        self._connect()

    def _connect(self):
        """Attempt to connect to NAOqi."""
        try:
            self._session.connect(self._session_url)
        except RuntimeError:
            self._main_loop.create_task(self._reconnect())

    async def _reconnect(self):
        """Reconnection loop."""
        while not self._connected.is_set() and not self._shutting_down:
            try:
                self._session.connect(self._session_url)
                return
            except (RuntimeError, asyncio.CancelledError):
                try:
                    await asyncio.sleep(5)
                except asyncio.CancelledError:
                    if self._shutting_down:
                        return
                    continue

    def _handle_disconnect(self, reason):
        """Handle disconnection in the main thread."""
        self._connected.clear()
        logger.warning(f"Disconnected from NAOqi: {reason}")
        # Reset all services
        for service in self._services.values():
            service._reset()
        # Start reconnection if not shutting down
        if not self._shutting_down:
            self._main_loop.create_task(self._reconnect())

    def __getattr__(self, name: str) -> "AsyncQiModule":
        """Get a service by name."""
        service_name = parse_service_name(name)
        if service_name not in self._services:
            self._services[service_name] = AsyncQiModule(
                lambda: self._get_service(service_name)
            )
        return self._services[service_name]

    async def _get_service(self, name: str):
        """Get a NAOqi service, waiting for connection if necessary."""
        while not self._shutting_down:
            try:
                await self._connected.wait()

                service = self._session.service(name)
                if hasattr(service, "ping"):
                    # Try to ping the service to ensure it's really ready
                    await wrap_future(qi.runAsync(service.ping))
                return service
            except (RuntimeError, asyncio.CancelledError):
                if self._shutting_down:
                    raise RuntimeError("Shutdown in progress")
                await asyncio.sleep(1)

    def connect_signal(self, signal_name: str) -> "AsyncSignal":
        """Connect to a NAOqi signal."""
        return AsyncSignal(
            self._session.service("ALMemory").subscriber(signal_name).signal,
            self._main_loop,
        )

    async def close(self):
        """Close the connection and clean up."""
        self._shutting_down = True
        if self._connection_signal is not None:
            self._session.connected.disconnect(self._connection_signal)
        if self._disconnection_signal is not None:
            self._session.disconnected.disconnect(self._disconnection_signal)
        self._session.close()
        self._connected.clear()


class AsyncQiModule:
    def __init__(self, get_service: Callable[[], Any]):
        self._get_service = get_service
        self._service = None
        self._no_retry_methods = {"say", "say_to_file", "sayToFile"}
        self._last_error_time = 0

    def _reset(self):
        """Reset the service instance."""
        self._service = None

    async def _ensure_service(self):
        """Ensure we have a valid service instance."""
        try:
            if self._service is None:
                self._service = await self._get_service()
            # Try to ping to verify service is still valid
            if hasattr(self._service, "ping"):
                await wrap_future(qi.runAsync(self._service.ping))
            return self._service
        except (RuntimeError, AttributeError):
            self._service = None
            raise

    def __getattr__(self, name: str) -> Any:
        async def wrapper(*args, **kwargs):
            while True:
                try:
                    service = await self._ensure_service()
                    method = getattr(service, name)
                    return await wrap_future(qi.runAsync(method, *args, **kwargs))
                except AttributeError as e:
                    logger.error(f"Attribute error: {e}")
                    raise
                except asyncio.CancelledError:
                    raise
                except RuntimeError as e:
                    error_msg = str(e).lower()
                    logger.debug(f"error caught for {name} with msg {error_msg}")
                    if (
                        "not connected" in error_msg
                        or "future canceled" in error_msg
                        or "service removed" in error_msg
                    ):
                        if name in self._no_retry_methods:
                            logger.debug(
                                f"TTS method {name} interrupted by disconnect, completing silently"
                            )
                            self._reset()  # Important: reset service for future calls
                            return None
                        self._reset()
                        try:
                            await asyncio.sleep(1)
                        except asyncio.CancelledError:
                            raise
                        continue
                    raise

        return wrapper


class AsyncSignal(AbstractContextManager):
    def __init__(self, qi_signal, loop):
        self.link = None
        self.qi_signal = qi_signal
        self.loop = loop

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.disconnect()

    def connect(self, callback):
        def wrapped_callback(value):
            async def safe_callback():
                try:
                    await callback(value)
                except asyncio.CancelledError:
                    pass
                except Exception:
                    # Log other exceptions but don't propagate
                    pass

            if threading.current_thread() is threading.main_thread():
                self.loop.create_task(safe_callback())
            else:
                self.loop.call_soon_threadsafe(
                    lambda: self.loop.create_task(safe_callback())
                )

        self.link = self.qi_signal.connect(wrapped_callback)
        return self

    def disconnect(self):
        if self.link is not None:
            try:
                self.qi_signal.disconnect(self.link)
            except RuntimeError:
                pass  # Ignore errors during disconnect
            self.link = None


def wrap_future(qi_future: qi.Future) -> asyncio.Future:
    """Convert a qi.Future to an asyncio.Future."""
    loop = asyncio.get_running_loop()
    future = loop.create_future()

    def on_done(f):
        if future.cancelled():
            return
        try:
            if threading.current_thread() is threading.main_thread():
                try:
                    result = f.value()
                    future.set_result(result)
                except RuntimeError as e:
                    # Don't set exception, let the wrapper retry
                    raise e
            else:
                loop.call_soon_threadsafe(lambda: on_done(f))
        except Exception as e:
            if not future.done():
                loop.call_soon_threadsafe(future.set_exception, e)

    qi_future.addCallback(on_done)
    return future
